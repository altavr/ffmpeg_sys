open Ctypes
module S = Avutil_stubs.Bindings (Generated_stubs)

module type FinObject = sig
  type t

  val create : unit -> t
  val finalize : t -> unit
end

type ff_error =
  [ `BsfNotFound
  | `Bug
  | `BufferTooSmall
  | `DecoderNotFound
  | `DemuxerNotFound
  | `EncoderNotFound
  | `Eof
  | `Exit
  | `External
  | `FilterNotFound
  | `Invaliddata
  | `MuxerNotFound
  | `OptionNotFound
  | `Patchwelcome
  | `ProtocolNotFound
  | `StreamNotFound
  | `Bug2
  | `Unknown
  | `Experimental
  | `InputChanged
  | `OutputChanged
  | `HttpBadRequest
  | `HttpUnauthorized
  | `HttpForbidden
  | `HttpNotFound
  | `HttpOther4xx
  | `HttpServerError
  | `UnknownUnknown ]
[@@deriving eq, show]

type unix_error = [ `EAGAIN | `EINVAL | `ENOMEM | `OTHER ] [@@deriving eq, show]
type u_error = [ ff_error | unix_error ]

type m_error =
  [ `AllocationError
  | `DestroyedObjAccess
  | `InvalidArgument of string
  | `EndOfSeqBuffer ]
[@@deriving eq, show]

type error = [ ff_error | unix_error | m_error ] [@@deriving eq, show]

module Cont
    (* : sig
         type ('a, 'e) t

         val return : ?finalize:('a -> unit) -> 'a -> ('a, 'e) t
         val fail : 'e -> ('a, 'e) t
         val bind : ('a, 'e) t -> ('a -> ('b, 'e) t) -> ('b, 'e) t
         val map : ('a -> 'b) -> ('a, 'e) t -> ('b, 'e) t
         val list_rev : ('a, 'e) t list -> ('a list, 'e) t
         val run : err:('e -> 'r) -> value:('a -> 'r) -> ('a, 'e) t -> 'r
         val list_fold_left : ('a -> 'b -> 'a) -> 'a -> ('b, 'e) t list -> ('a, 'e) t
         val seq_fold_left : ('a -> 'b -> 'a) -> 'a -> ('b, 'e) t Seq.t -> ('a, 'e) t
         val seq_iter : ('a -> unit) -> ('a, 'e) t Seq.t -> (unit, 'e) t
       end *) =
struct
  type ('a, 'e) t = {
    cont :
      'r.
      'e fin ->
      ('e -> 'e fin -> 'r * 'e fin) ->
      ('a -> 'e fin -> 'r * 'e fin) ->
      'r * 'e fin;
  }

  and 'e fin = (unit -> (unit, 'e) t) list

  let return ?finalize x =
    {
      cont =
        (fun dl _ k ->
          let d = Option.map (fun f () -> f x) finalize |> Option.to_list in
          k x (d @ dl));
    }

  let fail ?msg x = { cont = (fun dl g _ -> g (x, msg) dl) }

  let bind (m : _ t) f : _ t =
    { cont = (fun dl g k -> m.cont dl g (fun v dl -> (f v).cont dl g k)) }

  let map f m = bind m (fun v -> return (f v))

  let list_fold_left f init l =
    let cont dl g k =
      let rec aux dl' a = function
        | x :: xs -> (f a x).cont dl' g (fun v dl -> aux dl v xs)
        | [] -> k a dl'
      in
      aux dl init l
    in
    { cont }

  let list_rev_map f l =
    list_fold_left (fun a x -> map (fun r -> r :: a) (f x)) [] l

  let list_rev l = list_rev_map Fun.id l
  let list_iter l = list_fold_left (fun () x -> x) () l

  let seq_fold_left f init s =
    let cont dl g k =
      let rec aux dl' a s =
        match s () with
        | Seq.Cons (x, next) -> (f a x).cont dl' g (fun v dl -> aux dl v next)
        | Seq.Nil -> k a dl'
      in
      aux dl init s
    in
    { cont }

  let seq_iter f = seq_fold_left (fun _ -> f) ()

  let option o =
    let cont dl g k =
      match o with
      | Some x -> x.cont dl g (fun v dl -> k (Some v) dl)
      | None -> k None dl
    in
    { cont }

  let opt_iter f o =
    let cont dl g k =
      match o with
      | Some x -> (f x).cont dl g (fun _ dl -> (* f v; *)
                                               k () dl)
      | None -> k () dl
    in
    { cont }

  let run x =
    let aux x =
      x.cont [] (fun e dl -> (Error e, dl)) (fun v dl -> (Ok v, dl))
    in
    match aux x with
    | Ok v, dl -> (
        match List.map (fun f -> f ()) dl |> list_rev |> aux with
        | Ok _, _ -> Ok v
        | (Error _ as e), _ -> e)
    | (Error _ as e), dl ->
        List.map
          (fun f ->
            (* print_endline "fin"; *)
            f ())
          dl
        |> list_rev |> aux |> ignore;
        e

  module Infix = struct
    let ( >>= ) = bind
    let ( >>| ) m f = map f m
  end

  module Syntax = struct
    let ( let* ) = bind
    let ( let+ ) x f = map f x
  end
end

open Cont
open Cont.Syntax
open Cont.Infix

module Buffer = struct
  type 'a t = { arr : 'a array; mutable ptr : int }

  let create c =
    let arr = Array.of_list c in
    { arr; ptr = 0 }

  let finilize f t =
    Array.to_list t.arr |> List.map f |> Cont.list_fold_left (fun _ x -> x) ()

  let next t =
    let i = t.ptr in
    t.ptr <- (if i + 1 < Array.length t.arr then i + 1 else 0);
    t.arr.(i)
end

let items gen n = Seq.forever gen |> Seq.take n |> List.of_seq |> Cont.list_rev

let error_of_unix_error_code c : [> unix_error ] =
  let open S.UnixErrno in
  if c = eagain then `EAGAIN
  else if c = einval then `EINVAL
  else if c = enomem then `ENOMEM
  else `OTHER

let error_of_error_code c : [> u_error ] =
  let open S.ErrorCode in
  if c = -S.UnixErrno.eagain then `EAGAIN
  else if c = -S.UnixErrno.einval then `EINVAL
  else if c = -S.UnixErrno.enomem then `ENOMEM
  else if c = bsf_not_found then `BsfNotFound
  else if c = bug then `Bug
  else if c = buffer_too_small then `BufferTooSmall
  else if c = decoder_not_found then `DecoderNotFound
  else if c = demuxer_not_found then `DemuxerNotFound
  else if c = encoder_not_found then `EncoderNotFound
  else if c = eof then `Eof
  else if c = exit then `Exit
  else if c = external_ then `External
  else if c = filter_not_found then `FilterNotFound
  else if c = invaliddata then `Invaliddata
  else if c = muxer_not_found then `MuxerNotFound
  else if c = option_not_found then `OptionNotFound
  else if c = patchwelcome then `Patchwelcome
  else if c = protocol_not_found then `ProtocolNotFound
  else if c = stream_not_found then `StreamNotFound
  else if c = bug2 then `Bug2
  else if c = unknown then `Unknown
  else if c = experimental then `Experimental
  else if c = input_changed then `InputChanged
  else if c = output_changed then `OutputChanged
  else if c = http_bad_request then `HttpBadRequest
  else if c = http_unauthorized then `HttpUnauthorized
  else if c = http_forbidden then `HttpForbidden
  else if c = http_not_found then `HttpNotFound
  else if c = http_other_4xx then `HttpOther4xx
  else if c = http_server_error then `HttpServerError
  else `UnknownUnknown

(* let raise_error err = raise (Error (err, show_error err)) *)
let check_error_code c =
  if c < 0 then fail (error_of_error_code c) else return ()

let some_or_exception ?(err = `AllocationError) = function
  | Some x -> return x
  | None -> fail err

let get_errno () = error_of_unix_error_code @@ !@S.errno

exception Error of error

let run_with_exn x =
  match Cont.run x with
  | Result.Error (e, Some msg) ->
      Format.printf "%a: %s\n" pp_error e msg;
      raise (Error e)
  | Result.Error (e, None) ->
      Format.printf "%a\n" pp_error e;
      raise (Error e)
  | Ok v -> v

let ptr_ptr_null t = allocate (ptr t) (from_voidp t null)
let opt_iter_get (f : _ -> _ t) o = Option.map f o |> Option.get

type 'a media = [< `Video | `Audio ] as 'a

module Rational = struct
  type t = { num : int; den : int } [@@deriving show { with_path = false }]

  let copy ~dst ~src =
    dst |-> S.Rational.num <-@ src.num;
    dst |-> S.Rational.den <-@ src.den

  let to_ff { num; den } =
    let s = make S.Rational.t in
    setf s S.Rational.num num;
    setf s S.Rational.den den;
    s

  let of_parts s = { num = getf s S.Rational.num; den = getf s S.Rational.den }
end

module Time = struct
  type t = { hours : int; mins : int; secs : int; ms : int }
  [@@deriving show { with_path = false }]

  let ( + ) = Int64.add
  let ( * ) = Int64.mul
  let ( / ) = Int64.div
  let ( % ) = Int64.rem
  let start = { hours = 0; mins = 0; secs = 0; ms = 0 }

  let of_ts Rational.{ num; den } ts =
    let open Int64 in
    let num = of_int num in
    let den = of_int den in
    let ms = 100L * ts * num / den % 100L |> to_int in
    let secs = ts * num / den % 60L |> to_int in
    let mins = ts * num / (den * 60L) % 60L |> to_int in
    let hours = ts * num / (den * 3600L) |> to_int in
    { hours; mins; secs; ms }

  let to_ts Rational.{ num; den } { hours; mins; secs; ms } =
    let open Int64 in
    let num = of_int num in
    let den = of_int den in
    let hours = of_int hours in
    let mins = of_int mins in
    let secs = of_int secs in
    let ms = of_int ms in

    (den * ms / (100L * num))
    + (den * secs / num)
    + (60L * den * mins / num)
    + (3600L * den * hours / num)
end

let make_pp pp_elt fmt t =
  let open Format in
  fprintf fmt "@[<h>[@ @[";
  Seq.iter (fprintf fmt "%a,@ " pp_elt) t;
  fprintf fmt "@]]@]@."

type dict = (string, string) Hashtbl.t

(* exception DestroyedObjAccess *)

module FFDict
    (* : sig
         type t

         val to_parts : t -> S.Dictionary.t ptr
         val set_entries : t -> (string * string option) list -> unit
         val add_entries : t -> (string * string) list -> unit
         val create_from_parts : S.Dictionary.t ptr -> t
         val create : (string * string) list -> t
         val to_seq : t -> (string * string) Seq.t
         val pp: Format.formatter -> t -> unit
       end *) =
struct
  type t = FFDict of S.Dictionary.t ptr option ref

  let obj_or_exception (FFDict d) =
    some_or_exception !d ~err:`DestroyedObjAccess

  let set_entry p_dict (key, value) =
    check_error_code @@ S.dict_set p_dict key value 0

  let optionaze (k, v) = (k, Some v)
  let get_ptr = allocate (ptr S.Dictionary.t)
  let set_entries dict e = List.map (set_entry @@ get_ptr dict) e |> list_rev

  let add_entries dict l =
    List.map optionaze l |> List.map (set_entry @@ get_ptr dict) |> list_rev

  let take_parts (FFDict d) =
    match !d with
    | Some o ->
        d := None;
        return o
    | None -> fail `DestroyedObjAccess

  let create_from_parts p_dict = FFDict (ref @@ Some p_dict)

  let finalize (FFDict d) =
    Option.iter
      (fun p_dict' ->
        let pp_dict' = allocate (ptr S.Dictionary.t) p_dict' in
        S.dict_free pp_dict')
      !d;
    return ()

  let create = function
    | [] -> fail (`InvalidArgument "empty list")
    | l ->
        let p_dict =
          allocate (ptr S.Dictionary.t) @@ from_voidp S.Dictionary.t null
        in
        let* _ =
          List.map optionaze l |> List.map (set_entry p_dict) |> Cont.list_rev
        in
        return ~finalize (create_from_parts !@p_dict)

  let to_seq dict =
    obj_or_exception dict >>| fun ff_dict ->
    if is_null ff_dict then Seq.empty
    else
      Seq.unfold
        (fun prev ->
          S.dict_get ff_dict "" prev S.DicttinanaryConst.ignore_suffix
          |> Option.map (fun entry ->
                 ( ( getf !@entry S.DictionaryEntry.key,
                     getf !@entry S.DictionaryEntry.value ),
                   entry )))
        (from_voidp S.DictionaryEntry.t null)

  let pp fmt t =
    let t' =
      to_seq t
      >>| make_pp
            Format.(fun fmt (k, v) -> fprintf fmt "@[@ %s,@ %s@ @]" k v)
            fmt
    in

    run_with_exn t'
end

type value =
  [ `Int of int
  | `I64 of Int64.t
  | `Dbl of float
  | `Str of string
  | `Q of Rational.t
  | `Binary of string
  | `StrOpt of string option
  | `BinaryOpt of string option
  | `Dict of FFDict.t
  | `Duration of Int64.t
  | `Bool of bool
  | `ImageSize of int * int
  | `PixelFormat of S.PixFmt.t
  | `SampleFormat of S.SampleFmt.t
  | `VideoRate of Rational.t
  | `ChannelLayout of int64
  | `Unsupported ]
[@@deriving show { with_path = false }]

module Options = struct
  type opt_type = S.Opt_type.t [@@deriving show { with_path = false }]

  type entry = {
    name : string;
    help : string option;
    type_ : opt_type;
    default_value : value;
    min : float;
    max : float;
  }
  [@@deriving show { with_path = false }]

  let trans_opt r =
    let open S.Option in
    let f x = getf !@r x in

    let uni = f default_val in
    let fu x = getf uni x in
    let rational_trans r =
      Rational.{ num = getf r S.Rational.num; den = getf r S.Rational.den }
    in
    let default_value =
      (* let open S.Opt_type in *)
      match f type_ with
      | S.Opt_type.Int -> `Int (Int64.to_int @@ fu i64)
      | Int64 -> `I64 (fu i64)
      | Double -> `Dbl (fu dbl)
      | String -> `StrOpt (fu str)
      | Rational -> `Q (rational_trans @@ fu q)
      | _ -> `Unsupported
    in

    {
      name = f name;
      help = f help;
      type_ = f type_;
      default_value;
      min = f min;
      max = f max;
    }

  let get_opt_string name t flags =
    let p = allocate (ptr_opt uint8_t) None in
    let+ () =
      check_error_code
      @@ S.opt_get t name (flags lor S.OptSearchFlag.allow_null) p
    in
    let s =
      Option.map
        (fun p ->
          (* print_endline "@@@@@@@@@@@@@@@@@@@\n\n\n"; *)
          let s = (coerce (ptr uint8_t) string) p in
          S.free @@ to_voidp p;
          s)
        !@p
    in
    s

  let get_int64 f name t flags =
    let p = allocate int64_t 0L in
    let+ () = check_error_code @@ f t name flags p in
    !@p

  let get_rational f name t flags =
    let p = make S.Rational.t in
    let+ () = check_error_code @@ f t name flags @@ addr p in
    Rational.of_parts p

  let get_double f name t flags =
    let p = allocate float 0.0 in
    let+ () = check_error_code @@ f t name flags p in
    !@p

  module type Optionable = sig
    type ('a, 'b, 'c) t

    val obj : ('a, 'b, 'c) t -> unit ptr
  end

  module type FakeOptionable = sig
    include Optionable

    val avclass : ('a, 'b, 'c) t -> S.Class.t ptr ptr
  end

  module type Mediator = sig
    type ('a, 'b, 'c) t

    val obj_or_ppclass : ('a, 'b, 'c) t -> unit ptr
    val search_flags : int
  end

  module Mediator (M : Mediator) = struct
    let options t =
      Seq.unfold (fun prev ->
          S.opt_next (M.obj_or_ppclass t) prev
          |> Option.map (fun opt -> (trans_opt opt, opt)))
      @@ from_voidp S.Option.t null

    (* let set_opt_value  entry value t =  *)

    let set_opt_value ?(flags = 0) name value t =
      let t = M.obj_or_ppclass t in
      match value with
      | `Int x ->
          check_error_code @@ S.opt_set_int t name (Int64.of_int x) flags
      | `I64 x | `Duration x -> check_error_code @@ S.opt_set_int t name x flags
      | `Dbl x -> check_error_code @@ S.opt_set_double t name x flags
      | `Str x -> check_error_code @@ S.opt_set t name x flags
      (* | `Binary x -> S.opt_set_bin   *)
      | `Q x -> check_error_code @@ S.opt_set_q t name (Rational.to_ff x) flags
      | `Dict x ->
          let* p_dict = FFDict.take_parts x in
          let r = S.opt_set_dict_val t name p_dict flags in
          FFDict.create_from_parts p_dict |> ignore;
          check_error_code r
      | `Bool x ->
          check_error_code
          @@ S.opt_set_int t name (Bool.to_int x |> Int64.of_int) flags
      | `ImageSize (w, h) ->
          check_error_code @@ S.opt_set_image_size t name w h flags
      | `PixelFormat x -> check_error_code @@ S.opt_set_pixel_fmt t name x flags
      | `SampleFormat x ->
          check_error_code @@ S.opt_set_sample_fmt t name x flags
      | `VideoRate x ->
          check_error_code
          @@ S.opt_set_video_rate t name (Rational.to_ff x) flags
      | `ChannelLayout x ->
          check_error_code @@ S.opt_set_channel_layout t name x flags
    (* | _ -> raise (Invalid_argument "value") *)

    let get_opt_value ?(flags = 0) name type_ t =
      let t = M.obj_or_ppclass t in
      match type_ with
      | S.Opt_type.String ->
          let+ o = get_opt_string name t flags in
          `StrOpt o
      | Binary ->
          let+ o = get_opt_string name t flags in
          `BinaryOpt o
      | Int ->
          let+ o = get_int64 S.opt_get_int name t flags in
          `Int (Int64.to_int o)
      | Int64 ->
          let+ o = get_int64 S.opt_get_int name t flags in
          `I64 o
      | Duration ->
          let+ o = get_int64 S.opt_get_int name t flags in
          `Duration o
      | Double | Float ->
          let+ o = get_double S.opt_get_double name t flags in
          `Dbl o
      | Rational ->
          let+ o = get_rational S.opt_get_q name t flags in
          `Q o
      | Dict ->
          let p = ptr_ptr_null S.Dictionary.t in
          let+ () = check_error_code @@ S.opt_get_dict_val t name flags p in
          `Dict (FFDict.create_from_parts !@p)
      | Image_Size ->
          let p_w = allocate int 0 in
          let p_h = allocate int 0 in
          let+ () =
            check_error_code @@ S.opt_get_image_size t name flags p_w p_h
          in
          `ImageSize (!@p_w, !@p_h)
      | Pixel_Fmt ->
          let p = allocate S.PixFmt.t S.PixFmt.Nb in
          let+ () = check_error_code @@ S.opt_get_pixel_fmt t name flags p in
          `PixelFormat !@p
      | Sample_Fmt ->
          let p = allocate S.SampleFmt.t S.SampleFmt.Nb in
          let+ () = check_error_code @@ S.opt_get_sample_fmt t name flags p in
          `SampleFormat !@p
      | Video_Rate ->
          let+ o = get_rational S.opt_get_video_rate name t flags in
          `VideoRate o
      | Channel_Layout ->
          let+ o = get_int64 S.opt_get_channel_layout name t flags in
          `ChannelLayout o
      | Bool ->
          let+ o = get_int64 S.opt_get_int name t flags in
          `Bool (if o = 0L then false else true)
      | _ -> return `Unsupported

    let get_opt_entry entry t = get_opt_value entry.name entry.type_ t

    let opt_entries t =
      options t
      |> Seq.map (fun e ->
             let+ o = get_opt_entry e t in
             (e, o))

    let pp_opt_entries fmt t =
      Cont.seq_iter
        (fun x ->
          let+ e, o = x in
          Format.fprintf fmt "@[<hv>%a:@ %a@]@." pp_entry e pp_value o)
        (opt_entries t)
      |> run_with_exn

    let set_opt_from_dict dict t =
      let* p_dict = FFDict.take_parts dict in
      let pp_dict = allocate (ptr S.Dictionary.t) p_dict in
      let+ () = check_error_code @@ S.opt_set_dict t pp_dict in
      FFDict.create_from_parts !@pp_dict
  end

  module Make (M : Optionable) = struct
    include Mediator (struct
      type ('a, 'b, 'c) t = ('a, 'b, 'c) M.t

      let obj_or_ppclass = M.obj
      let search_flags = 0
    end)
  end

  module MakeFake (M : FakeOptionable) = struct
    include Mediator (struct
      type ('a, 'b, 'c) t = ('a, 'b, 'c) M.t

      let obj_or_ppclass t = to_voidp @@ M.avclass t
      let search_flags = S.OptSearchFlag.search_fake_obj
    end)
  end
end

module Frame = struct
  type 'm t = { p_frame : S.Frame.t ptr }

  let finalize { p_frame } =
    let p = allocate (ptr S.Frame.t) p_frame in
    S.frame_free p;
    return ()

  let create () =
    let* p_frame = some_or_exception @@ S.frame_alloc () in
    Cont.return ~finalize { p_frame }

  let to_parts { p_frame } = p_frame
  let unref { p_frame } = S.frame_unref p_frame

  let video ?width ?height ?pix_fmt () : ([ `Video ] t, _) Cont.t =
    let+ ({ p_frame } as frame) = create () in
    Option.iter (fun w -> p_frame |-> S.Frame.width <-@ w) width;
    Option.iter (fun h -> p_frame |-> S.Frame.height <-@ h) height;
    Option.iter
      (fun f -> p_frame |-> S.Frame.format <-@ S.PixFmt.of_enum S.PixFmt.m f)
      pix_fmt;

    frame

  let audio : _ -> ([ `Audio ] t, _) Cont.t = create

  let set_new_buffer { p_frame : S.Frame.t ptr } =
    check_error_code @@ S.frame_get_buffer p_frame 0

  let copy_props ~src ~dst =
    check_error_code @@ S.frame_copy_props dst.p_frame src.p_frame

  let get_pts { p_frame } = !@(p_frame |-> S.Frame.pts)
  let get_pkt_dts { p_frame } = !@(p_frame |-> S.Frame.pkt_dts)
  let get_pkt_duration { p_frame } = !@(p_frame |-> S.Frame.pkt_duration)

  let get_pix_format ({ p_frame } : [ `Video ] t) =
    getf !@p_frame S.Frame.format |> S.PixFmt.to_enum S.PixFmt.m

  let get_sample_format ({ p_frame } : [ `Audio ] t) =
    getf !@p_frame S.Frame.format |> S.SampleFmt.to_enum S.SampleFmt.m

  let is_key_frame { p_frame } = !@(p_frame |-> S.Frame.key_frame) != 0
  let get_pic_type { p_frame } = !@(p_frame |-> S.Frame.pict_type)
  let get_pos { p_frame } = !@(p_frame |-> S.Frame.pkt_pos)
  let get_width { p_frame } = !@(p_frame |-> S.Frame.width)
  let get_height { p_frame } = !@(p_frame |-> S.Frame.height)

  let pp fmt t =
    Format.fprintf fmt
      "@[frame@ >>@ pts:@ %i,@ key frame: %b,@ pic type:@ %a,@ pix format:@ \
       %a,@ width:@ %d,@ height:@ %d@]@."
      (Int64.to_int @@ get_pts t)
      (is_key_frame t) S.PictureType.pp (get_pic_type t) S.PixFmt.pp
      (get_pix_format t) (get_width t) (get_height t)

  let get_data ({ p_frame } : [ `Video ] t) =
    let height = getf !@p_frame S.Frame.height in
    let data =
      (* coerce
        (array 8 @@ ptr_opt uint8_t)
        (array 8 @@ ptr_opt Ctypes.char) *)
        (getf !@p_frame S.Frame.data)
    in
 List.combine (CArray.to_list data)
   (CArray.to_list @@ getf !@p_frame S.Frame.linesize)
 |> List.filter_map (fun (d, linesize) ->
        Option.map
          (fun d ->
            let d' = coerce (ptr uint8_t) (ptr Ctypes.char) d in
               bigarray_of_ptr Ctypes_static.Array2 (height, linesize)
                 Bigarray.Char d')
             d)
    |> Array.of_list
end

type pix_fmt = S.PixFmt.t [@@deriving show { with_path = false }]

module VideoFrame = struct
  type plane =
    (char, Bigarray.int8_unsigned_elt, Bigarray.c_layout) Bigarray.Array1.t
    * int

  open Bigarray

  let get_data (Frame.{ p_frame } : [ `Video ] Frame.t) =
    let height = getf !@p_frame S.Frame.height in
    let data =
         (* coerce
           (array 8 @@ ptr_opt uint8_t)
           (array 8 @@ ptr_opt Ctypes.char) *)
           (getf !@p_frame S.Frame.data)
       in
    List.combine (CArray.to_list data)
      (CArray.to_list @@ getf !@p_frame S.Frame.linesize)
    |> List.filter_map (fun (d, linesize) ->
           Option.map
             (fun d ->
               let d' = coerce (ptr uint8_t) (ptr Ctypes.char) d in
               ( bigarray_of_ptr Ctypes_static.Array1 (height * linesize)
                   Bigarray.Char d',
                 linesize ))
             d)
    |> Array.of_list

  let view f frame = f @@ get_data frame

  let dimentions (Frame.{ p_frame } : [ `Video ] Frame.t) =
    (getf !@p_frame S.Frame.width, getf !@p_frame S.Frame.height)

  let get_lines frame =
    let xsize, ysize = dimentions frame in
    let to_lines (arr, linesize) =
      Seq.unfold
        (fun ofs ->
          if ofs >= Array1.dim arr then None
          else Some (Array1.sub arr ofs xsize, ofs + linesize))
        0
      |> Seq.take ysize
    in

    Array.map to_lines @@ get_data frame

  let view_lines f frame = f @@ get_lines frame

  let pixel_format (Frame.{ p_frame } : [ `Video ] Frame.t) =
    getf !@p_frame S.Frame.format
end

module Flags = struct
  module type Enumerate = sig
    type flag

    val to_enum : flag -> int
  end

  module Make (E : Enumerate) : sig
    type t
    type flag

    val s : flag -> t
    val ( + ) : t -> flag -> t
    val empty : t
    val to_int : t -> int
  end
  with type flag = E.flag = struct
    type t = int
    type flag = E.flag

    let s f = E.to_enum f
    let ( + ) s f = s + E.to_enum f
    let empty = 0
    let to_int s = s
  end

  module type Member = sig
    type (_, _, _) t

    include Enumerate

    val get_flags : _ t -> int
    val set_flags : int -> _ t -> unit
  end

  module MakeMember (E : Member) = struct
    let flag_is_set f t = E.get_flags t land E.to_enum f != 0
    let flag_is_not_set f t = not @@ flag_is_set f t
    let flags_is_set fl t = List.for_all (fun f -> flag_is_set f t) fl
    let add_flag f t = E.set_flags (E.get_flags t lor E.to_enum f)

    let set_flags fl t =
      let f = List.map E.to_enum fl |> List.fold_left ( lor ) 0 in
      E.set_flags f t
  end
end
