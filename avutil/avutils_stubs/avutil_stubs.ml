(* open Ctypes *)

module Bindings (F : Ctypes.FOREIGN) = struct
  open Ctypes
  include Types
  include Types.AVUtil
  open F

  let errno = foreign_value "errno" int
  let free = foreign "av_free" @@ ptr void @-> returning void
  let freep = foreign "av_freep" @@ ptr void @-> returning void

  let frame_alloc =
    foreign "av_frame_alloc" @@ void @-> returning @@ ptr_opt Frame.t

  let frame_free =
    foreign "av_frame_free" @@ ptr (ptr Frame.t) @-> returning void

  let frame_unref = foreign "av_frame_unref" @@ ptr Frame.t @-> returning void

  let frame_get_buffer =
    foreign "av_frame_get_buffer" @@ ptr Frame.t @-> int @-> returning int

  let frame_ref =
    foreign "av_frame_ref" @@ ptr Frame.t @-> ptr Frame.t @-> returning int

  let frame_is_writable =
    foreign "av_frame_is_writable" @@ ptr Frame.t @-> returning int

  let frame_make_writable =
    foreign "av_frame_make_writable" @@ ptr Frame.t @-> returning int

  let frame_copy =
    foreign "av_frame_copy" @@ ptr Frame.t @-> ptr Frame.t @-> returning int

  let frame_copy_props =
    foreign "av_frame_copy_props"
    @@ ptr Frame.t @-> ptr Frame.t @-> returning int

  let codec_get_frame_class =
    foreign "avcodec_get_frame_class" @@ void @-> returning @@ ptr Class.t

  let opt_next =
    foreign "av_opt_next" @@ ptr void @-> ptr Option.t @-> returning
    @@ ptr_opt Option.t

  let opt_find =
    foreign "av_opt_find" @@ ptr void @-> string @-> string @-> int @-> int
    @-> returning @@ ptr_opt Option.t

  let opt_set =
    foreign "av_opt_set" @@ ptr void @-> string @-> string @-> int
    @-> returning int

  let opt_set_int =
    foreign "av_opt_set_int" @@ ptr void @-> string @-> int64_t @-> int
    @-> returning int

  let opt_set_double =
    foreign "av_opt_set_double"
    @@ ptr void @-> string @-> double @-> int @-> returning int

  let opt_set_q =
    foreign "av_opt_set_q" @@ ptr void @-> string @-> Rational.t @-> int
    @-> returning int

  let opt_set_bin =
    foreign "av_opt_set_bin" @@ ptr void @-> string @-> ptr uint8_t @-> int
    @-> int @-> returning int

  let opt_set_image_size =
    foreign "av_opt_set_image_size"
    @@ ptr void @-> string @-> int @-> int @-> int @-> returning int

  let opt_set_pixel_fmt =
    foreign "av_opt_set_pixel_fmt"
    @@ ptr void @-> string @-> PixFmt.t @-> int @-> returning int

  let opt_set_sample_fmt =
    foreign "av_opt_set_sample_fmt"
    @@ ptr void @-> string @-> SampleFmt.t @-> int @-> returning int

  let opt_set_video_rate =
    foreign "av_opt_set_video_rate"
    @@ ptr void @-> string @-> Rational.t @-> int @-> returning int

  let opt_set_channel_layout =
    foreign "av_opt_set_channel_layout"
    @@ ptr void @-> string @-> int64_t @-> int @-> returning int

  let opt_set_dict_val =
    foreign "av_opt_set_dict_val"
    @@ ptr void @-> string @-> ptr Dictionary.t @-> int @-> returning int

  let opt_get =
    foreign "av_opt_get" @@ ptr void @-> string @-> int
    @-> ptr (ptr_opt uint8_t)
    @-> returning int

  let opt_get_int =
    foreign "av_opt_get_int" @@ ptr void @-> string @-> int @-> ptr int64_t
    @-> returning int

  let opt_get_double =
    foreign "av_opt_get_double"
    @@ ptr void @-> string @-> int @-> ptr double @-> returning int

  let opt_get_q =
    foreign "av_opt_get_q" @@ ptr void @-> string @-> int @-> ptr Rational.t
    @-> returning int

  let opt_get_image_size =
    foreign "av_opt_get_image_size"
    @@ ptr void @-> string @-> int @-> ptr int @-> ptr int @-> returning int

  let opt_get_pixel_fmt =
    foreign "av_opt_get_pixel_fmt"
    @@ ptr void @-> string @-> int @-> ptr PixFmt.t @-> returning int

  let opt_get_sample_fmt =
    foreign "av_opt_get_sample_fmt"
    @@ ptr void @-> string @-> int @-> ptr SampleFmt.t @-> returning int

  let opt_get_video_rate =
    foreign "av_opt_get_video_rate"
    @@ ptr void @-> string @-> int @-> ptr Rational.t @-> returning int

  let opt_get_channel_layout =
    foreign "av_opt_get_channel_layout"
    @@ ptr void @-> string @-> int @-> ptr int64_t @-> returning int

  let opt_get_dict_val =
    foreign "av_opt_get_dict_val"
    @@ ptr void @-> string @-> int
    @-> ptr (ptr Dictionary.t)
    @-> returning int

  let opt_set_dict =
    foreign "av_opt_set_dict" @@ ptr void
    @-> ptr (ptr Dictionary.t)
    @-> returning int

  let dict_set =
    foreign "av_dict_set"
    @@ ptr (ptr Dictionary.t)
    @-> string @-> string_opt @-> int @-> returning int

  let dict_get =
    foreign "av_dict_get" @@ ptr Dictionary.t @-> string
    @-> ptr DictionaryEntry.t @-> int @-> returning @@ ptr_opt DictionaryEntry.t

  let dict_free =
    foreign "av_dict_free" @@ ptr (ptr Dictionary.t) @-> returning void

  let dict_get_string =
    foreign "av_dict_get_string"
    @@ ptr Dictionary.t @-> ptr string_opt @-> char @-> char @-> returning int

  let dict_count = foreign "av_dict_count" @@ ptr Dictionary.t @-> returning int
end
