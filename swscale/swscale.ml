open Ctypes
open Avutil.Cont.Syntax
module S = Swscale_stubs.Bindings (Generated_stubs)
module U = Avutil
module Cont = U.Cont

module Scaler = struct
  type t = Scaler of S.Context.t ptr * [ `Video ] U.Frame.t U.Buffer.t

  let create ?(flags = S.Flag.bicubic) ~src_width ~src_height ~src_fmt
      ~dst_width ~dst_height ~dst_fmt num =
    let finalize (Scaler (p_ctx, _)) =
      S.free_context p_ctx;
      Cont.return ()
    in
    let* p_ctx =
      U.some_or_exception
      @@ S.get_context src_width src_height src_fmt dst_width dst_height dst_fmt
           flags None None None
    in
    let* frames =
      U.items
        (fun () ->
          let* frame =
            U.Frame.video ~width:dst_width ~height:dst_height ~pix_fmt:dst_fmt
              ()
          in
          let+ () = U.Frame.set_new_buffer frame in
          frame)
        num
    in
    Cont.return ~finalize (Scaler (p_ctx, U.Buffer.create frames))

  let create_for ?flags ?dst_fmt ~dst_width ~dst_height src =
    let dst_fmt = Option.value dst_fmt ~default:(U.Frame.get_pix_format src) in
    create ?flags ~src_width:(U.Frame.get_width src)
      ~src_height:(U.Frame.get_height src)
      ~src_fmt:(U.Frame.get_pix_format src)
      ~dst_width ~dst_height ~dst_fmt
end

let scale (src : [ `Video ] U.Frame.t) Scaler.(Scaler (p_ctx, buff)) =
  let dst = U.Buffer.next buff in
  let p_src = U.Frame.to_parts src in
  let p_dst = U.Frame.to_parts dst in
  let* () = U.Frame.copy_props ~src ~dst in
  let+ () =
    U.check_error_code
    @@ S.scale p_ctx
         (CArray.start !@(p_src |-> U.S.Frame.data))
         (CArray.start !@(p_src |-> U.S.Frame.linesize))
         0 (U.Frame.get_height src)
         (CArray.start !@(p_dst |-> U.S.Frame.data))
         (CArray.start !@(p_dst |-> U.S.Frame.linesize))
  in
  dst
