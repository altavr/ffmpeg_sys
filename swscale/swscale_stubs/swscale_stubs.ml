module Bindings (F : Ctypes.FOREIGN) = struct
  open Ctypes
  open F
  include Types
  include Types.Swscale

  let free_context =
    foreign "sws_freeContext" @@ ptr Context.t @-> returning void

  let get_context =
    foreign "sws_getContext" @@ int @-> int @-> AVUtil.PixFmt.t @-> int @-> int
    @-> AVUtil.PixFmt.t @-> int @-> ptr_opt Filter.t @-> ptr_opt Filter.t
    @-> ptr_opt double @-> returning @@ ptr_opt Context.t

  let scale =
    foreign "sws_scale" @@ ptr Context.t
    @-> ptr (ptr_opt uint8_t)
    @-> ptr int @-> int @-> int
    @-> ptr (ptr_opt uint8_t)
    @-> ptr int @-> returning int
end
