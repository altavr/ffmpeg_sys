open Ctypes
open Avutil.Cont.Syntax
open Avutil.Cont.Infix
module S = Avformat_stubs.Bindings (Generated_stubs)
module U = Avutil
module C = Avcodec
module Cont = U.Cont
module IntMap = Map.Make (Int)

type input = C.de
type output = C.en
type 'a media = 'a U.media
type fmt_flag = NoFile | GlobalHeader

type seek_flag = Backward | Byte | Any | Frame

module SeekFlag = U.Flags.Make (struct
  open S.SeekFlag
   
  type flag = seek_flag

  let to_enum = function
    | Backward -> backward
    | Byte -> byte
    | Any -> any
    | Frame -> frame
end)

let ( >>=@ ) = Option.bind
let ( >>|@ ) m f = Option.map f m
let ptr_ptr_null t = allocate (ptr t) (from_voidp t null)
let elim_opt t = coerce (ptr (ptr_opt t)) (ptr (ptr t))

module OFormat = struct
  type 'm t = OFormat of S.OutputFormat.t ptr

  let formats () =
    Seq.unfold (fun s -> Option.map (fun m -> (m, s)) @@ S.muxer_iterate s)
    @@ ptr_ptr_null void

  let name (OFormat t) = getf !@t S.OutputFormat.name

  module Flags = U.Flags.MakeMember (struct
    open S.FmtFlag

    type nonrec ('a, _, _) t = 'a t
    type flag = fmt_flag

    let get_flags (OFormat t) = getf !@t S.OutputFormat.flags
    let set_flags f (OFormat t) = setf !@t S.OutputFormat.flags f
    let to_enum = function NoFile -> no_file | GlobalHeader -> globalheader
  end)

  let make_seq field res predicate (OFormat p_fmt) =
    !@(!@(p_fmt |-> field))
    >>|@ Seq.unfold (fun p ->
             let r = res p in
             if predicate r then None else Some (r, p +@ 1))
    |> Option.to_seq |> Seq.concat

  let supported_codec_tags fmt =
    make_seq S.OutputFormat.codec_tag ( !@ )
      (fun t -> getf t S.CodecTag.id = C.S.CODEC_ID.NONE)
      fmt
    |> Seq.map (fun ct ->
           ( getf ct S.CodecTag.id,
             getf ct S.CodecTag.tag |> Unsigned.UInt.to_int64 ))

  let pp_codec_tag fmt (id, tag) =
    Format.fprintf fmt "@[@ %a,@ %d@ )@]" C.S.CODEC_ID.pp id (Int64.to_int tag)

  let pp fmt c =
    let open Format in
    let pp_seq h pp s =
      fprintf fmt "@[<h>%s:@ [@ @[" h;
      Seq.iter (fprintf fmt "%a,@ " pp) s;
      fprintf fmt "@]]@]@."
    in
    fprintf fmt "@[Codec:@ %s@ @]@." (name c);
    pp_seq "supported_codec_tags" pp_codec_tag (supported_codec_tags c)

  (* let flags t = getf !@t S.OutputFormat.flags *)
end

type 'm ofmt = 'm OFormat.t = OFormat of S.OutputFormat.t ptr

module Container = struct
  type _ t =
    | IContainer : S.FormatContext.t ptr -> input t
    | OContainer : S.FormatContext.t ptr -> output t

  let to_ctx : type d. d t -> S.FormatContext.t ptr = function
    | IContainer x | OContainer x -> x

  include U.Options.Make (struct
    type nonrec ('a, 'b, 'c) t = 'a t

    let obj c = to_voidp @@ to_ctx c
  end)

  let new_stream ?codec (OContainer ctx) =
    U.some_or_exception @@ S.format_new_stream ctx codec

  let output_format (OContainer ctx) : _ ofmt =
    OFormat (getf !@ctx S.FormatContext.oformat)

  let close_output (OContainer ctx as cnt) =
    let+ () =
      if OFormat.Flags.flag_is_set NoFile @@ output_format cnt then
        Cont.opt_iter 
          (fun pb ->
            let p_pb = allocate (ptr S.IOContext.t) pb in
            U.check_error_code @@ S.io_closep p_pb)

          (getf !@ctx S.FormatContext.pb
          
          )
      else Cont.return ()
    in
    S.format_free_context ctx

  let write_trailer (OContainer ctx) =
    U.check_error_code @@ S.format_write_trailer ctx

  let close : type d. d t -> (unit, _) Cont.t = function
    | IContainer ctx ->
        let p_ctx ctx = allocate (ptr S.FormatContext.t) ctx in
        Cont.return @@ S.format_close_input (p_ctx ctx)
    | OContainer _ as cnt ->
        let* () = write_trailer cnt in
        close_output cnt
end

type 'dir container = 'dir Container.t =
  | IContainer : S.FormatContext.t ptr -> input container
  | OContainer : S.FormatContext.t ptr -> output container

module Stream = struct
  type (_, _) t =
    | IStream : S.Stream.t ptr * input Container.t -> (input, 'm) t
    | OStream : S.Stream.t ptr * output Container.t -> (output, 'm) t

  let to_parts : type d. (d, 'm) t -> S.Stream.t ptr * d Container.t = function
    | IStream (x, y) -> (x, y)
    | OStream (x, y) -> (x, y)

  let set_codec_params (param : ('m, _) C.param) (stream : ('d, 'm) t) =
    let p_param = C.Param.to_parts param in
    let p_stream, _ = to_parts stream in
    let dst = Option.get (getf !@p_stream S.Stream.codecpar) in
    U.check_error_code @@ C.S.codec_parameters_copy dst p_param

  let rational_field field stream =
    let p_stream, _ = to_parts stream in
    let tb = !@(p_stream |-> field) in
    U.Rational.of_parts tb

  let get_time_base s = rational_field S.Stream.time_base s
  let get_avg_frame_rate s = rational_field S.Stream.avg_frame_rate s
  let get_real_frame_rate s = rational_field S.Stream.r_frame_rate s

  let get_duration stream =
    let p_stream, _ = to_parts stream in
    !@(p_stream |-> S.Stream.duration)

  let get_index stream =
    let p_stream, _ = to_parts stream in
    getf !@p_stream S.Stream.index

  let get_id stream =
    let p_stream, _ = to_parts stream in
    getf !@p_stream S.Stream.id

  let get_type stream =
    let p_stream, _ = to_parts stream in
    let cp = getf !@p_stream S.Stream.codecpar in
    (* if Option.is_none p then print_endline "null param"; *)
    (* p *)
    match
      Option.map (fun param -> getf !@param C.S.CodecParameters.codec_type) cp
    with
    | Some U.S.MediaType.TYPE_VIDEO -> Cont.return `Video
    | Some U.S.MediaType.TYPE_AUDIO -> Cont.return `Audio
    | Some _ -> Cont.fail (`InvalidArgument "Unknown stream mediatype")
    | None -> Cont.fail (`InvalidArgument "Not set codec parameters")

  let init_param stream p_param =
    let p_stream, _ = to_parts stream in
    match !@(p_stream |-> S.Stream.codecpar) with
    | None -> failwith "param not found"
    | Some p -> U.check_error_code @@ C.S.codec_parameters_copy p_param p

  let video_codec_param (stream : (_, [ `Video ]) t) =
    C.Param.create_video @@ init_param stream

  let audio_codec_param (stream : (_, [ `Audio ]) t) =
    C.Param.create_audio @@ init_param stream
end

type ('d, 'm) stream = ('d, 'm) Stream.t =
  | IStream : S.Stream.t ptr * input container -> (input, 'm) stream
  | OStream : S.Stream.t ptr * output container -> (output, 'm) stream

let write_header (OContainer ctx) =
  let p_dict = ptr_ptr_null U.S.Dictionary.t in
  U.check_error_code @@ S.format_write_header ctx p_dict

let dump_format : type d m. (d, m) stream -> string -> unit =
 fun stream url ->
  let p_ctx, is_output =
    match stream with
    | IStream (_, IContainer c) -> (c, 0)
    | OStream (_, OContainer c) -> (c, 1)
  in
  S.dump_format p_ctx (Stream.get_index stream) url is_output

let open_input uri =
  let p_ctx ctx = allocate (ptr S.FormatContext.t) ctx in
  let* ctx = U.some_or_exception @@ S.format_alloc_context () in
  let dict = ptr_ptr_null U.S.Dictionary.t in
  let* () =
    U.check_error_code
    @@ S.format_open_input (p_ctx ctx) uri
         (from_voidp S.InputFormat.t null)
         dict
  in
  let format = IContainer ctx in
  let dict' = ptr_ptr_null U.S.Dictionary.t in
  let* () = U.check_error_code @@ S.format_find_stream_info ctx dict' in
  Cont.return ~finalize:(fun c -> Container.close c) format

let open_output ?format ?format_name uri =
  let p_ctx = ptr_ptr_null S.FormatContext.t in

  let* () =
    U.check_error_code
    @@ S.format_alloc_output_context2 p_ctx format format_name uri
  in
  let ctx = !@p_ctx in
  let container = OContainer ctx in
  let* () =
    if OFormat.Flags.flag_is_not_set NoFile @@ Container.output_format container
    then
      let p_pb = ctx |-> S.FormatContext.pb |> elim_opt S.IOContext.t in
      U.check_error_code @@ S.io_open p_pb uri S.IOConst.flag_write
    else Cont.return ()
  in

  U.Cont.return
    ~finalize:(fun c ->
      print_endline "close close";
      Container.close c)
    container

let new_stream ?codec ~time_base (OContainer _ as cnt) =
  let+ p_stream =
    Container.new_stream ?codec:(codec >>|@ C.Codec.to_parts) cnt
  in
  U.Rational.copy ~dst:(p_stream |-> S.Stream.time_base) ~src:time_base;

  (* p_stream |-> S.Stream.duration <-@ 0L; *)
  (* p_stream |-> S.Stream.nb_frames <-@ 0L; *)
  OStream (p_stream, cnt)

let new_video_stream ?(codec : (_, ([ `Video ] as 'm)) C.Codec.t option)
    ~time_base cnt : ((_, 'm) stream, _) Cont.t =
  new_stream ?codec ~time_base cnt

let new_audio_stream ?(codec : (_, ([ `Audio ] as 'm)) C.Codec.t option)
    ~time_base cnt : ((_, 'm) stream, _) Cont.t =
  new_stream ?codec ~time_base cnt

let get_streams (IContainer ctx as cnt) =
  let num = getf !@ctx S.FormatContext.nb_streams |> Unsigned.UInt.to_int in
  Seq.unfold (fun s ->
      (* if is_null s then Printf.printf "streams is null\n"; *)
      Some (!@s, s +@ 1))
  @@ getf !@ctx S.FormatContext.streams
  |> Seq.map (fun p_stream -> IStream (p_stream, cnt))
  |> Seq.take num

let streams_by_type typ format =
  get_streams format
  (* |> Seq.map Stream.get_type *)
  |> Cont.seq_fold_left
       (fun a x ->
         let+ t = Stream.get_type x in
         if t = typ then x :: a else a)
       []
  >>| List.rev

let video_streams f : ((_, [ `Video ]) stream list, _) Cont.t =
  streams_by_type `Video f

let audio_streams f : ((_, [ `Audio ]) stream list, _) Cont.t =
  streams_by_type `Audio f

let stream_by_index :
    type d. int -> d Container.t -> ((d, _) Stream.t, _) Cont.t =
 fun idx c ->
  let p_ctx = Container.to_ctx c in
  if
    Unsigned.UInt.to_int !@(p_ctx |-> S.FormatContext.nb_streams) <= idx
    || idx < 0
  then
    (* (    print_int idx; *)
    Cont.fail (`InvalidArgument "Not correct stream index")
  else
    let p_stream = !@(!@(p_ctx |-> S.FormatContext.streams) +@ idx) in
    match c with
    | Container.OContainer _ -> Cont.return @@ Stream.OStream (p_stream, c)
    | IContainer _ -> Cont.return @@ Stream.IStream (p_stream, c)

type chunk =
  | AudioPkt of [ `Audio ] C.Packet.t
  | VideoPkt of [ `Video ] C.Packet.t

let read_packet (IContainer ctx as cnt) (C.Packet.{ p_packet } as pkt) =
  let rc = S.read_frame ctx p_packet in
  if rc < 0 then Cont.fail ~msg:"in read_frame" (U.error_of_error_code rc)
  else
    let idx = C.Packet.get_stream_idx pkt in
    let* stream = stream_by_index idx cnt in
    let* t = Stream.get_type stream in
    match t with
    | `Audio -> Cont.return  (AudioPkt pkt , idx )
    | `Video -> Cont.return   ( VideoPkt pkt, idx )

let read_frames f (IContainer ctx as cnt) buffsize =
  let* pkts = U.items (fun () -> C.Packet.create ()) buffsize in
  let buff = U.Buffer.create pkts in
  let* m =
    get_streams cnt
    |> Cont.seq_fold_left
         (fun m s ->
           let+ t = Stream.get_type s in
           IntMap.add (Stream.get_index s) t m)
         IntMap.empty
  in
  try
    while true do
      let (C.Packet.{ p_packet } as pkt) = U.Buffer.next buff in
      C.Packet.unref pkt;
      let rc = S.read_frame ctx p_packet in
      if rc >= 0 then
        let stream_idx = C.Packet.get_stream_idx pkt in
        U.run_with_exn
          (match IntMap.find stream_idx m with
          | `Audio -> f (AudioPkt pkt, stream_idx)
          | `Video -> f (VideoPkt pkt, stream_idx))
      else
        let e = U.error_of_error_code rc in
        raise (U.Error e)
    done;
    Cont.return ()
  with
  | U.Error `Eof -> Cont.return ()
  | U.Error e -> Cont.fail e

let write_packet (OStream (_, OContainer ctx) as stream : (output, 'm) stream)
    ({ p_packet } as packet : 'm C.packet) =
  C.Packet.set_stream_index (Stream.get_index stream) packet;
  let+ () = U.check_error_code @@ S.interleaved_write_frame ctx p_packet in
  C.Packet.unref packet

let seek_file_ts ?(flags = SeekFlag.empty) ?(min_ts = 0L) ?max_ts stream ts =
  let _, cnt = Stream.to_parts stream in
  let p_ctx = Container.to_ctx cnt in
  let max_ts = Option.value max_ts ~default:(Stream.get_duration stream) in
  U.check_error_code
  @@ S.format_seek_file p_ctx (Stream.get_index stream) min_ts ts max_ts
       (SeekFlag.to_int flags)

let seek_file ?(flags = SeekFlag.empty) ?(min_time = U.Time.start) ?max_time
    stream time =
  let tb = Stream.get_time_base stream in
  seek_file_ts ~flags
    ~min_ts:((U.Time.to_ts tb) min_time)
    ?max_ts:(Option.map (U.Time.to_ts tb) max_time)
    stream
  @@ U.Time.to_ts tb time

let reset_stream (stream : (input, _) Stream.t) =
  let _, cnt = Stream.to_parts stream in
  let p_ctx = Container.to_ctx cnt in
  let* _ =
    Option.map
      (fun io_ctx ->
        let rc = S.io_seek io_ctx 0L U.S.UnixErrno.seek_set in
        if rc = -1L then Cont.fail (U.get_errno ()) else Cont.return ())
      !@(p_ctx |-> S.FormatContext.pb)
    |> Cont.option
  in
  seek_file_ts ~flags:(SeekFlag.s Backward) stream 0L
