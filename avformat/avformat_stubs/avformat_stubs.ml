module Bindings (F : Ctypes.FOREIGN) = struct
  open Ctypes
  open F
  include Types
  include Types.AVFormat

  let format_alloc_context =
    foreign "avformat_alloc_context"
    @@ void @-> returning @@ ptr_opt FormatContext.t

  let format_free_context =
    foreign "avformat_free_context" @@ ptr FormatContext.t @-> returning void

  let dump_format =
    foreign "av_dump_format" @@ ptr FormatContext.t @-> int @-> string @-> int
    @-> returning void

  let format_open_input =
    foreign "avformat_open_input"
    @@ ptr (ptr FormatContext.t)
    @-> string @-> ptr InputFormat.t
    @-> ptr (ptr AVUtil.Dictionary.t)
    @-> returning int

  let format_close_input =
    foreign "avformat_close_input"
    @@ ptr (ptr FormatContext.t)
    @-> returning void

  let io_closep =
    foreign "avio_closep" @@ ptr (ptr IOContext.t) @-> returning int

  let seek_frame =
    foreign "av_seek_frame" @@ ptr FormatContext.t @-> int @-> int64_t @-> int
    @-> returning int

  let format_seek_file =
    foreign "avformat_seek_file"
    @@ ptr FormatContext.t @-> int @-> int64_t @-> int64_t @-> int64_t @-> int
    @-> returning int

  let format_flush =
    foreign "avformat_flush" @@ ptr FormatContext.t @-> returning int

  let format_find_stream_info =
    foreign "avformat_find_stream_info"
    @@ ptr FormatContext.t
    @-> ptr (ptr AVUtil.Dictionary.t)
    @-> returning int

  let read_frame =
    foreign "av_read_frame"
    @@ ptr AVFormat.FormatContext.t
    @-> ptr AVCodec.Packet.t @-> returning int

  let format_alloc_output_context2 =
    foreign "avformat_alloc_output_context2"
    @@ ptr (ptr FormatContext.t)
    @-> ptr_opt OutputFormat.t @-> string_opt @-> string @-> returning int

  let format_new_stream =
    foreign "avformat_new_stream"
    @@ ptr FormatContext.t @-> ptr_opt AVCodec.Codec.t @-> returning
    @@ ptr_opt Stream.t

  let io_open =
    foreign "avio_open"
    @@ ptr (ptr IOContext.t)
    @-> string @-> int @-> returning int

  let io_open2 =
    foreign "avio_open2"
    @@ ptr (ptr IOContext.t)
    @-> string @-> int @-> ptr IOInterruptCB.t
    @-> ptr (ptr AVUtil.Dictionary.t)
    @-> returning int

  let io_seek =
    foreign "avio_seek" @@ ptr IOContext.t @-> int64_t @-> int
    @-> returning int64_t

  let format_write_header =
    foreign "avformat_write_header"
    @@ ptr FormatContext.t
    @-> ptr (ptr AVUtil.Dictionary.t)
    @-> returning int

  let format_write_trailer =
    foreign "av_write_trailer" @@ ptr FormatContext.t @-> returning int

  let muxer_iterate =
    foreign "av_muxer_iterate"
    @@ ptr (ptr void)
    @-> returning @@ ptr_opt OutputFormat.t

  let write_frame =
    foreign "av_write_frame" @@ ptr FormatContext.t @-> ptr AVCodec.Packet.t
    @-> returning int

  let interleaved_write_frame =
    foreign "av_interleaved_write_frame"
    @@ ptr FormatContext.t @-> ptr AVCodec.Packet.t @-> returning int
end
