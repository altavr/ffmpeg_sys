module Bindings (F : Ctypes.FOREIGN) = struct
  open Ctypes
  include Types
  include Types.AVCodec
  open F

  let codec_find_decoder =
    foreign "avcodec_find_decoder"
    @@ CODEC_ID.t @-> returning @@ ptr_opt Codec.t

  let codec_find_decoder_by_name =
    foreign "avcodec_find_decoder_by_name"
    @@ string @-> returning @@ ptr_opt Codec.t

  let codec_find_encoder =
    foreign "avcodec_find_encoder"
    @@ CODEC_ID.t @-> returning @@ ptr_opt Codec.t

  let codec_find_encoder_by_name =
    foreign "avcodec_find_encoder_by_name"
    @@ string @-> returning @@ ptr_opt Codec.t

  let codec_alloc_context3 =
    foreign "avcodec_alloc_context3"
    @@ ptr Codec.t @-> returning @@ ptr_opt CodecContext.t

  let codec_free_context =
    foreign "avcodec_free_context"
    @@ ptr (ptr CodecContext.t)
    @-> returning void

  let codec_parameters_to_context =
    foreign "avcodec_parameters_to_context"
    @@ ptr CodecContext.t @-> ptr CodecParameters.t @-> returning int

  let codec_open2 =
    foreign "avcodec_open2" @@ ptr CodecContext.t @-> ptr Codec.t
    @-> ptr (ptr AVUtil.Dictionary.t)
    @-> returning int

  let packet_alloc =
    foreign "av_packet_alloc" @@ void @-> returning @@ ptr_opt Packet.t

  let packet_unref =
    foreign "av_packet_unref" @@ ptr Packet.t @-> returning void

  let packet_free =
    foreign "av_packet_free" @@ ptr (ptr Packet.t) @-> returning void

  let codec_send_frame =
    foreign "avcodec_send_frame"
    @@ ptr CodecContext.t @-> ptr AVUtil.Frame.t @-> returning int

  let codec_send_packet =
    foreign "avcodec_send_packet"
    @@ ptr CodecContext.t @-> ptr Packet.t @-> returning int

  let codec_receive_frame =
    foreign "avcodec_receive_frame"
    @@ ptr CodecContext.t @-> ptr AVUtil.Frame.t @-> returning int

  let codec_receive_packet =
    foreign "avcodec_receive_packet"
    @@ ptr CodecContext.t @-> ptr Packet.t @-> returning int

  let codec_parameters_alloc =
    foreign "avcodec_parameters_alloc"
    @@ void @-> returning @@ ptr_opt CodecParameters.t

  let codec_parameters_free =
    foreign "avcodec_parameters_free"
    @@ ptr (ptr CodecParameters.t)
    @-> returning void

  let codec_parameters_copy =
    foreign "avcodec_parameters_copy"
    @@ ptr CodecParameters.t @-> ptr CodecParameters.t @-> returning int

  let codec_parameters_from_context =
    foreign "avcodec_parameters_from_context"
    @@ ptr CodecParameters.t @-> ptr CodecContext.t @-> returning int

  let packet_rescale_ts =
    foreign "av_packet_rescale_ts"
    @@ ptr Packet.t @-> AVUtil.Rational.t @-> AVUtil.Rational.t
    @-> returning void
end
