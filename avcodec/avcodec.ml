open Ctypes
open Avutil.Cont.Syntax
open Avutil.Cont.Infix
module S = Avcodec_stubs.Bindings (Generated_stubs)
module U = Avutil
module Cont = U.Cont
 
type 'a media = 'a U.media
type en = EN
type de = DE

(* let ( >>=@ ) = Option.bind *)
let ( >>|@ ) m f = Option.map f m

module Codec = struct
  type (_, 'm) t =
    | Decodec : S.Codec.t ptr -> (de, 'm) t
    | Encodec : S.Codec.t ptr -> (en, 'm) t

  let to_parts : type c m. (c, m) t -> S.Codec.t ptr = function
    | Decodec x -> x
    | Encodec x -> x

  let find_de codec_id =
    let+ p_codec =
      S.codec_find_decoder codec_id |> U.some_or_exception ~err:`DecoderNotFound
    in
    Decodec p_codec

  let find_video_de codec_id : ((_, [ `Video ]) t, _) Cont.t = find_de codec_id
  let find_audio_de codec_id : ((_, [ `Audio ]) t, _) Cont.t = find_de codec_id

  let find_en codec_id =
    let+ p_codec =
      S.codec_find_encoder codec_id |> U.some_or_exception ~err:`EncoderNotFound
    in
    Encodec p_codec

  let find_video_en codec_id : ((_, [ `Video ]) t, _) Cont.t = find_en codec_id
  let find_audio_en codec_id : ((_, [ `Audio ]) t, _) Cont.t = find_en codec_id

  let find_en_by_name name =
    let+ p_codec =
      S.codec_find_encoder_by_name name
      |> U.some_or_exception ~err:`EncoderNotFound
    in
    Encodec p_codec

  let find_video_en_by_name codec_id : ((_, [ `Video ]) t, _) Cont.t =
    find_en_by_name codec_id

  let find_audio_en_by_name codec_id : ((_, [ `Audio ]) t, _) Cont.t =
    find_en_by_name codec_id

  let name c = !@(to_parts c |-> S.Codec.name)
  let long_name c = !@(to_parts c |-> S.Codec.long_name)
  let codec_id c = !@(to_parts c |-> S.Codec.id)

  let make_seq field res predicate c =
    !@(to_parts c |-> field)
    >>|@ Seq.unfold (fun p ->
             let r = res p in
             if predicate r then None else Some (r, p +@ 1))
    |> Option.to_seq |> Seq.concat

  let supported_framerates c =
    make_seq S.Codec.supported_framerates
      (fun p -> U.Rational.of_parts !@p)
      (fun r -> r.num = 0 && r.den = 0)
      c

  let supported_pix_fmts c =
    make_seq S.Codec.pix_fmts ( !@ )
      (fun p -> coerce U.S.PixFmt.t int32_t p = -1l)
      c

  let supported_samplerates c =
    make_seq S.Codec.supported_samplerates ( !@ ) (Int.equal 0) c

  let supported_sample_fmts c =
    make_seq S.Codec.sample_fmts ( !@ )
      (fun p -> coerce U.S.SampleFmt.t int32_t p = -1l)
      c

  let supported_channel_layouts c =
    make_seq S.Codec.channel_layouts ( !@ ) (Int64.equal 0L) c

  let pp fmt c =
    let open Format in
    let pp_seq h pp s =
      fprintf fmt "@[<h>%s:@ [@ @[" h;
      Seq.iter (fprintf fmt "%a,@ " pp) s;
      fprintf fmt "@]]@]@."
    in
    fprintf fmt "@[Codec:@ %s@ [[@ %s@ ]]@]@." (long_name c) (name c);
    fprintf fmt "@[codec_id:@ %a@]@." S.CODEC_ID.pp (codec_id c);
    pp_seq "supported_framerates" U.Rational.pp (supported_framerates c);
    pp_seq "pix_fmts" U.S.PixFmt.pp (supported_pix_fmts c);
    pp_seq "supported_samplerates" pp_print_int (supported_samplerates c);
    pp_seq "sample_fmts" U.S.SampleFmt.pp (supported_sample_fmts c);
    pp_seq "channel_layouts"
      (fun fmt -> fprintf fmt "%Ld")
      (supported_channel_layouts c)
end

module Param = struct
  type ('m, _) t =
    | AudioParam : S.CodecParameters.t ptr -> ([ `Audio ], U.S.SampleFmt.t) t
    | VideoParam : S.CodecParameters.t ptr -> ([ `Video ], U.S.PixFmt.t) t

  let to_parts : type m fmt. (m, fmt) t -> S.CodecParameters.t ptr = function
    | AudioParam x -> x
    | VideoParam x -> x

  let create codec_type init =
    let* p_param = U.some_or_exception @@ S.codec_parameters_alloc () in
    p_param |-> S.CodecParameters.codec_type <-@ codec_type;
    let* param = init p_param in
    let finalize param' =
      let p_param' = to_parts param' in
      let p = allocate (ptr S.CodecParameters.t) p_param' in
      S.codec_parameters_free p;
      Cont.return ()
    in
    Cont.return ~finalize param

  let create_video init =
    let f p_param =
      let+ () = init p_param in
      VideoParam p_param
    in
    create U.S.MediaType.TYPE_VIDEO f

  let create_audio init =
    let f p_param =
      let+ () = init p_param in
      AudioParam p_param
    in
    create U.S.MediaType.TYPE_AUDIO f

  let get_codec_id param =
    let p_param = to_parts param in
    getf !@p_param S.CodecParameters.codec_id

  let get_format : type m fmt. (m, fmt) t -> fmt = function
    | AudioParam p_param ->
        getf !@p_param S.CodecParameters.format
        |> U.S.SampleFmt.to_enum U.S.SampleFmt.m
    | VideoParam p_param ->
        getf !@p_param S.CodecParameters.format
        |> U.S.PixFmt.to_enum U.S.PixFmt.m

  let get_channels (AudioParam p_param) =
    getf !@p_param S.CodecParameters.channels

  let get_sample_rate (AudioParam p_param) =
    getf !@p_param S.CodecParameters.sample_rate

  let get_frame_size (AudioParam p_param) =
    getf !@p_param S.CodecParameters.frame_size

  let get_height (VideoParam p_param) = getf !@p_param S.CodecParameters.height
  let get_width (VideoParam p_param) = getf !@p_param S.CodecParameters.width

  let set_code_tag code_tag param =
    let p_param = to_parts param in
    p_param |-> S.CodecParameters.codec_tag <-@ Unsigned.UInt32.of_int code_tag

  let pp : type m fmt. Format.formatter -> (m, fmt) t -> unit =
   fun fmt t ->
    match t with
    | VideoParam _ ->
        Format.fprintf fmt
          "@[Codec@ param@ >>@ codec:@ %a, pix@ format:@ %a,@ width:@ %d,@ \
           height:@ %d@]@."
          S.CODEC_ID.pp (get_codec_id t) U.S.PixFmt.pp (get_format t)
          (get_width t) (get_height t)
    | _ -> assert false
end

type ('m, 'a) param = ('m, 'a) Param.t =
  | AudioParam : S.CodecParameters.t ptr -> ([ `Audio ], U.S.SampleFmt.t) param
  | VideoParam : S.CodecParameters.t ptr -> ([ `Video ], U.S.PixFmt.t) param

module Packet = struct
  type 'm t = { p_packet : S.Packet.t ptr }

  let finalize { p_packet } =
    let p = allocate (ptr S.Packet.t) p_packet in
    S.packet_free p;
    Cont.return ()

  let create () =
    let* p_packet = U.some_or_exception @@ S.packet_alloc () in
    Cont.return ~finalize { p_packet }

  let to_parts { p_packet } = p_packet
  let video () : ([ `Video ] t, _) Cont.t = create ()
  let audio () : ([ `Audio ] t, _) Cont.t = create ()

  let rescale_ts ~src ~dst { p_packet } =
    S.packet_rescale_ts p_packet (U.Rational.to_ff src) (U.Rational.to_ff dst)

  let unref { p_packet } = S.packet_unref p_packet
  let get_stream_idx { p_packet } = getf !@p_packet S.Packet.stream_index
  let get_pts { p_packet } = getf !@p_packet S.Packet.pts
  let get_dts { p_packet } = getf !@p_packet S.Packet.dts
  let get_duration { p_packet } = getf !@p_packet S.Packet.duration
  let get_pos { p_packet } = getf !@p_packet S.Packet.pos
  let set_pts v { p_packet } = p_packet |-> S.Packet.pts <-@ v
  let set_dts v { p_packet } = p_packet |-> S.Packet.dts <-@ v
  let set_duration v { p_packet } = p_packet |-> S.Packet.duration <-@ v
  let set_stream_index v { p_packet } = p_packet |-> S.Packet.stream_index <-@ v
  let set_pos v { p_packet } = p_packet |-> S.Packet.pos <-@ v

  let pp fmt t =
    Format.fprintf fmt
      "@[packet@ >>@ stream_id:@ %i,@ pts:@ %i,@ dts:@ %i,@ duration:@ %i@]@."
      (get_stream_idx t)
      (Int64.to_int @@ get_pts t)
      (Int64.to_int @@ get_dts t)
      (Int64.to_int @@ get_duration t)
end

type 'm packet = 'm Packet.t = { p_packet : S.Packet.t ptr }
type codec_flag = GlobalHeader

module Coder = struct
  type (_, _, _) t =
    | Decoder : {
        p_coder : S.CodecContext.t ptr;
        buff : 'm U.Frame.t U.Buffer.t;
      }
        -> (de, 'm, 'm U.Frame.t) t
    | Encoder : {
        p_coder : S.CodecContext.t ptr;
        mutable buff : 'm packet U.Buffer.t;
      }
        -> (en, 'm, 'm packet) t

  let to_parts : type c m r. (c, m, r) t -> S.CodecContext.t ptr * r U.Buffer.t
      = function
    | Decoder { p_coder; buff } -> (p_coder, buff)
    | Encoder { p_coder; buff } -> (p_coder, buff)

  let next_item : type c m r. (c, m, r) t -> r =
   fun c ->
    match c with
    | Decoder x -> U.Buffer.next x.buff
    | Encoder x -> U.Buffer.next x.buff

  include U.Options.Make (struct
    type nonrec ('c, 'm, 'r) t = ('c, 'm, 'r) t

    let obj t = to_voidp @@ fst @@ to_parts t
  end)

  module Flags = U.Flags.MakeMember (struct
    open S.CodecFlag

    type nonrec ('a, 'b, 'c) t = ('a, 'b, 'c) t
    type flag = codec_flag

    let get_flags t =
      let ctx, _ = to_parts t in
      !@(ctx |-> S.CodecContext.flags)

    let set_flags f t =
      let ctx, _ = to_parts t in
      ctx |-> S.CodecContext.flags <-@ f

    let to_enum = function GlobalHeader -> global_header
  end)

  module PrivData = U.Options.Make (struct
    type nonrec ('c, 'm, 'r) t = ('c, 'm, 'r) t

    let obj t = to_voidp (fst @@ to_parts t |-> S.CodecContext.priv_data)
  end)

  let create f _item_finalize ?(flags = []) ?opts
      ?(param : ('m, _) param option) ?bitrate ?encoder_params
      (codec : ('d, 'm) Codec.t) : (('d, 'm, _) t * _, _) Cont.t =
    let p_codec = Codec.to_parts codec in

    let* ctx = S.codec_alloc_context3 p_codec |> U.some_or_exception in

    (* Option.iter (fun flags -> ctx |-> S.CodecContext.flags <-@ flags) flags; *)
    let* p_dict =
      Option.map
        (fun d -> U.FFDict.take_parts d >>| allocate (ptr U.S.Dictionary.t))
        opts
      |> Option.value ~default:(Cont.return @@ U.ptr_ptr_null U.S.Dictionary.t)
    in
    let* _ =
      Option.map
        (fun x ->
          Param.to_parts x
          |> S.codec_parameters_to_context ctx
          |> U.check_error_code)
        param
      |> Cont.option
    in
    Option.iter
      (fun bitrate -> ctx |-> S.CodecContext.bitrate <-@ bitrate)
      bitrate;
    Option.iter
      (fun timebase ->
        U.Rational.copy ~dst:(ctx |-> S.CodecContext.time_base) ~src:timebase)
      encoder_params;
    let r = f ctx in
    Flags.set_flags flags r;
    let* () = U.check_error_code @@ S.codec_open2 ctx p_codec p_dict in
    (* (ctx |->  S.CodecContext.thread_count <-@ 4); *)
    let finalize (coder, dict) =
      let+ () = U.FFDict.finalize dict in
      let p_coder, _item = to_parts coder in
      (* let+ () = (U.Buffer.finilize item_finalize) item in  *)
      let p = allocate (ptr S.CodecContext.t) p_coder in
      S.codec_free_context p
    in
    U.Cont.return ~finalize (r, U.FFDict.create_from_parts !@p_dict)

  let create_video_decoder param codec buffsize :
      ((_, [ `Video ], _) t * _, _) Cont.t =
    let* c = U.items (fun () -> U.Frame.create ()) buffsize in
    let buff = U.Buffer.create c in
    create
      (fun p_coder -> Decoder { p_coder; buff })
      U.Frame.finalize ~param codec

  let create_audio_decoder param codec buffsize :
      ((_, [ `Audio ], _) t * _, _) Cont.t =
    let* c = U.items (fun () -> U.Frame.create ()) buffsize in
    let buff = U.Buffer.create c in
    create
      (fun p_coder -> Decoder { p_coder; buff })
      U.Frame.finalize ~param codec

  let create_video_encoder ?flags ?opts ?bitrate ~framerate ~time_base
      ~pix_format ~width ~height codec buffsize :
      ((_, [ `Video ], _) t * _, _) Cont.t =
    let* c = U.items (fun () -> Packet.create ()) buffsize in
    let buff = U.Buffer.create c in
    create
      (fun p_coder ->
        p_coder |-> S.CodecContext.pix_fmt <-@ pix_format;
        p_coder |-> S.CodecContext.width <-@ width;
        p_coder |-> S.CodecContext.height <-@ height;
        U.Rational.copy
          ~dst:(p_coder |-> S.CodecContext.framerate)
          ~src:framerate;

        (* c |-> S.CodecContext.rc_buffer_size <-@ 4_000_000L;
           c |-> S.CodecContext.rc_min_rate <-@ 500_000L;
           c |-> S.CodecContext.rc_max_rate <-@ 2500_000L; *)

        (* c |-> S.CodecContext.gop_size <-@ 10; *)
        (* c |-> S.CodecContext.max_b_frames <-@ 1; *)
        Encoder { p_coder; buff })
      Packet.finalize ?flags ?opts ?bitrate ~encoder_params:time_base codec

  let create_audio_encoder ?flags ?opts ?(channels = 2)
      ?(channel_layout = U.S.ChannelLayout.layout_stereo) ?bitrate ~time_base
      ~sample_format ~sample_rate codec buffsize :
      (('d, [ `Audio ], _) t * _, _) Cont.t =
    let* c = U.items (fun () -> Packet.create ()) buffsize in
    let buff = U.Buffer.create c in
    create
      (fun p_coder ->
        p_coder |-> S.CodecContext.channels <-@ channels;
        p_coder |-> S.CodecContext.channel_layout <-@ channel_layout;
        p_coder |-> S.CodecContext.sample_fmt <-@ sample_format;
        p_coder |-> S.CodecContext.sample_rate <-@ sample_rate;

        Encoder { p_coder; buff })
      Packet.finalize ?flags ?opts ?bitrate ~encoder_params:time_base codec

  let init_param coder param_t =
    U.check_error_code
    @@ S.codec_parameters_from_context param_t
    @@ fst @@ to_parts coder

  let get_video_params (coder : (_, 'm, _) t) : (('m, _) param, _) Cont.t =
    let f = init_param coder in
    Param.create_video f

  let get_audio_params (coder : (_, 'm, _) t) : (('m, _) param, _) Cont.t =
    let f = init_param coder in
    Param.create_audio f

  let get_thread_count coder =
    let p_ctx, _ = to_parts coder in
    !@(p_ctx |-> S.CodecContext.thread_count)

  let set_thread_count n coder =
    let p_ctx, _ = to_parts coder in
    p_ctx |-> S.CodecContext.thread_count <-@ n
end

let decode (decoder : (de, 'm, _) Coder.t) ({ p_packet } : 'm packet) =
  let p_decoder, buff = Coder.to_parts decoder in
  let* () = U.check_error_code @@ S.codec_send_packet p_decoder p_packet in
  let rec aux a =
    let frame = U.Buffer.next buff in
    U.Frame.unref frame;
    let rc = S.codec_receive_frame p_decoder (U.Frame.to_parts frame) in
    if rc >= 0 then aux (frame :: a)
    else
      match U.error_of_error_code rc with
      | `EAGAIN | `Eof -> Cont.return @@ List.rev a
      | e -> Cont.fail e
  in
  aux []

let encode (encoder : (en, 'm, _) Coder.t) ({ p_frame } : 'm U.Frame.t) :
    (_ Packet.t list, _) Cont.t =
  let p_encoder, buff = Coder.to_parts encoder in
  let* () = U.check_error_code @@ S.codec_send_frame p_encoder p_frame in
  let rec aux a =
    let pkt = U.Buffer.next buff in
    Packet.unref pkt;
    let rc = S.codec_receive_packet p_encoder (Packet.to_parts pkt) in
    if rc >= 0 then aux (pkt :: a)
    else
      match U.error_of_error_code rc with
      | `EAGAIN | `Eof -> Cont.return @@ List.rev a
      | e -> Cont.fail e
  in
  aux []
