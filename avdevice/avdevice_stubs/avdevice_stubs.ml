module Bindings (F : Ctypes.FOREIGN) = struct
  (* open F *)
  open Ctypes
  open F
  include Types
  include Types.AVDevice
end
 