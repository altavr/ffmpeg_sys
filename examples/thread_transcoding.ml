module F = Avformat
module U = Avutil
module C = Avcodec

[@@@warning "-32"]

module Cont = U.Cont
open U.Cont.Syntax
open U.Cont.Infix

let time_all = ref 0.
let time_encode = ref 0.

let running_time c f x =
  let start = Unix.gettimeofday () in
  let r = f x in
  c := !c +. (Unix.gettimeofday () -. start);
  r

let () =
  let ifile =
    "file://home/alex/downloads/10000000_345493607584308_4456146274528545550_n.mp4"
  in
  let ofile = "example1.mkv" in
  let tranform =
    F.open_input ifile >>= fun ic ->
    (* "file://home/alex/downloads/15219135733320.mp4" *)
    let ivstream = F.video_streams ic |> List.hd in
    let iastream = F.audio_streams ic |> List.hd in
    F.dump_format ivstream ifile;

    (* stream_info ivstream; *)
    let* vparam = F.Stream.video_codec_param ivstream in
    let* aparam = F.Stream.audio_codec_param iastream in

    let* vdecodec = C.Codec.find_video_de @@ C.Param.get_codec_id vparam in
    C.Coder.create_video_decoder vparam vdecodec 5 >>= fun (vdecoder, _) ->


    F.open_output ofile >>= fun oc ->
    let* vencodec = C.Codec.find_video_en_by_name "libx265" in
    let framerate = F.Stream.get_real_frame_rate ivstream in
    let time_base = F.Stream.get_time_base ivstream in
      (* let time_base  : U.Rational.t = {num=1; den=1000} in *)
    (* C.Codec.supported_pix_fmts vencodec
       |> Seq.iter (  Format.printf "%a@." U.S.PixFmt.pp   ); *)
    let _ofmt = F.Container.output_format oc in
    let flags =
      if F.OFormat.Flags.flag_is_set F.GlobalHeader _ofmt then
        [ C.GlobalHeader ]
      else []
    in

    let* opts = U.FFDict.create [ ("preset", "veryfast"); ("crf", "20") ] in
    C.Coder.create_video_encoder ~flags ~opts ~framerate ~time_base
      ~pix_format:U.S.PixFmt.Yuv420p ~width:(C.Param.get_width vparam)
      ~height:(C.Param.get_height vparam)
      vencodec 5
    >>= fun (vencoder, _rejected_opts) ->
    U.FFDict.pp Format.std_formatter _rejected_opts;

    Printf.printf "threads: %d\n" @@ C.Coder.get_thread_count vencoder;
    (* C.Coder.PrivData.options _vencoder
       |> Seq.iter
        ( U.Options.pp_entry Format.std_formatter ) ; *)
    let* veparam = C.Coder.get_video_params vencoder in

    let* ovs = F.new_video_stream ~codec:vencodec ~time_base oc in
    let* oas =
      F.new_audio_stream ~time_base:(F.Stream.get_time_base iastream) oc
    in
    Format.printf "@[Video encode:@ %a@]" C.Param.pp veparam;
    let* () = F.Stream.set_codec_params veparam ovs in

    C.Param.set_code_tag 0 aparam;
    let* () = F.Stream.set_codec_params aparam oas in

    let* () = F.write_header oc in

    let _write_apacket istream ostream pkt =
      let tb1 = F.Stream.get_time_base ostream in
      let tb2 = F.Stream.get_time_base istream in
      C.Packet.rescale_ts ~src:tb2 ~dst:tb1 pkt;
      F.write_packet ostream pkt
    in

    let ov_tb = F.Stream.get_time_base ovs in
    let iv_tb = F.Stream.get_time_base ivstream in

    let frames_ch = Event.new_channel () in
    let packets_ch = Event.new_channel () in

    let encode_video () =
      print_endline "Run encode video thread";
      try
        while true do
          let frames =
            match Event.(receive frames_ch |> sync) with
            | `Stop -> raise Exit
            | `Msg x -> x
          in

          (* Printf.printf "Receive %d frames\n" (List.length frames); *)
          List.iter
            (fun frame ->
              (* U.Frame.pp Format.std_formatter frame; *)
              C.encode vencoder frame |> U.run_with_exn |> fun pkts ->
              Event.(send packets_ch (`Msg pkts) |> sync)
              (* print_endline "Send packet" *))
            frames
        done
      with Exit ->
        Event.(send packets_ch `Stop |> sync);
        Thread.exit ()
    in

    let write_video () =
      print_endline "Run writer thread";
      try
        while true do
          let packets =
            match Event.(receive packets_ch |> sync) with
            | `Stop -> raise Exit
            | `Msg x -> x
          in

          (* Printf.printf "Receive %d packets\n" (List.length packets); *)
          List.iter
            (fun pkt ->
              (* Format.printf "@[Encode@ %a@]@." C.Packet.pp pkt; *)
              C.Packet.rescale_ts ~src:ov_tb ~dst:iv_tb pkt;
              U.run_with_exn @@ F.write_packet ovs pkt)
            packets
        done
      with Exit -> Thread.exit ()
    in

    let thread_encode = Thread.create encode_video () in
    let thread_write = Thread.create write_video () in

    let write_vpacket pkt =
      let* frames = C.decode vdecoder pkt in
      Cont.return Event.(send frames_ch (`Msg frames) |> sync)
    in

    (* let write_vpacket pkt =
         let* frames = C.decode vdecoder pkt in
         let* pkts =
           List.map (C.encode vencoder) frames |> Cont.list_rev >>| fun x ->
           List.rev x |> List.concat
         in
         List.map
           (fun pkt ->
             C.Packet.rescale_ts ~dst:ov_tb ~src:iv_tb pkt;
             F.write_packet ovs pkt)
           pkts
         |> Cont.list_iter
       in *)
    let read_handler = function
      (* | F.AudioPkt pkt, _ -> write_apacket iastream oas pkt *)
      | F.VideoPkt pkt, _ -> write_vpacket pkt
      | _ -> Cont.return ()
    in

    let read_handler' = running_time time_all read_handler in

    let* () = F.read_frames read_handler' ic 5 in

    Event.(send frames_ch `Stop |> sync);
    Thread.join thread_encode;
    Thread.join thread_write;
    F.dump_format oas ofile;
    U.Cont.return ()
  in

  U.run_with_exn tranform;
  Printf.printf "Transcode time - %f. Encode time - %f \n" !time_all
    !time_encode
