module F = Avformat
module U = Avutil
module C = Avcodec

[@@@warning "-32"]

let ( >>= ) = U.Cont.bind
let ( >>| ) = U.Cont.map

let stream_info s =
  let open F.Stream in
  Format.printf
    "stream info: %d@.time base: %a@.avr_frame_rate: %a@.real_frame_rate: \
     %a@.duration: %Ld@.@."
    (get_index s) U.Rational.pp (get_time_base s) U.Rational.pp
    (get_avg_frame_rate s) U.Rational.pp (get_real_frame_rate s)
    (get_duration s)

let packet_info idx p =
  let open C.Packet in
  Format.printf "packet %d: pts: %Ld, dts: %Ld, duration: %Ld, pos: %Ld@." idx
    (get_pts p) (get_dts p) (get_duration p) (get_pos p)

let () =
  let ifile =
    "file://home/alex/downloads/10000000_345493607584308_4456146274528545550_n.mp4"
  in
  let ofile = "example1.avi" in
  let machine =
    F.open_input ifile >>= fun ic ->
    let ivstream = F.video_streams ic |> List.hd in
    let iastream = F.audio_streams ic |> List.hd in
    F.dump_format iastream ifile;

    let vparam = F.Stream.video_codec_param ivstream in
    let aparam = F.Stream.audio_codec_param iastream in

    F.open_output ofile >>= fun oc ->
    let ovs = F.new_video_stream ~timebase:{ num = 1; den = 1000 } oc in
    let oas =
      F.new_audio_stream ~timebase:(F.Stream.get_time_base iastream) oc
    in
    F.Stream.set_codec_params vparam ovs;

    C.Param.set_code_tag 0 aparam;
    F.Stream.set_codec_params aparam oas;
    stream_info oas;
    F.write_header oc;
    let write_packet istream ostream pkt =
      let tb1 = F.Stream.get_time_base ostream in
      let tb2 = F.Stream.get_time_base istream in
      C.Packet.rescale_ts ~src:tb2 ~dst:tb1 pkt;
      F.write_packet ostream () pkt
    in
    let read_handler = function
      | F.AudioPkt pkt, _ -> write_packet iastream oas pkt
      | F.VideoPkt pkt, _ -> write_packet ivstream ovs pkt
      (* | _ ->  () *)
    in
    F.read_frames read_handler ic;
    U.Cont.return @@ F.dump_format oas ofile
  in

  U.Cont.run machine
