module F = Avformat
module U = Avutil
module C = Avcodec
(* open Bigarray *)

(* let slice array a b =
   let sub = Bigarray.Array1.sub array a (b - a) in
   Bytes.init (b - a) (fun i -> Bigarray.Array1.unsafe_get sub i) *)

let bytes_of_bigarray arr =
  Bytes.init (Bigarray.Array1.dim arr) (Bigarray.Array1.unsafe_get arr)

exception StopDecoding

let write_pic frame =
  let w, h = U.VideoFrame.dimentions frame in

  Printf.printf "size - %d x %d\npixel format - %s\n" w h
  @@ U.show_pix_fmt
  @@ U.VideoFrame.pixel_format frame;

  U.VideoFrame.view_lines
    (fun iters -> 
      let f = open_out_bin "screenshot.pgm" in

      Printf.fprintf f "P5\n%d %d\n%d\n" w h 255;

      iters.(0)
      |> Seq.iter (fun line ->
             let b = bytes_of_bigarray line in
             output_bytes f b);

      close_out f;
      raise StopDecoding)
    frame

let () =
  let c =
    F.open_input
      "file://home/alex/downloads/10000000_345493607584308_4456146274528545550_n.mp4"
    (* in
       F.Container.options c
       |> fun s ->
           print_endline "open";
             Seq.iter (fun opt -> print_endline @@ U.Options.show_entry opt) s *)
  in
  let vstream = F.video_streams c |> List.hd in
  let _coder =
    F.codec_param_of_stream
      (fun param ->
        let param = Option.get param in
        let codec = C.find_de_codec param in
        C.make_decoder param codec)
      vstream
  in
  let frame_handler = C.decode _coder write_pic in

  F.read_frames ~handlers:[ (vstream, frame_handler) ] c

(* C.Coder.options _codec
   |> fun s ->
       Seq.iter (fun opt -> print_endline @@ U.Options.show_entry opt) s *)
