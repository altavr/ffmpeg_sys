let headers_avformat =
  {|
    #include <libavformat/avformat.h>
    #include <libavformat/avio.h>
    |}

let header_avcodec =
  {|
    #include <libavutil/log.h>
    #include <libavutil/opt.h>
    #include <libavcodec/avcodec.h>

  |}

let header_avutil =
  {|
  #include <errno.h>
  #include <libavutil/log.h>
  #include <libavutil/opt.h>
  #include <libavutil/frame.h>
  #include <libavutil/dict.h>
|}

let header_avswscale = {|
#include <libswscale/swscale.h>
|}

let () =
  let target, libname, file =
    match Sys.argv with
    | [| _; target; ln; file |] -> (target, ln, file)
    | _ -> failwith "Arguments error"
  in
  let oc = open_out file in
  let fmt = Format.formatter_of_out_channel oc in
  let m, headers =
    match libname with
    | "avformat" -> ((module Avformat_stubs.Bindings : Cstubs.BINDINGS), headers_avformat)
    | "avcodec" -> ((module Avcodec_stubs.Bindings), header_avcodec)
    | "avutil" -> ((module Avutil_stubs.Bindings), header_avutil)
    | "swscale" -> ((module Swscale_stubs.Bindings), header_avswscale)
    | _ -> failwith "Unknown library"
  in

  (match target with
  | "ml" ->
      Cstubs.write_ml fmt ~concurrency:Cstubs.unlocked ~prefix:libname
        m
  | "c" ->
      output_string oc headers;
      Cstubs.write_c fmt ~concurrency:Cstubs.unlocked ~prefix:libname m
  | _ -> failwith "Unknown target");

  Format.pp_print_flush fmt ();
  close_out oc
