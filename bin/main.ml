module F = Avformat
module U = Avutil
module C = Avcodec
module S = Swscale
module JC = Jpeglib.Compress

[@@@warning "-32"]

module Cont = U.Cont
open U.Cont.Syntax
open U.Cont.Infix

let bytes_of_bigarray arr =
  Bytes.init (Bigarray.Array1.dim arr) (Bigarray.Array1.unsafe_get arr)

let save_data_file arr fn =
  let f = open_out_bin fn in
  output_bytes f @@ bytes_of_bigarray arr;
  close_out f

let time_all = ref 0.
let time_encode = ref 0.

let running_time c f x =
  let start = Unix.gettimeofday () in
  let r = f x in
  c := !c +. (Unix.gettimeofday () -. start);
  r

let () =
  let ifile =
    "file://home/alex/downloads/10000000_345493607584308_4456146274528545550_n.mp4"
  in
  let ofile = "example1.jpg" in
  let tranform =
    let* ic = F.open_input ifile in
    let* ivstream = F.video_streams ic >>| List.hd in
    F.dump_format ivstream ifile;

    let* vparam = F.Stream.video_codec_param ivstream in

    let* vdecodec = C.Codec.find_video_de @@ C.Param.get_codec_id vparam in
    C.Coder.create_video_decoder vparam vdecodec 5 >>= fun (vdecoder, _) ->
    let src_width = C.Param.get_width vparam in
    let src_height = C.Param.get_height vparam in
    let src_fmt = C.Param.get_format vparam in
    let dst_width = src_width * 4 in
    let dst_height = src_height * 4 in

    let* _scaler =
      S.Scaler.create ~src_width ~src_height ~src_fmt ~dst_width ~dst_height
        ~dst_fmt:U.S.PixFmt.Rgb24 1
    in

    F.open_output ~format_name:"singlejpeg" ofile >>= fun oc ->
    let* vencodec = C.Codec.find_video_en_by_name "libopenjpeg" in
    let framerate = F.Stream.get_real_frame_rate ivstream in
    let time_base = F.Stream.get_time_base ivstream in
    let _ofmt = F.Container.output_format oc in
    print_endline "  ______________";
    print_endline @@ F.OFormat.name _ofmt;

    (* let flags =
         if F.OFormat.Flags.flag_is_set F.GlobalHeader _ofmt then
           [ C.GlobalHeader ]
         else []
       in *)
    let s = C.Codec.supported_pix_fmts vencodec in
    Seq.iter (Format.printf "%a " U.S.PixFmt.pp) s;

    (* let* opts = U.FFDict.create [ ("preset", "veryfast"); ("crf", "20") ] in *)
    let* vencoder, _rejected_opts =
      C.Coder.create_video_encoder ~framerate ~time_base
        ~pix_format:U.S.PixFmt.Rgb24 ~width:dst_width ~height:dst_height
        vencodec 5
    in
    U.FFDict.pp Format.std_formatter _rejected_opts;

    Printf.printf "threads: %d\n" @@ C.Coder.get_thread_count vencoder;
    let* veparam = C.Coder.get_video_params vencoder in

    let* ovs = F.new_video_stream ~codec:vencodec ~time_base oc in
    Format.printf "@[Video encode:@ %a@]" C.Param.pp veparam;
    let* () = F.Stream.set_codec_params veparam ovs in

    (* let* () = F.write_header oc in *)

    let ov_tb = F.Stream.get_time_base ovs in
    let iv_tb = F.Stream.get_time_base ivstream in

    (* let* scaled_frame = U.Frame.video ~width:dst_width ~height:dst_height
          ~pix_fmt:U.S.PixFmt.Yuv420p  () in
       let* () = U.Frame.set_new_buffer scaled_frame in *)
    let write_vpacket pkt =
      let* frames = C.decode vdecoder pkt in
      let* pkts =
        (* Format.printf " %a@." U.Frame.pp scaled_frame ; *)
        List.map
          (fun frame ->
            (* let* () = U.Frame.copy_props ~src:frame ~dst:scaled_frame in  *)
            let* scaled_frame = S.scale frame _scaler in

            (* Format.printf "scaled %a@." U.Frame.pp scaled_frame ; *)
            (* exit 1 |> ignore ; *)
            C.encode vencoder scaled_frame)
          frames
        |> Cont.list_rev
        >>| fun x -> List.rev x |> List.concat
      in
      (* exit 1 |> ignore ; *)
      List.map
        (fun pkt ->
          (* Format.printf "%a@." U.Rational.pp  iv_tb ;
             U.Time.of_ts iv_tb ( C.Packet.get_pts pkt   )
             |> (fun t -> Format.printf "%a@.%a@." U.Time.pp  t  C.Packet.pp  pkt ;
             Format.printf "%d@." ( U.Time.to_ts iv_tb t |> Int64.to_int ) ;
             ); *)
          C.Packet.rescale_ts ~dst:ov_tb ~src:iv_tb pkt;
          F.write_packet ovs pkt)
        pkts
      |> Cont.list_iter
    in

    let read_handler = function
      (* | F.AudioPkt pkt, _ -> write_apacket iastream oas pkt *)
      | F.VideoPkt pkt, _ -> write_vpacket pkt
      | _ -> Cont.return ()
    in

    let* packet = C.Packet.create () in

    let rec get_one_frame ic =
      let* p = F.read_packet ic packet in
      match p with
      | F.VideoPkt pkt, _ -> C.decode vdecoder pkt
      | _ ->
          print_endline "read";
          get_one_frame ic
    in

    let* frames = get_one_frame ic in
    Format.printf "%a" U.Frame.pp (List.hd frames);
    let* scaled_frame = S.scale (List.hd frames) _scaler in

    print_endline "before" ;
    let cjpeg = JC.create () in 
    print_endline "create" ;
    let pic = JC.compress ~width:(U.Frame.get_width scaled_frame) 
  ~height:(U.Frame.get_height scaled_frame) ~components:3 
  Jpeglib.Rgb  (U.Frame.get_data scaled_frame).(0)  cjpeg in 
  print_endline "compress" ;
      save_data_file pic "ex1.jpg" ;

    (* let* pkgs = C.encode vencoder scaled_frame in *)
    (* let* () = F.write_packet ovs (List.hd pkgs) in *)

    let _read_handler' = running_time time_all read_handler in

    (* let* () = F.read_frames read_handler' ic 5 in *)
    F.dump_format ovs ofile;
    U.Cont.return ()
  in

  U.run_with_exn tranform;
  Printf.printf "Transcode time - %f. Encode time - %f \n" !time_all
    !time_encode
