module type Util = sig
  type 'a typ

  module type Decl = sig
    type t

    val t : t typ
  end

  module Buffer : Decl
  module MediaType : Decl
  module SampleFmt : Decl
  module Rational : Decl
  module PixFmt : Decl
end

module Types (T : Cstubs.Types.TYPE) (U : Util with type 'a typ = 'a T.typ) =
struct
  type 'a structure = 'a Ctypes.structure
  type 'a union = 'a Ctypes.union
  type 'a typ = 'a T.typ

  open T 

  module Flag = struct
    let fast_bilinear = constant "SWS_FAST_BILINEAR" int
    let bilinear = constant "SWS_BILINEAR" int
    let bicubic = constant "SWS_BICUBIC" int
    let x = constant "SWS_X" int
    let point = constant "SWS_POINT" int
    let area = constant "SWS_AREA" int
    let bicublin = constant "SWS_BICUBLIN" int
    let gauss = constant "SWS_GAUSS" int
    let sinc = constant "SWS_SINC" int
    let lanczos = constant "SWS_LANCZOS" int
    let spline = constant "SWS_SPLINE" int
    let src_v_chr_drop_mask = constant "SWS_SRC_V_CHR_DROP_MASK" int
    let src_v_chr_drop_shift = constant "SWS_SRC_V_CHR_DROP_SHIFT" int
    let param_default = constant "SWS_PARAM_DEFAULT" int
    let print_info = constant "SWS_PRINT_INFO" int
  end

  module Filter = struct
    type filter = Filter
    type t = filter structure

    let t : t typ = structure "SwsFilter"
  end
 
  module Context = struct
    type sws_ctx = Sws_ctx
    type t = sws_ctx structure

    let t : t typ = structure "SwsContext"
  end
end
