(* open Ctypes *)

module Types (T : Ctypes.TYPE) = struct
  (* open Ctypes_static *)
  type 'a structure = 'a Ctypes.structure
  type 'a union = 'a Ctypes.union
  type 'a typ = 'a T.typ

  open T

  module type Decl = sig
    type t

    val t : t typ
  end

  module MakeEnum (E : sig
    type t

    val f : int64 -> t
  end) =
  struct
    let to_enum m k =
      let k = Int64.of_int k in
      let rec aux = function
        | [] -> None
        | (v, k') :: xs -> if Int64.equal k k' then Some v else aux xs
      in
      aux m |> Option.value ~default:(E.f k)

    let of_enum m k = List.assoc k m |> Int64.to_int
  end

  module UnixErrno = struct
    (* let eagain = constant "EAGAIN" int
       let einval = constant "EINVAL" int
       let enomem = constant "ENOMEM" int *)

    let eperm = constant "EPERM" int
    let enoent = constant "ENOENT" int
    let esrch = constant "ESRCH" int
    let eintr = constant "EINTR" int
    let eio = constant "EIO" int
    let enxio = constant "ENXIO" int
    let e2big = constant "E2BIG" int
    let enoexec = constant "ENOEXEC" int
    let ebadf = constant "EBADF" int
    let echild = constant "ECHILD" int
    let eagain = constant "EAGAIN" int
    let enomem = constant "ENOMEM" int
    let eacces = constant "EACCES" int
    let efault = constant "EFAULT" int
    let enotblk = constant "ENOTBLK" int
    let ebusy = constant "EBUSY" int
    let eexist = constant "EEXIST" int
    let exdev = constant "EXDEV" int
    let enodev = constant "ENODEV" int
    let enotdir = constant "ENOTDIR" int
    let eisdir = constant "EISDIR" int
    let einval = constant "EINVAL" int
    let enfile = constant "ENFILE" int
    let emfile = constant "EMFILE" int
    let enotty = constant "ENOTTY" int
    let etxtbsy = constant "ETXTBSY" int
    let efbig = constant "EFBIG" int
    let enospc = constant "ENOSPC" int
    let espipe = constant "ESPIPE" int
    let erofs = constant "EROFS" int
    let emlink = constant "EMLINK" int
    let epipe = constant "EPIPE" int
    let edom = constant "EDOM" int
    let erange = constant "ERANGE" int
    let edeadlk = constant "EDEADLK" int
    let enametoolong = constant "ENAMETOOLONG" int
    let enolck = constant "ENOLCK" int
    let enosys = constant "ENOSYS" int
    let enotempty = constant "ENOTEMPTY" int
    let eloop = constant "ELOOP" int
    let enomsg = constant "ENOMSG" int
    let eidrm = constant "EIDRM" int
    let echrng = constant "ECHRNG" int
    let el2nsync = constant "EL2NSYNC" int
    let el3hlt = constant "EL3HLT" int
    let el3rst = constant "EL3RST" int
    let elnrng = constant "ELNRNG" int
    let eunatch = constant "EUNATCH" int
    let enocsi = constant "ENOCSI" int
    let el2hlt = constant "EL2HLT" int
    let ebade = constant "EBADE" int
    let ebadr = constant "EBADR" int
    let exfull = constant "EXFULL" int
    let enoano = constant "ENOANO" int
    let ebadrqc = constant "EBADRQC" int
    let ebadslt = constant "EBADSLT" int
    let ebfont = constant "EBFONT" int
    let enostr = constant "ENOSTR" int
    let enodata = constant "ENODATA" int
    let etime = constant "ETIME" int
    let enosr = constant "ENOSR" int
    let enonet = constant "ENONET" int
    let enopkg = constant "ENOPKG" int
    let eremote = constant "EREMOTE" int
    let enolink = constant "ENOLINK" int
    let eadv = constant "EADV" int
    let esrmnt = constant "ESRMNT" int
    let ecomm = constant "ECOMM" int
    let eproto = constant "EPROTO" int
    let emultihop = constant "EMULTIHOP" int
    let edotdot = constant "EDOTDOT" int
    let ebadmsg = constant "EBADMSG" int
    let eoverflow = constant "EOVERFLOW" int
    let enotuniq = constant "ENOTUNIQ" int
    let ebadfd = constant "EBADFD" int
    let eremchg = constant "EREMCHG" int
    let elibacc = constant "ELIBACC" int
    let elibbad = constant "ELIBBAD" int
    let elibscn = constant "ELIBSCN" int
    let elibmax = constant "ELIBMAX" int
    let elibexec = constant "ELIBEXEC" int
    let eilseq = constant "EILSEQ" int
    let erestart = constant "ERESTART" int
    let estrpipe = constant "ESTRPIPE" int
    let eusers = constant "EUSERS" int
    let enotsock = constant "ENOTSOCK" int
    let edestaddrreq = constant "EDESTADDRREQ" int
    let emsgsize = constant "EMSGSIZE" int
    let eprototype = constant "EPROTOTYPE" int
    let enoprotoopt = constant "ENOPROTOOPT" int
    let eprotonosupport = constant "EPROTONOSUPPORT" int
    let esocktnosupport = constant "ESOCKTNOSUPPORT" int
    let eopnotsupp = constant "EOPNOTSUPP" int
    let epfnosupport = constant "EPFNOSUPPORT" int
    let eafnosupport = constant "EAFNOSUPPORT" int
    let eaddrinuse = constant "EADDRINUSE" int
    let eaddrnotavail = constant "EADDRNOTAVAIL" int
    let enetdown = constant "ENETDOWN" int
    let enetunreach = constant "ENETUNREACH" int
    let enetreset = constant "ENETRESET" int
    let econnaborted = constant "ECONNABORTED" int
    let econnreset = constant "ECONNRESET" int
    let enobufs = constant "ENOBUFS" int
    let eisconn = constant "EISCONN" int
    let enotconn = constant "ENOTCONN" int
    let eshutdown = constant "ESHUTDOWN" int
    let etoomanyrefs = constant "ETOOMANYREFS" int
    let etimedout = constant "ETIMEDOUT" int
    let econnrefused = constant "ECONNREFUSED" int
    let ehostdown = constant "EHOSTDOWN" int
    let ehostunreach = constant "EHOSTUNREACH" int
    let ealready = constant "EALREADY" int
    let einprogress = constant "EINPROGRESS" int
    let estale = constant "ESTALE" int
    let euclean = constant "EUCLEAN" int
    let enotnam = constant "ENOTNAM" int
    let enavail = constant "ENAVAIL" int
    let eisnam = constant "EISNAM" int
    let eremoteio = constant "EREMOTEIO" int
    let edquot = constant "EDQUOT" int
    let enomedium = constant "ENOMEDIUM" int
    let emediumtype = constant "EMEDIUMTYPE" int
    let ecanceled = constant "ECANCELED" int
    let enokey = constant "ENOKEY" int
    let ekeyexpired = constant "EKEYEXPIRED" int
    let ekeyrevoked = constant "EKEYREVOKED" int
    let ekeyrejected = constant "EKEYREJECTED" int
    let eownerdead = constant "EOWNERDEAD" int
    let enotrecoverable = constant "ENOTRECOVERABLE" int
    let seek_set = constant "SEEK_SET" int
  end

  module ErrorCode = struct
    let bsf_not_found = constant "AVERROR_BSF_NOT_FOUND" int
    let bug = constant "AVERROR_BUG" int
    let buffer_too_small = constant "AVERROR_BUFFER_TOO_SMALL" int
    let decoder_not_found = constant "AVERROR_DECODER_NOT_FOUND" int
    let demuxer_not_found = constant "AVERROR_DEMUXER_NOT_FOUND" int
    let encoder_not_found = constant "AVERROR_ENCODER_NOT_FOUND" int
    let eof = constant "AVERROR_EOF" int
    let exit = constant "AVERROR_EXIT" int
    let external_ = constant "AVERROR_EXTERNAL" int
    let filter_not_found = constant "AVERROR_FILTER_NOT_FOUND" int
    let invaliddata = constant "AVERROR_INVALIDDATA" int
    let muxer_not_found = constant "AVERROR_MUXER_NOT_FOUND" int
    let option_not_found = constant "AVERROR_OPTION_NOT_FOUND" int
    let patchwelcome = constant "AVERROR_PATCHWELCOME" int
    let protocol_not_found = constant "AVERROR_PROTOCOL_NOT_FOUND" int
    let stream_not_found = constant "AVERROR_STREAM_NOT_FOUND" int
    let bug2 = constant "AVERROR_BUG2" int
    let unknown = constant "AVERROR_UNKNOWN" int
    let experimental = constant "AVERROR_EXPERIMENTAL" int
    let input_changed = constant "AVERROR_INPUT_CHANGED" int
    let output_changed = constant "AVERROR_OUTPUT_CHANGED" int
    let http_bad_request = constant "AVERROR_HTTP_BAD_REQUEST" int
    let http_unauthorized = constant "AVERROR_HTTP_UNAUTHORIZED" int
    let http_forbidden = constant "AVERROR_HTTP_FORBIDDEN" int
    let http_not_found = constant "AVERROR_HTTP_NOT_FOUND" int
    let http_other_4xx = constant "AVERROR_HTTP_OTHER_4XX" int
    let http_server_error = constant "AVERROR_HTTP_SERVER_ERROR" int
  end

  module MediaType = struct
    let type_video = constant "AVMEDIA_TYPE_VIDEO" int64_t
    let type_audio = constant "AVMEDIA_TYPE_AUDIO" int64_t
    let type_subtitle = constant "AVMEDIA_TYPE_SUBTITLE" int64_t

    type t = TYPE_VIDEO | TYPE_AUDIO | TYPE_SUBTITLE
    [@@deriving show { with_path = false }]

    let t : t typ =
      enum "AVMediaType"
        [
          (TYPE_VIDEO, type_video);
          (TYPE_AUDIO, type_audio);
          (TYPE_SUBTITLE, type_subtitle);
        ]
  end

  module Rational = struct
    type rational
    type t = rational structure

    let t : t typ = structure "AVRational"
    let num = field t "num" int
    let den = field t "den" int
    let () = seal t
  end

  module Opt_type = struct
    let flags = constant "AV_OPT_TYPE_FLAGS" int64_t
    and int_ = constant "AV_OPT_TYPE_INT" int64_t

    let int64 = constant "AV_OPT_TYPE_INT64" int64_t
    let double = constant "AV_OPT_TYPE_DOUBLE" int64_t
    let float = constant "AV_OPT_TYPE_FLOAT" int64_t
    let string = constant "AV_OPT_TYPE_STRING" int64_t
    let rational = constant "AV_OPT_TYPE_RATIONAL" int64_t
    let binary = constant "AV_OPT_TYPE_BINARY" int64_t
    let dict = constant "AV_OPT_TYPE_DICT" int64_t
    let uint64 = constant "AV_OPT_TYPE_UINT64" int64_t
    let const = constant "AV_OPT_TYPE_CONST" int64_t
    let image_size = constant "AV_OPT_TYPE_IMAGE_SIZE" int64_t
    let pixel_fmt = constant "AV_OPT_TYPE_PIXEL_FMT" int64_t
    let sample_fmt = constant "AV_OPT_TYPE_SAMPLE_FMT" int64_t
    let video_rate = constant "AV_OPT_TYPE_VIDEO_RATE" int64_t
    let duration = constant "AV_OPT_TYPE_DURATION" int64_t
    let color = constant "AV_OPT_TYPE_COLOR" int64_t
    let channel_layout = constant "AV_OPT_TYPE_CHANNEL_LAYOUT" int64_t
    let bool = constant "AV_OPT_TYPE_BOOL" int64_t

    type t =
      | Flags
      | Int
      | Int64
      | Double
      | Float
      | String
      | Rational
      | Binary
      | Dict
      | Uint64
      | Const
      | Image_Size
      | Pixel_Fmt
      | Sample_Fmt
      | Video_Rate
      | Duration
      | Color
      | Channel_Layout
      | Bool
      | UnexpectFlag
    [@@deriving show { with_path = false }]

    let m =
      [
        (Flags, flags);
        (Int, int_);
        (Int64, int64);
        (Double, double);
        (Float, float);
        (String, string);
        (Rational, rational);
        (Binary, binary);
        (Dict, dict);
        (Uint64, uint64);
        (Const, const);
        (Image_Size, image_size);
        (Pixel_Fmt, pixel_fmt);
        (Sample_Fmt, sample_fmt);
        (Video_Rate, video_rate);
        (Duration, duration);
        (Color, color);
        (Channel_Layout, channel_layout);
        (Bool, bool);
      ]

    let t : t typ = enum "AVOptionType" m ~unexpected:(fun _ -> UnexpectFlag)
  end

  module OptSearchFlag = struct
    let search_children = constant "AV_OPT_SEARCH_CHILDREN" int
    let search_fake_obj = constant "AV_OPT_SEARCH_FAKE_OBJ" int
    let allow_null = constant "AV_OPT_ALLOW_NULL" int
    let multi_component_range = constant "AV_OPT_MULTI_COMPONENT_RANGE" int
    let serialize_skip_defaults = constant "AV_OPT_SERIALIZE_SKIP_DEFAULTS" int

    let serialize_opt_flags_exact =
      constant "AV_OPT_SERIALIZE_OPT_FLAGS_EXACT" int
  end

  module PixFmt = struct
    let none = constant "AV_PIX_FMT_NONE" int64_t
    and yuv420p = constant "AV_PIX_FMT_YUV420P" int64_t
    and yuyv422 = constant "AV_PIX_FMT_YUYV422" int64_t
    and rgb24 = constant "AV_PIX_FMT_RGB24" int64_t
    and bgr24 = constant "AV_PIX_FMT_BGR24" int64_t
    and yuv422p = constant "AV_PIX_FMT_YUV422P" int64_t
    and yuv444p = constant "AV_PIX_FMT_YUV444P" int64_t
    and yuv410p = constant "AV_PIX_FMT_YUV410P" int64_t
    and yuv411p = constant "AV_PIX_FMT_YUV411P" int64_t
    and gray8 = constant "AV_PIX_FMT_GRAY8" int64_t
    and monowhite = constant "AV_PIX_FMT_MONOWHITE" int64_t
    and monoblack = constant "AV_PIX_FMT_MONOBLACK" int64_t
    and pal8 = constant "AV_PIX_FMT_PAL8" int64_t
    and yuvj420p = constant "AV_PIX_FMT_YUVJ420P" int64_t
    and yuvj422p = constant "AV_PIX_FMT_YUVJ422P" int64_t
    and yuvj444p = constant "AV_PIX_FMT_YUVJ444P" int64_t
    and uyvy422 = constant "AV_PIX_FMT_UYVY422" int64_t
    and uyyvyy411 = constant "AV_PIX_FMT_UYYVYY411" int64_t
    and bgr8 = constant "AV_PIX_FMT_BGR8" int64_t
    and bgr4 = constant "AV_PIX_FMT_BGR4" int64_t
    and bgr4_byte = constant "AV_PIX_FMT_BGR4_BYTE" int64_t
    and rgb8 = constant "AV_PIX_FMT_RGB8" int64_t
    and rgb4 = constant "AV_PIX_FMT_RGB4" int64_t
    and rgb4_byte = constant "AV_PIX_FMT_RGB4_BYTE" int64_t
    and nv12 = constant "AV_PIX_FMT_NV12" int64_t
    and nv21 = constant "AV_PIX_FMT_NV21" int64_t
    and argb = constant "AV_PIX_FMT_ARGB" int64_t
    and rgba = constant "AV_PIX_FMT_RGBA" int64_t
    and abgr = constant "AV_PIX_FMT_ABGR" int64_t
    and bgra = constant "AV_PIX_FMT_BGRA" int64_t
    and gray16be = constant "AV_PIX_FMT_GRAY16BE" int64_t
    and gray16le = constant "AV_PIX_FMT_GRAY16LE" int64_t
    and yuv440p = constant "AV_PIX_FMT_YUV440P" int64_t
    and yuvj440p = constant "AV_PIX_FMT_YUVJ440P" int64_t
    and yuva420p = constant "AV_PIX_FMT_YUVA420P" int64_t
    and rgb48be = constant "AV_PIX_FMT_RGB48BE" int64_t
    and rgb48le = constant "AV_PIX_FMT_RGB48LE" int64_t
    and rgb565be = constant "AV_PIX_FMT_RGB565BE" int64_t
    and rgb565le = constant "AV_PIX_FMT_RGB565LE" int64_t
    and rgb555be = constant "AV_PIX_FMT_RGB555BE" int64_t
    and rgb555le = constant "AV_PIX_FMT_RGB555LE" int64_t
    and bgr565be = constant "AV_PIX_FMT_BGR565BE" int64_t
    and bgr565le = constant "AV_PIX_FMT_BGR565LE" int64_t
    and bgr555be = constant "AV_PIX_FMT_BGR555BE" int64_t
    and bgr555le = constant "AV_PIX_FMT_BGR555LE" int64_t
    and vaapi_moco = constant "AV_PIX_FMT_VAAPI_MOCO" int64_t
    and vaapi_idct = constant "AV_PIX_FMT_VAAPI_IDCT" int64_t
    and vaapi_vld = constant "AV_PIX_FMT_VAAPI_VLD" int64_t
    and vaapi = constant "AV_PIX_FMT_VAAPI" int64_t
    and yuv420p16le = constant "AV_PIX_FMT_YUV420P16LE" int64_t
    and yuv420p16be = constant "AV_PIX_FMT_YUV420P16BE" int64_t
    and yuv422p16le = constant "AV_PIX_FMT_YUV422P16LE" int64_t
    and yuv422p16be = constant "AV_PIX_FMT_YUV422P16BE" int64_t
    and yuv444p16le = constant "AV_PIX_FMT_YUV444P16LE" int64_t
    and yuv444p16be = constant "AV_PIX_FMT_YUV444P16BE" int64_t
    and dxva2_vld = constant "AV_PIX_FMT_DXVA2_VLD" int64_t
    and rgb444le = constant "AV_PIX_FMT_RGB444LE" int64_t
    and rgb444be = constant "AV_PIX_FMT_RGB444BE" int64_t
    and bgr444le = constant "AV_PIX_FMT_BGR444LE" int64_t
    and bgr444be = constant "AV_PIX_FMT_BGR444BE" int64_t
    and ya8 = constant "AV_PIX_FMT_YA8" int64_t
    and y400a = constant "AV_PIX_FMT_Y400A" int64_t
    and gray8a = constant "AV_PIX_FMT_GRAY8A" int64_t
    and bgr48be = constant "AV_PIX_FMT_BGR48BE" int64_t
    and bgr48le = constant "AV_PIX_FMT_BGR48LE" int64_t
    and yuv420p9be = constant "AV_PIX_FMT_YUV420P9BE" int64_t
    and yuv420p9le = constant "AV_PIX_FMT_YUV420P9LE" int64_t
    and yuv420p10be = constant "AV_PIX_FMT_YUV420P10BE" int64_t
    and yuv420p10le = constant "AV_PIX_FMT_YUV420P10LE" int64_t
    and yuv422p10be = constant "AV_PIX_FMT_YUV422P10BE" int64_t
    and yuv422p10le = constant "AV_PIX_FMT_YUV422P10LE" int64_t
    and yuv444p9be = constant "AV_PIX_FMT_YUV444P9BE" int64_t
    and yuv444p9le = constant "AV_PIX_FMT_YUV444P9LE" int64_t
    and yuv444p10be = constant "AV_PIX_FMT_YUV444P10BE" int64_t
    and yuv444p10le = constant "AV_PIX_FMT_YUV444P10LE" int64_t
    and yuv422p9be = constant "AV_PIX_FMT_YUV422P9BE" int64_t
    and yuv422p9le = constant "AV_PIX_FMT_YUV422P9LE" int64_t
    and gbrp = constant "AV_PIX_FMT_GBRP" int64_t
    and gbr24p = constant "AV_PIX_FMT_GBR24P" int64_t
    and gbrp9be = constant "AV_PIX_FMT_GBRP9BE" int64_t
    and gbrp9le = constant "AV_PIX_FMT_GBRP9LE" int64_t
    and gbrp10be = constant "AV_PIX_FMT_GBRP10BE" int64_t
    and gbrp10le = constant "AV_PIX_FMT_GBRP10LE" int64_t
    and gbrp16be = constant "AV_PIX_FMT_GBRP16BE" int64_t
    and gbrp16le = constant "AV_PIX_FMT_GBRP16LE" int64_t
    and yuva422p = constant "AV_PIX_FMT_YUVA422P" int64_t
    and yuva444p = constant "AV_PIX_FMT_YUVA444P" int64_t
    and yuva420p9be = constant "AV_PIX_FMT_YUVA420P9BE" int64_t
    and yuva420p9le = constant "AV_PIX_FMT_YUVA420P9LE" int64_t
    and yuva422p9be = constant "AV_PIX_FMT_YUVA422P9BE" int64_t
    and yuva422p9le = constant "AV_PIX_FMT_YUVA422P9LE" int64_t
    and yuva444p9be = constant "AV_PIX_FMT_YUVA444P9BE" int64_t
    and yuva444p9le = constant "AV_PIX_FMT_YUVA444P9LE" int64_t
    and yuva420p10be = constant "AV_PIX_FMT_YUVA420P10BE" int64_t
    and yuva420p10le = constant "AV_PIX_FMT_YUVA420P10LE" int64_t
    and yuva422p10be = constant "AV_PIX_FMT_YUVA422P10BE" int64_t
    and yuva422p10le = constant "AV_PIX_FMT_YUVA422P10LE" int64_t
    and yuva444p10be = constant "AV_PIX_FMT_YUVA444P10BE" int64_t
    and yuva444p10le = constant "AV_PIX_FMT_YUVA444P10LE" int64_t
    and yuva420p16be = constant "AV_PIX_FMT_YUVA420P16BE" int64_t
    and yuva420p16le = constant "AV_PIX_FMT_YUVA420P16LE" int64_t
    and yuva422p16be = constant "AV_PIX_FMT_YUVA422P16BE" int64_t
    and yuva422p16le = constant "AV_PIX_FMT_YUVA422P16LE" int64_t
    and yuva444p16be = constant "AV_PIX_FMT_YUVA444P16BE" int64_t
    and yuva444p16le = constant "AV_PIX_FMT_YUVA444P16LE" int64_t
    and vdpau = constant "AV_PIX_FMT_VDPAU" int64_t
    and xyz12le = constant "AV_PIX_FMT_XYZ12LE" int64_t
    and xyz12be = constant "AV_PIX_FMT_XYZ12BE" int64_t
    and nv16 = constant "AV_PIX_FMT_NV16" int64_t
    and nv20le = constant "AV_PIX_FMT_NV20LE" int64_t
    and nv20be = constant "AV_PIX_FMT_NV20BE" int64_t
    and rgba64be = constant "AV_PIX_FMT_RGBA64BE" int64_t
    and rgba64le = constant "AV_PIX_FMT_RGBA64LE" int64_t
    and bgra64be = constant "AV_PIX_FMT_BGRA64BE" int64_t
    and bgra64le = constant "AV_PIX_FMT_BGRA64LE" int64_t
    and yvyu422 = constant "AV_PIX_FMT_YVYU422" int64_t
    and ya16be = constant "AV_PIX_FMT_YA16BE" int64_t
    and ya16le = constant "AV_PIX_FMT_YA16LE" int64_t
    and gbrap = constant "AV_PIX_FMT_GBRAP" int64_t
    and gbrap16be = constant "AV_PIX_FMT_GBRAP16BE" int64_t
    and gbrap16le = constant "AV_PIX_FMT_GBRAP16LE" int64_t
    and qsv = constant "AV_PIX_FMT_QSV" int64_t
    and mmal = constant "AV_PIX_FMT_MMAL" int64_t
    and d3d11va_vld = constant "AV_PIX_FMT_D3D11VA_VLD" int64_t
    and cuda = constant "AV_PIX_FMT_CUDA" int64_t
    and _0rgb = constant "AV_PIX_FMT_0RGB" int64_t
    and rgb0 = constant "AV_PIX_FMT_RGB0" int64_t
    and _0bgr = constant "AV_PIX_FMT_0BGR" int64_t
    and bgr0 = constant "AV_PIX_FMT_BGR0" int64_t
    and yuv420p12be = constant "AV_PIX_FMT_YUV420P12BE" int64_t
    and yuv420p12le = constant "AV_PIX_FMT_YUV420P12LE" int64_t
    and yuv420p14be = constant "AV_PIX_FMT_YUV420P14BE" int64_t
    and yuv420p14le = constant "AV_PIX_FMT_YUV420P14LE" int64_t
    and yuv422p12be = constant "AV_PIX_FMT_YUV422P12BE" int64_t
    and yuv422p12le = constant "AV_PIX_FMT_YUV422P12LE" int64_t
    and yuv422p14be = constant "AV_PIX_FMT_YUV422P14BE" int64_t
    and yuv422p14le = constant "AV_PIX_FMT_YUV422P14LE" int64_t
    and yuv444p12be = constant "AV_PIX_FMT_YUV444P12BE" int64_t
    and yuv444p12le = constant "AV_PIX_FMT_YUV444P12LE" int64_t
    and yuv444p14be = constant "AV_PIX_FMT_YUV444P14BE" int64_t
    and yuv444p14le = constant "AV_PIX_FMT_YUV444P14LE" int64_t
    and gbrp12be = constant "AV_PIX_FMT_GBRP12BE" int64_t
    and gbrp12le = constant "AV_PIX_FMT_GBRP12LE" int64_t
    and gbrp14be = constant "AV_PIX_FMT_GBRP14BE" int64_t
    and gbrp14le = constant "AV_PIX_FMT_GBRP14LE" int64_t
    and yuvj411p = constant "AV_PIX_FMT_YUVJ411P" int64_t
    and bayer_bggr8 = constant "AV_PIX_FMT_BAYER_BGGR8" int64_t
    and bayer_rggb8 = constant "AV_PIX_FMT_BAYER_RGGB8" int64_t
    and bayer_gbrg8 = constant "AV_PIX_FMT_BAYER_GBRG8" int64_t
    and bayer_grbg8 = constant "AV_PIX_FMT_BAYER_GRBG8" int64_t
    and bayer_bggr16le = constant "AV_PIX_FMT_BAYER_BGGR16LE" int64_t
    and bayer_bggr16be = constant "AV_PIX_FMT_BAYER_BGGR16BE" int64_t
    and bayer_rggb16le = constant "AV_PIX_FMT_BAYER_RGGB16LE" int64_t
    and bayer_rggb16be = constant "AV_PIX_FMT_BAYER_RGGB16BE" int64_t
    and bayer_gbrg16le = constant "AV_PIX_FMT_BAYER_GBRG16LE" int64_t
    and bayer_gbrg16be = constant "AV_PIX_FMT_BAYER_GBRG16BE" int64_t
    and bayer_grbg16le = constant "AV_PIX_FMT_BAYER_GRBG16LE" int64_t
    and bayer_grbg16be = constant "AV_PIX_FMT_BAYER_GRBG16BE" int64_t
    and xvmc = constant "AV_PIX_FMT_XVMC" int64_t
    and yuv440p10le = constant "AV_PIX_FMT_YUV440P10LE" int64_t
    and yuv440p10be = constant "AV_PIX_FMT_YUV440P10BE" int64_t
    and yuv440p12le = constant "AV_PIX_FMT_YUV440P12LE" int64_t
    and yuv440p12be = constant "AV_PIX_FMT_YUV440P12BE" int64_t
    and ayuv64le = constant "AV_PIX_FMT_AYUV64LE" int64_t
    and ayuv64be = constant "AV_PIX_FMT_AYUV64BE" int64_t
    and videotoolbox = constant "AV_PIX_FMT_VIDEOTOOLBOX" int64_t
    and p010le = constant "AV_PIX_FMT_P010LE" int64_t
    and p010be = constant "AV_PIX_FMT_P010BE" int64_t
    and gbrap12be = constant "AV_PIX_FMT_GBRAP12BE" int64_t
    and gbrap12le = constant "AV_PIX_FMT_GBRAP12LE" int64_t
    and gbrap10be = constant "AV_PIX_FMT_GBRAP10BE" int64_t
    and gbrap10le = constant "AV_PIX_FMT_GBRAP10LE" int64_t
    and mediacodec = constant "AV_PIX_FMT_MEDIACODEC" int64_t
    and gray12be = constant "AV_PIX_FMT_GRAY12BE" int64_t
    and gray12le = constant "AV_PIX_FMT_GRAY12LE" int64_t
    and gray10be = constant "AV_PIX_FMT_GRAY10BE" int64_t
    and gray10le = constant "AV_PIX_FMT_GRAY10LE" int64_t
    and p016le = constant "AV_PIX_FMT_P016LE" int64_t
    and p016be = constant "AV_PIX_FMT_P016BE" int64_t
    and d3d11 = constant "AV_PIX_FMT_D3D11" int64_t
    and gray9be = constant "AV_PIX_FMT_GRAY9BE" int64_t
    and gray9le = constant "AV_PIX_FMT_GRAY9LE" int64_t
    and gbrpf32be = constant "AV_PIX_FMT_GBRPF32BE" int64_t
    and gbrpf32le = constant "AV_PIX_FMT_GBRPF32LE" int64_t
    and gbrapf32be = constant "AV_PIX_FMT_GBRAPF32BE" int64_t
    and gbrapf32le = constant "AV_PIX_FMT_GBRAPF32LE" int64_t
    and drm_prime = constant "AV_PIX_FMT_DRM_PRIME" int64_t
    and opencl = constant "AV_PIX_FMT_OPENCL" int64_t
    and gray14be = constant "AV_PIX_FMT_GRAY14BE" int64_t
    and gray14le = constant "AV_PIX_FMT_GRAY14LE" int64_t
    and grayf32be = constant "AV_PIX_FMT_GRAYF32BE" int64_t
    and grayf32le = constant "AV_PIX_FMT_GRAYF32LE" int64_t
    and nb = constant "AV_PIX_FMT_NB" int64_t

    type t =
      | None
      | Yuv420p
      | Yuyv422
      | Rgb24
      | Bgr24
      | Yuv422p
      | Yuv444p
      | Yuv410p
      | Yuv411p
      | Gray8
      | Monowhite
      | Monoblack
      | Pal8
      | Yuvj420p
      | Yuvj422p
      | Yuvj444p
      | Uyvy422
      | Uyyvyy411
      | Bgr8
      | Bgr4
      | Bgr4_byte
      | Rgb8
      | Rgb4
      | Rgb4_byte
      | Nv12
      | Nv21
      | Argb
      | Rgba
      | Abgr
      | Bgra
      | Gray16be
      | Gray16le
      | Yuv440p
      | Yuvj440p
      | Yuva420p
      | Rgb48be
      | Rgb48le
      | Rgb565be
      | Rgb565le
      | Rgb555be
      | Rgb555le
      | Bgr565be
      | Bgr565le
      | Bgr555be
      | Bgr555le
      | Vaapi_moco
      | Vaapi_idct
      | Vaapi_vld
      | Vaapi
      | Yuv420p16le
      | Yuv420p16be
      | Yuv422p16le
      | Yuv422p16be
      | Yuv444p16le
      | Yuv444p16be
      | Dxva2_vld
      | Rgb444le
      | Rgb444be
      | Bgr444le
      | Bgr444be
      | Ya8
      | Y400a
      | Gray8a
      | Bgr48be
      | Bgr48le
      | Yuv420p9be
      | Yuv420p9le
      | Yuv420p10be
      | Yuv420p10le
      | Yuv422p10be
      | Yuv422p10le
      | Yuv444p9be
      | Yuv444p9le
      | Yuv444p10be
      | Yuv444p10le
      | Yuv422p9be
      | Yuv422p9le
      | Gbrp
      | Gbr24p
      | Gbrp9be
      | Gbrp9le
      | Gbrp10be
      | Gbrp10le
      | Gbrp16be
      | Gbrp16le
      | Yuva422p
      | Yuva444p
      | Yuva420p9be
      | Yuva420p9le
      | Yuva422p9be
      | Yuva422p9le
      | Yuva444p9be
      | Yuva444p9le
      | Yuva420p10be
      | Yuva420p10le
      | Yuva422p10be
      | Yuva422p10le
      | Yuva444p10be
      | Yuva444p10le
      | Yuva420p16be
      | Yuva420p16le
      | Yuva422p16be
      | Yuva422p16le
      | Yuva444p16be
      | Yuva444p16le
      | Vdpau
      | Xyz12le
      | Xyz12be
      | Nv16
      | Nv20le
      | Nv20be
      | Rgba64be
      | Rgba64le
      | Bgra64be
      | Bgra64le
      | Yvyu422
      | Ya16be
      | Ya16le
      | Gbrap
      | Gbrap16be
      | Gbrap16le
      | Qsv
      | Mmal
      | D3d11va_vld
      | Cuda
      | F_0rgb
      | Rgb0
      | F_0bgr
      | Bgr0
      | Yuv420p12be
      | Yuv420p12le
      | Yuv420p14be
      | Yuv420p14le
      | Yuv422p12be
      | Yuv422p12le
      | Yuv422p14be
      | Yuv422p14le
      | Yuv444p12be
      | Yuv444p12le
      | Yuv444p14be
      | Yuv444p14le
      | Gbrp12be
      | Gbrp12le
      | Gbrp14be
      | Gbrp14le
      | Yuvj411p
      | Bayer_bggr8
      | Bayer_rggb8
      | Bayer_gbrg8
      | Bayer_grbg8
      | Bayer_bggr16le
      | Bayer_bggr16be
      | Bayer_rggb16le
      | Bayer_rggb16be
      | Bayer_gbrg16le
      | Bayer_gbrg16be
      | Bayer_grbg16le
      | Bayer_grbg16be
      | Xvmc
      | Yuv440p10le
      | Yuv440p10be
      | Yuv440p12le
      | Yuv440p12be
      | Ayuv64le
      | Ayuv64be
      | Videotoolbox
      | P010le
      | P010be
      | Gbrap12be
      | Gbrap12le
      | Gbrap10be
      | Gbrap10le
      | Mediacodec
      | Gray12be
      | Gray12le
      | Gray10be
      | Gray10le
      | P016le
      | P016be
      | D3d11
      | Gray9be
      | Gray9le
      | Gbrpf32be
      | Gbrpf32le
      | Gbrapf32be
      | Gbrapf32le
      | Drm_prime
      | Opencl
      | Gray14be
      | Gray14le
      | Grayf32be
      | Grayf32le
      | Nb
    [@@deriving show { with_path = false }]

    let m =
      [
        (None, none);
        (Yuv420p, yuv420p);
        (Yuyv422, yuyv422);
        (Rgb24, rgb24);
        (Bgr24, bgr24);
        (Yuv422p, yuv422p);
        (Yuv444p, yuv444p);
        (Yuv410p, yuv410p);
        (Yuv411p, yuv411p);
        (Gray8, gray8);
        (Monowhite, monowhite);
        (Monoblack, monoblack);
        (Pal8, pal8);
        (Yuvj420p, yuvj420p);
        (Yuvj422p, yuvj422p);
        (Yuvj444p, yuvj444p);
        (Uyvy422, uyvy422);
        (Uyyvyy411, uyyvyy411);
        (Bgr8, bgr8);
        (Bgr4, bgr4);
        (Bgr4_byte, bgr4_byte);
        (Rgb8, rgb8);
        (Rgb4, rgb4);
        (Rgb4_byte, rgb4_byte);
        (Nv12, nv12);
        (Nv21, nv21);
        (Argb, argb);
        (Rgba, rgba);
        (Abgr, abgr);
        (Bgra, bgra);
        (Gray16be, gray16be);
        (Gray16le, gray16le);
        (Yuv440p, yuv440p);
        (Yuvj440p, yuvj440p);
        (Yuva420p, yuva420p);
        (Rgb48be, rgb48be);
        (Rgb48le, rgb48le);
        (Rgb565be, rgb565be);
        (Rgb565le, rgb565le);
        (Rgb555be, rgb555be);
        (Rgb555le, rgb555le);
        (Bgr565be, bgr565be);
        (Bgr565le, bgr565le);
        (Bgr555be, bgr555be);
        (Bgr555le, bgr555le);
        (Vaapi_moco, vaapi_moco);
        (Vaapi_idct, vaapi_idct);
        (Vaapi_vld, vaapi_vld);
        (Vaapi, vaapi);
        (Yuv420p16le, yuv420p16le);
        (Yuv420p16be, yuv420p16be);
        (Yuv422p16le, yuv422p16le);
        (Yuv422p16be, yuv422p16be);
        (Yuv444p16le, yuv444p16le);
        (Yuv444p16be, yuv444p16be);
        (Dxva2_vld, dxva2_vld);
        (Rgb444le, rgb444le);
        (Rgb444be, rgb444be);
        (Bgr444le, bgr444le);
        (Bgr444be, bgr444be);
        (Ya8, ya8);
        (Y400a, y400a);
        (Gray8a, gray8a);
        (Bgr48be, bgr48be);
        (Bgr48le, bgr48le);
        (Yuv420p9be, yuv420p9be);
        (Yuv420p9le, yuv420p9le);
        (Yuv420p10be, yuv420p10be);
        (Yuv420p10le, yuv420p10le);
        (Yuv422p10be, yuv422p10be);
        (Yuv422p10le, yuv422p10le);
        (Yuv444p9be, yuv444p9be);
        (Yuv444p9le, yuv444p9le);
        (Yuv444p10be, yuv444p10be);
        (Yuv444p10le, yuv444p10le);
        (Yuv422p9be, yuv422p9be);
        (Yuv422p9le, yuv422p9le);
        (Gbrp, gbrp);
        (Gbr24p, gbr24p);
        (Gbrp9be, gbrp9be);
        (Gbrp9le, gbrp9le);
        (Gbrp10be, gbrp10be);
        (Gbrp10le, gbrp10le);
        (Gbrp16be, gbrp16be);
        (Gbrp16le, gbrp16le);
        (Yuva422p, yuva422p);
        (Yuva444p, yuva444p);
        (Yuva420p9be, yuva420p9be);
        (Yuva420p9le, yuva420p9le);
        (Yuva422p9be, yuva422p9be);
        (Yuva422p9le, yuva422p9le);
        (Yuva444p9be, yuva444p9be);
        (Yuva444p9le, yuva444p9le);
        (Yuva420p10be, yuva420p10be);
        (Yuva420p10le, yuva420p10le);
        (Yuva422p10be, yuva422p10be);
        (Yuva422p10le, yuva422p10le);
        (Yuva444p10be, yuva444p10be);
        (Yuva444p10le, yuva444p10le);
        (Yuva420p16be, yuva420p16be);
        (Yuva420p16le, yuva420p16le);
        (Yuva422p16be, yuva422p16be);
        (Yuva422p16le, yuva422p16le);
        (Yuva444p16be, yuva444p16be);
        (Yuva444p16le, yuva444p16le);
        (Vdpau, vdpau);
        (Xyz12le, xyz12le);
        (Xyz12be, xyz12be);
        (Nv16, nv16);
        (Nv20le, nv20le);
        (Nv20be, nv20be);
        (Rgba64be, rgba64be);
        (Rgba64le, rgba64le);
        (Bgra64be, bgra64be);
        (Bgra64le, bgra64le);
        (Yvyu422, yvyu422);
        (Ya16be, ya16be);
        (Ya16le, ya16le);
        (Gbrap, gbrap);
        (Gbrap16be, gbrap16be);
        (Gbrap16le, gbrap16le);
        (Qsv, qsv);
        (Mmal, mmal);
        (D3d11va_vld, d3d11va_vld);
        (Cuda, cuda);
        (F_0rgb, _0rgb);
        (Rgb0, rgb0);
        (F_0bgr, _0bgr);
        (Bgr0, bgr0);
        (Yuv420p12be, yuv420p12be);
        (Yuv420p12le, yuv420p12le);
        (Yuv420p14be, yuv420p14be);
        (Yuv420p14le, yuv420p14le);
        (Yuv422p12be, yuv422p12be);
        (Yuv422p12le, yuv422p12le);
        (Yuv422p14be, yuv422p14be);
        (Yuv422p14le, yuv422p14le);
        (Yuv444p12be, yuv444p12be);
        (Yuv444p12le, yuv444p12le);
        (Yuv444p14be, yuv444p14be);
        (Yuv444p14le, yuv444p14le);
        (Gbrp12be, gbrp12be);
        (Gbrp12le, gbrp12le);
        (Gbrp14be, gbrp14be);
        (Gbrp14le, gbrp14le);
        (Yuvj411p, yuvj411p);
        (Bayer_bggr8, bayer_bggr8);
        (Bayer_rggb8, bayer_rggb8);
        (Bayer_gbrg8, bayer_gbrg8);
        (Bayer_grbg8, bayer_grbg8);
        (Bayer_bggr16le, bayer_bggr16le);
        (Bayer_bggr16be, bayer_bggr16be);
        (Bayer_rggb16le, bayer_rggb16le);
        (Bayer_rggb16be, bayer_rggb16be);
        (Bayer_gbrg16le, bayer_gbrg16le);
        (Bayer_gbrg16be, bayer_gbrg16be);
        (Bayer_grbg16le, bayer_grbg16le);
        (Bayer_grbg16be, bayer_grbg16be);
        (Xvmc, xvmc);
        (Yuv440p10le, yuv440p10le);
        (Yuv440p10be, yuv440p10be);
        (Yuv440p12le, yuv440p12le);
        (Yuv440p12be, yuv440p12be);
        (Ayuv64le, ayuv64le);
        (Ayuv64be, ayuv64be);
        (Videotoolbox, videotoolbox);
        (P010le, p010le);
        (P010be, p010be);
        (Gbrap12be, gbrap12be);
        (Gbrap12le, gbrap12le);
        (Gbrap10be, gbrap10be);
        (Gbrap10le, gbrap10le);
        (Mediacodec, mediacodec);
        (Gray12be, gray12be);
        (Gray12le, gray12le);
        (Gray10be, gray10be);
        (Gray10le, gray10le);
        (P016le, p016le);
        (P016be, p016be);
        (D3d11, d3d11);
        (Gray9be, gray9be);
        (Gray9le, gray9le);
        (Gbrpf32be, gbrpf32be);
        (Gbrpf32le, gbrpf32le);
        (Gbrapf32be, gbrapf32be);
        (Gbrapf32le, gbrapf32le);
        (Drm_prime, drm_prime);
        (Opencl, opencl);
        (Gray14be, gray14be);
        (Gray14le, gray14le);
        (Grayf32be, grayf32be);
        (Grayf32le, grayf32le);
        (Nb, nb);
      ]

    include MakeEnum (struct
      type nonrec t = t

      let f _ = Nb
    end)

    let t : t typ = enum ~unexpected:(fun _ -> Nb) "AVPixelFormat" m
  end

  module SampleFmt = struct
    let none = constant "AV_SAMPLE_FMT_NONE" int64_t
    and u8 = constant "AV_SAMPLE_FMT_U8" int64_t
    and s16 = constant "AV_SAMPLE_FMT_S16" int64_t
    and s32 = constant "AV_SAMPLE_FMT_S32" int64_t
    and flt = constant "AV_SAMPLE_FMT_FLT" int64_t
    and dbl = constant "AV_SAMPLE_FMT_DBL" int64_t
    and u8p = constant "AV_SAMPLE_FMT_U8P" int64_t
    and s16p = constant "AV_SAMPLE_FMT_S16P" int64_t
    and s32p = constant "AV_SAMPLE_FMT_S32P" int64_t
    and fltp = constant "AV_SAMPLE_FMT_FLTP" int64_t
    and dblp = constant "AV_SAMPLE_FMT_DBLP" int64_t
    and s64 = constant "AV_SAMPLE_FMT_S64" int64_t
    and s64p = constant "AV_SAMPLE_FMT_S64P" int64_t
    and nb = constant "AV_SAMPLE_FMT_NB" int64_t

    type t =
      | None
      | U8
      | S16
      | S32
      | Flt
      | Dbl
      | U8p
      | S16p
      | S32p
      | Fltp
      | Dblp
      | S64
      | S64p
      | Nb
    [@@deriving show { with_path = false }]

    let m =
      [
        (None, none);
        (U8, u8);
        (S16, s16);
        (S32, s32);
        (Flt, flt);
        (Dbl, dbl);
        (U8p, u8p);
        (S16p, s16p);
        (S32p, s32p);
        (Fltp, fltp);
        (Dblp, dblp);
        (S64, s64);
        (S64p, s64p);
        (Nb, nb);
      ]

    include MakeEnum (struct
      type nonrec t = t

      let f _ = Nb
    end)

    let t : t typ = enum ~unexpected:(fun _ -> Nb) "AVSampleFormat" m
  end

  module ChannelLayout = struct
    let front_left = constant "AV_CH_FRONT_LEFT" int64_t
    and front_right = constant "AV_CH_FRONT_RIGHT" int64_t
    and front_center = constant "AV_CH_FRONT_CENTER" int64_t
    and low_frequency = constant "AV_CH_LOW_FREQUENCY" int64_t
    and back_left = constant "AV_CH_BACK_LEFT" int64_t
    and back_right = constant "AV_CH_BACK_RIGHT" int64_t
    and front_left_of_center = constant "AV_CH_FRONT_LEFT_OF_CENTER" int64_t
    and front_right_of_center = constant "AV_CH_FRONT_RIGHT_OF_CENTER" int64_t
    and back_center = constant "AV_CH_BACK_CENTER" int64_t
    and side_left = constant "AV_CH_SIDE_LEFT" int64_t
    and side_right = constant "AV_CH_SIDE_RIGHT" int64_t
    and top_center = constant "AV_CH_TOP_CENTER" int64_t
    and top_front_left = constant "AV_CH_TOP_FRONT_LEFT" int64_t
    and top_front_center = constant "AV_CH_TOP_FRONT_CENTER" int64_t
    and top_front_right = constant "AV_CH_TOP_FRONT_RIGHT" int64_t
    and top_back_left = constant "AV_CH_TOP_BACK_LEFT" int64_t
    and top_back_center = constant "AV_CH_TOP_BACK_CENTER" int64_t
    and top_back_right = constant "AV_CH_TOP_BACK_RIGHT" int64_t
    and stereo_left = constant "AV_CH_STEREO_LEFT" int64_t
    and stereo_right = constant "AV_CH_STEREO_RIGHT" int64_t
    and wide_left = constant "AV_CH_WIDE_LEFT" int64_t
    and wide_right = constant "AV_CH_WIDE_RIGHT" int64_t
    and surround_direct_left = constant "AV_CH_SURROUND_DIRECT_LEFT" int64_t
    and surround_direct_right = constant "AV_CH_SURROUND_DIRECT_RIGHT" int64_t
    and low_frequency_2 = constant "AV_CH_LOW_FREQUENCY_2" int64_t
    and layout_native = constant "AV_CH_LAYOUT_NATIVE" int64_t
    and layout_mono = constant "AV_CH_LAYOUT_MONO" int64_t
    and layout_stereo = constant "AV_CH_LAYOUT_STEREO" int64_t
    and layout_2point1 = constant "AV_CH_LAYOUT_2POINT1" int64_t
    and layout_2_1 = constant "AV_CH_LAYOUT_2_1" int64_t
    and layout_surround = constant "AV_CH_LAYOUT_SURROUND" int64_t
    and layout_3point1 = constant "AV_CH_LAYOUT_3POINT1" int64_t
    and layout_4point0 = constant "AV_CH_LAYOUT_4POINT0" int64_t
    and layout_4point1 = constant "AV_CH_LAYOUT_4POINT1" int64_t
    and layout_2_2 = constant "AV_CH_LAYOUT_2_2" int64_t
    and layout_quad = constant "AV_CH_LAYOUT_QUAD" int64_t
    and layout_5point0 = constant "AV_CH_LAYOUT_5POINT0" int64_t
    and layout_5point1 = constant "AV_CH_LAYOUT_5POINT1" int64_t
    and layout_5point0_back = constant "AV_CH_LAYOUT_5POINT0_BACK" int64_t
    and layout_5point1_back = constant "AV_CH_LAYOUT_5POINT1_BACK" int64_t
    and layout_6point0 = constant "AV_CH_LAYOUT_6POINT0" int64_t
    and layout_6point0_front = constant "AV_CH_LAYOUT_6POINT0_FRONT" int64_t
    and layout_hexagonal = constant "AV_CH_LAYOUT_HEXAGONAL" int64_t
    and layout_6point1 = constant "AV_CH_LAYOUT_6POINT1" int64_t
    and layout_6point1_back = constant "AV_CH_LAYOUT_6POINT1_BACK" int64_t
    and layout_6point1_front = constant "AV_CH_LAYOUT_6POINT1_FRONT" int64_t
    and layout_7point0 = constant "AV_CH_LAYOUT_7POINT0" int64_t
    and layout_7point0_front = constant "AV_CH_LAYOUT_7POINT0_FRONT" int64_t
    and layout_7point1 = constant "AV_CH_LAYOUT_7POINT1" int64_t
    and layout_7point1_wide = constant "AV_CH_LAYOUT_7POINT1_WIDE" int64_t

    and layout_7point1_wide_back =
      constant "AV_CH_LAYOUT_7POINT1_WIDE_BACK" int64_t

    and layout_octagonal = constant "AV_CH_LAYOUT_OCTAGONAL" int64_t
    and layout_hexadecagonal = constant "AV_CH_LAYOUT_HEXADECAGONAL" int64_t
    and layout_stereo_downmix = constant "AV_CH_LAYOUT_STEREO_DOWNMIX" int64_t
  end

  module PictureType = struct
    let none = constant "AV_PICTURE_TYPE_NONE" int64_t
    let i = constant "AV_PICTURE_TYPE_I" int64_t
    let p = constant "AV_PICTURE_TYPE_P" int64_t
    let b = constant "AV_PICTURE_TYPE_B" int64_t
    let s = constant "AV_PICTURE_TYPE_S" int64_t
    let si = constant "AV_PICTURE_TYPE_SI" int64_t
    let sp = constant "AV_PICTURE_TYPE_SP" int64_t
    let bi = constant "AV_PICTURE_TYPE_BI" int64_t

    type t = None | I | P | B | S | SI | SP | BI
    [@@deriving show { with_path = false }]

    let t : t typ =
      enum "AVPictureType"
        ~unexpected:(fun _ -> None)
        [
          (None, none);
          (I, i);
          (P, p);
          (B, b);
          (S, s);
          (SI, si);
          (SP, sp);
          (BI, bi);
        ]
  end

  module Option = struct
    type av_option_union

    let av_option_union : av_option_union union typ = union "av_option_union"
    let i64 = field av_option_union "i64" int64_t
    let dbl = field av_option_union "dbl" double
    let str = field av_option_union "str" string_opt
    let q = field av_option_union "q" Rational.t
    let () = seal av_option_union

    type option
    type t = option structure

    let t : t typ = structure "AVOption"
    let name = field t "name" @@ string
    let help = field t "help" @@ string_opt
    let offset = field t "offset" @@ int
    let type_ = field t "type" Opt_type.t
    let default_val = field t "default_val" av_option_union
    let min = field t "min" double
    let max = field t "max" double
    let flags = field t "flags" int
    let unit = field t "unit" @@ ptr char
    let () = seal t
  end

  (* type av_option_ranges

     let av_option_ranges : av_option_ranges structure typ =
       structure "AVOptionRanges" *)

  module Class_category = struct
    let na = constant "AV_CLASS_CATEGORY_NA" int64_t
    and input = constant "AV_CLASS_CATEGORY_INPUT" int64_t

    type t = AV_CLASS_CATEGORY_NA | AV_CLASS_CATEGORY_INPUT

    let t : t typ =
      enum ~typedef:true "AVClassCategory"
        [ (AV_CLASS_CATEGORY_NA, na); (AV_CLASS_CATEGORY_INPUT, input) ]
  end

  module DicttinanaryConst = struct
    let match_case = constant "AV_DICT_MATCH_CASE" int
    and ignore_suffix = constant "AV_DICT_IGNORE_SUFFIX" int
    and strdup_key = constant "AV_DICT_DONT_STRDUP_KEY" int
    and strdup_val = constant "AV_DICT_DONT_STRDUP_VAL" int
    and dont_overwrite = constant "AV_DICT_DONT_OVERWRITE" int
    and append = constant "AV_DICT_APPEND" int
    and multikey = constant "AV_DICT_MULTIKEY" int
  end

  module Class = struct
    type class_
    type t = class_ structure

    let t : t typ = structure "AVClass"
    let class_name = field t "class_name" @@ ptr char
    (* let item_name = field t "item_name" (  ptr void @-> returning @@ ptr char  ) *)
  end

  module DictionaryEntry = struct
    type dict_entry
    type t = dict_entry structure

    let t : t typ = structure "AVDictionaryEntry"
    let key = field t "key" string
    let value = field t "value" string
    let () = seal t
  end

  module Dictionary = struct
    type dict
    type t = dict structure

    let t : t typ = structure "AVDictionary"
    (* let count = field t "count" int
       let elems = field t "elems" @@ ptr DictionaryEntry.t
       let () = seal t *)
  end

  module BufferRef = struct
    type buffer_ref
    type t = buffer_ref structure

    let t : t typ = structure "AVBufferRef"
  end

  module Buffer = struct
    type buffer
    type t = buffer structure

    let t : t typ = structure "AVBuffer"
  end

  module FrameFlag = struct
    let corrupt = constant "AV_FRAME_FLAG_CORRUPT" int
    let discard = constant "AV_FRAME_FLAG_DISCARD" int
  end

  module ColorRange = struct
    let unspecified = constant "AVCOL_RANGE_UNSPECIFIED" int64_t
    let mpeg = constant "AVCOL_RANGE_MPEG" int64_t
    let jpeg = constant "AVCOL_RANGE_JPEG" int64_t
    let nb = constant "AVCOL_RANGE_NB" int64_t

    type t = UNSPECIFIED | Mpeg | Jpeg | Nb
    [@@deriving show { with_path = false }]

    let t : t typ =
      enum "AVColorRange"
        ~unexpected:(fun _ -> Nb)
        [ (UNSPECIFIED, unspecified); (Mpeg, mpeg); (Jpeg, jpeg); (Nb, nb) ]
  end

  module ColorSpace = struct
    let rgb = constant "AVCOL_SPC_RGB" int64_t
    and bt709 = constant "AVCOL_SPC_BT709" int64_t
    and unspecified = constant "AVCOL_SPC_UNSPECIFIED" int64_t
    and reserved = constant "AVCOL_SPC_RESERVED" int64_t
    and fcc = constant "AVCOL_SPC_FCC" int64_t
    and bt470bg = constant "AVCOL_SPC_BT470BG" int64_t
    and smpte170m = constant "AVCOL_SPC_SMPTE170M" int64_t
    and smpte240m = constant "AVCOL_SPC_SMPTE240M" int64_t
    and ycgco = constant "AVCOL_SPC_YCGCO" int64_t
    and ycocg = constant "AVCOL_SPC_YCOCG" int64_t
    and bt2020_ncl = constant "AVCOL_SPC_BT2020_NCL" int64_t
    and bt2020_cl = constant "AVCOL_SPC_BT2020_CL" int64_t
    and smpte2085 = constant "AVCOL_SPC_SMPTE2085" int64_t
    and chroma_derived_ncl = constant "AVCOL_SPC_CHROMA_DERIVED_NCL" int64_t
    and chroma_derived_cl = constant "AVCOL_SPC_CHROMA_DERIVED_CL" int64_t
    and ictcp = constant "AVCOL_SPC_ICTCP" int64_t
    and nb = constant "AVCOL_SPC_NB" int64_t

    type t =
      | Rgb
      | Bt709
      | Unspecified
      | Reserved
      | Fcc
      | Bt470bg
      | Smpte170m
      | Smpte240m
      | Ycgco
      | Ycocg
      | Bt2020_ncl
      | Bt2020_cl
      | Smpte2085
      | Chroma_derived_ncl
      | Chroma_derived_cl
      | Ictcp
      | Nb
    [@@deriving show { with_path = false }]

    let t : t typ =
      enum " AVColorSpace"
        ~unexpected:(fun _ -> Nb)
        [
          (Rgb, rgb);
          (Bt709, bt709);
          (Unspecified, unspecified);
          (Reserved, reserved);
          (Fcc, fcc);
          (Bt470bg, bt470bg);
          (Smpte170m, smpte170m);
          (Smpte240m, smpte240m);
          (Ycgco, ycgco);
          (Ycocg, ycocg);
          (Bt2020_ncl, bt2020_ncl);
          (Bt2020_cl, bt2020_cl);
          (Smpte2085, smpte2085);
          (Chroma_derived_ncl, chroma_derived_ncl);
          (Chroma_derived_cl, chroma_derived_cl);
          (Ictcp, ictcp);
          (Nb, nb);
        ]
  end

  module Frame = struct
    type frame
    type t = frame structure

    let t : t typ = structure "AVFrame"
    let data = field t "data" @@ array 8 @@ ptr_opt uint8_t
    let linesize = field t "linesize" @@ array 8 int
    let nb_samples = field t "nb_samples" int
    let width = field t "width" int
    let height = field t "height" int
    let format = field t "format" int
    let key_frame = field t "key_frame" int
    let pict_type = field t "pict_type" PictureType.t
    let sample_aspect_ratio = field t "sample_aspect_ratio" Rational.t
    let pts = field t "pts" int64_t
    let pkt_dts = field t "pkt_dts" int64_t
    let interlaced_frame = field t "interlaced_frame" int
    let sample_rate = field t "sample_rate" int
    let channel_layout = field t "channel_layout" uint64_t
    let flags = field t "flags" int
    let color_range = field t "color_range" ColorRange.t
    let colorspace = field t "colorspace" ColorSpace.t
    let pkt_pos = field t "pkt_pos" int64_t
    let pkt_duration = field t "pkt_duration" int64_t
    let metadata = field t "metadata" @@ ptr Dictionary.t
    let channels = field t "channels" int
    let () = seal t
  end
end
