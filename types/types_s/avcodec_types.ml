module type Util = sig
  type 'a typ

  module type Decl = sig
    type t

    val t : t typ
  end

  module Buffer : Decl
  module MediaType : Decl
  module SampleFmt : Decl
  module Rational : Decl
  module PixFmt : Decl
end

module Types (T : Cstubs.Types.TYPE) (U : Util with type 'a typ = 'a T.typ) =
struct
  type 'a structure = 'a Ctypes.structure
  type 'a union = 'a Ctypes.union
  type 'a typ = 'a T.typ

  open T

  module type Decl = sig
    type t

    val t : t typ
  end
  (* module Util = Avutils_types.Types (T) *)

  module CODEC_ID = struct
    let none = constant "AV_CODEC_ID_NONE" int64_t
    and mpeg1video = constant "AV_CODEC_ID_MPEG1VIDEO" int64_t
    and mpeg2video = constant "AV_CODEC_ID_MPEG2VIDEO" int64_t
    and h261 = constant "AV_CODEC_ID_H261" int64_t
    and h263 = constant "AV_CODEC_ID_H263" int64_t
    and rv10 = constant "AV_CODEC_ID_RV10" int64_t
    and rv20 = constant "AV_CODEC_ID_RV20" int64_t
    and mjpeg = constant "AV_CODEC_ID_MJPEG" int64_t
    and mjpegb = constant "AV_CODEC_ID_MJPEGB" int64_t
    and ljpeg = constant "AV_CODEC_ID_LJPEG" int64_t
    and sp5x = constant "AV_CODEC_ID_SP5X" int64_t
    and jpegls = constant "AV_CODEC_ID_JPEGLS" int64_t
    and mpeg4 = constant "AV_CODEC_ID_MPEG4" int64_t
    and rawvideo = constant "AV_CODEC_ID_RAWVIDEO" int64_t
    and msmpeg4v1 = constant "AV_CODEC_ID_MSMPEG4V1" int64_t
    and msmpeg4v2 = constant "AV_CODEC_ID_MSMPEG4V2" int64_t
    and msmpeg4v3 = constant "AV_CODEC_ID_MSMPEG4V3" int64_t
    and wmv1 = constant "AV_CODEC_ID_WMV1" int64_t
    and wmv2 = constant "AV_CODEC_ID_WMV2" int64_t
    and h263p = constant "AV_CODEC_ID_H263P" int64_t
    and h263i = constant "AV_CODEC_ID_H263I" int64_t
    and flv1 = constant "AV_CODEC_ID_FLV1" int64_t
    and svq1 = constant "AV_CODEC_ID_SVQ1" int64_t
    and svq3 = constant "AV_CODEC_ID_SVQ3" int64_t
    and dvvideo = constant "AV_CODEC_ID_DVVIDEO" int64_t
    and huffyuv = constant "AV_CODEC_ID_HUFFYUV" int64_t
    and cyuv = constant "AV_CODEC_ID_CYUV" int64_t
    and h264 = constant "AV_CODEC_ID_H264" int64_t
    and indeo3 = constant "AV_CODEC_ID_INDEO3" int64_t
    and vp3 = constant "AV_CODEC_ID_VP3" int64_t
    and theora = constant "AV_CODEC_ID_THEORA" int64_t
    and asv1 = constant "AV_CODEC_ID_ASV1" int64_t
    and asv2 = constant "AV_CODEC_ID_ASV2" int64_t
    and ffv1 = constant "AV_CODEC_ID_FFV1" int64_t
    and _4xm = constant "AV_CODEC_ID_4XM" int64_t
    and vcr1 = constant "AV_CODEC_ID_VCR1" int64_t
    and cljr = constant "AV_CODEC_ID_CLJR" int64_t
    and mdec = constant "AV_CODEC_ID_MDEC" int64_t
    and roq = constant "AV_CODEC_ID_ROQ" int64_t
    and interplay_video = constant "AV_CODEC_ID_INTERPLAY_VIDEO" int64_t
    and xan_wc3 = constant "AV_CODEC_ID_XAN_WC3" int64_t
    and xan_wc4 = constant "AV_CODEC_ID_XAN_WC4" int64_t
    and rpza = constant "AV_CODEC_ID_RPZA" int64_t
    and cinepak = constant "AV_CODEC_ID_CINEPAK" int64_t
    and ws_vqa = constant "AV_CODEC_ID_WS_VQA" int64_t
    and msrle = constant "AV_CODEC_ID_MSRLE" int64_t
    and msvideo1 = constant "AV_CODEC_ID_MSVIDEO1" int64_t
    and idcin = constant "AV_CODEC_ID_IDCIN" int64_t
    and _8bps = constant "AV_CODEC_ID_8BPS" int64_t
    and smc = constant "AV_CODEC_ID_SMC" int64_t
    and flic = constant "AV_CODEC_ID_FLIC" int64_t
    and truemotion1 = constant "AV_CODEC_ID_TRUEMOTION1" int64_t
    and vmdvideo = constant "AV_CODEC_ID_VMDVIDEO" int64_t
    and mszh = constant "AV_CODEC_ID_MSZH" int64_t
    and zlib = constant "AV_CODEC_ID_ZLIB" int64_t
    and qtrle = constant "AV_CODEC_ID_QTRLE" int64_t
    and tscc = constant "AV_CODEC_ID_TSCC" int64_t
    and ulti = constant "AV_CODEC_ID_ULTI" int64_t
    and qdraw = constant "AV_CODEC_ID_QDRAW" int64_t
    and vixl = constant "AV_CODEC_ID_VIXL" int64_t
    and qpeg = constant "AV_CODEC_ID_QPEG" int64_t
    and png = constant "AV_CODEC_ID_PNG" int64_t
    and ppm = constant "AV_CODEC_ID_PPM" int64_t
    and pbm = constant "AV_CODEC_ID_PBM" int64_t
    and pgm = constant "AV_CODEC_ID_PGM" int64_t
    and pgmyuv = constant "AV_CODEC_ID_PGMYUV" int64_t
    and pam = constant "AV_CODEC_ID_PAM" int64_t
    and ffvhuff = constant "AV_CODEC_ID_FFVHUFF" int64_t
    and rv30 = constant "AV_CODEC_ID_RV30" int64_t
    and rv40 = constant "AV_CODEC_ID_RV40" int64_t
    and vc1 = constant "AV_CODEC_ID_VC1" int64_t
    and wmv3 = constant "AV_CODEC_ID_WMV3" int64_t
    and loco = constant "AV_CODEC_ID_LOCO" int64_t
    and wnv1 = constant "AV_CODEC_ID_WNV1" int64_t
    and aasc = constant "AV_CODEC_ID_AASC" int64_t
    and indeo2 = constant "AV_CODEC_ID_INDEO2" int64_t
    and fraps = constant "AV_CODEC_ID_FRAPS" int64_t
    and truemotion2 = constant "AV_CODEC_ID_TRUEMOTION2" int64_t
    and bmp = constant "AV_CODEC_ID_BMP" int64_t
    and cscd = constant "AV_CODEC_ID_CSCD" int64_t
    and mmvideo = constant "AV_CODEC_ID_MMVIDEO" int64_t
    and zmbv = constant "AV_CODEC_ID_ZMBV" int64_t
    and avs = constant "AV_CODEC_ID_AVS" int64_t
    and smackvideo = constant "AV_CODEC_ID_SMACKVIDEO" int64_t
    and nuv = constant "AV_CODEC_ID_NUV" int64_t
    and kmvc = constant "AV_CODEC_ID_KMVC" int64_t
    and flashsv = constant "AV_CODEC_ID_FLASHSV" int64_t
    and cavs = constant "AV_CODEC_ID_CAVS" int64_t
    and jpeg2000 = constant "AV_CODEC_ID_JPEG2000" int64_t
    and vmnc = constant "AV_CODEC_ID_VMNC" int64_t
    and vp5 = constant "AV_CODEC_ID_VP5" int64_t
    and vp6 = constant "AV_CODEC_ID_VP6" int64_t
    and vp6f = constant "AV_CODEC_ID_VP6F" int64_t
    and targa = constant "AV_CODEC_ID_TARGA" int64_t
    and dsicinvideo = constant "AV_CODEC_ID_DSICINVIDEO" int64_t
    and tiertexseqvideo = constant "AV_CODEC_ID_TIERTEXSEQVIDEO" int64_t
    and tiff = constant "AV_CODEC_ID_TIFF" int64_t
    and gif = constant "AV_CODEC_ID_GIF" int64_t
    and dxa = constant "AV_CODEC_ID_DXA" int64_t
    and dnxhd = constant "AV_CODEC_ID_DNXHD" int64_t
    and thp = constant "AV_CODEC_ID_THP" int64_t
    and sgi = constant "AV_CODEC_ID_SGI" int64_t
    and c93 = constant "AV_CODEC_ID_C93" int64_t
    and bethsoftvid = constant "AV_CODEC_ID_BETHSOFTVID" int64_t
    and ptx = constant "AV_CODEC_ID_PTX" int64_t
    and txd = constant "AV_CODEC_ID_TXD" int64_t
    and vp6a = constant "AV_CODEC_ID_VP6A" int64_t
    and amv = constant "AV_CODEC_ID_AMV" int64_t
    and vb = constant "AV_CODEC_ID_VB" int64_t
    and pcx = constant "AV_CODEC_ID_PCX" int64_t
    and sunrast = constant "AV_CODEC_ID_SUNRAST" int64_t
    and indeo4 = constant "AV_CODEC_ID_INDEO4" int64_t
    and indeo5 = constant "AV_CODEC_ID_INDEO5" int64_t
    and mimic = constant "AV_CODEC_ID_MIMIC" int64_t
    and rl2 = constant "AV_CODEC_ID_RL2" int64_t
    and escape124 = constant "AV_CODEC_ID_ESCAPE124" int64_t
    and dirac = constant "AV_CODEC_ID_DIRAC" int64_t
    and bfi = constant "AV_CODEC_ID_BFI" int64_t
    and cmv = constant "AV_CODEC_ID_CMV" int64_t
    and motionpixels = constant "AV_CODEC_ID_MOTIONPIXELS" int64_t
    and tgv = constant "AV_CODEC_ID_TGV" int64_t
    and tgq = constant "AV_CODEC_ID_TGQ" int64_t
    and tqi = constant "AV_CODEC_ID_TQI" int64_t
    and aura = constant "AV_CODEC_ID_AURA" int64_t
    and aura2 = constant "AV_CODEC_ID_AURA2" int64_t
    and v210x = constant "AV_CODEC_ID_V210X" int64_t
    and tmv = constant "AV_CODEC_ID_TMV" int64_t
    and v210 = constant "AV_CODEC_ID_V210" int64_t
    and dpx = constant "AV_CODEC_ID_DPX" int64_t
    and mad = constant "AV_CODEC_ID_MAD" int64_t
    and frwu = constant "AV_CODEC_ID_FRWU" int64_t
    and flashsv2 = constant "AV_CODEC_ID_FLASHSV2" int64_t
    and cdgraphics = constant "AV_CODEC_ID_CDGRAPHICS" int64_t
    and r210 = constant "AV_CODEC_ID_R210" int64_t
    and anm = constant "AV_CODEC_ID_ANM" int64_t
    and binkvideo = constant "AV_CODEC_ID_BINKVIDEO" int64_t
    and iff_ilbm = constant "AV_CODEC_ID_IFF_ILBM" int64_t
    and kgv1 = constant "AV_CODEC_ID_KGV1" int64_t
    and yop = constant "AV_CODEC_ID_YOP" int64_t
    and vp8 = constant "AV_CODEC_ID_VP8" int64_t
    and pictor = constant "AV_CODEC_ID_PICTOR" int64_t
    and ansi = constant "AV_CODEC_ID_ANSI" int64_t
    and a64_multi = constant "AV_CODEC_ID_A64_MULTI" int64_t
    and a64_multi5 = constant "AV_CODEC_ID_A64_MULTI5" int64_t
    and r10k = constant "AV_CODEC_ID_R10K" int64_t
    and mxpeg = constant "AV_CODEC_ID_MXPEG" int64_t
    and lagarith = constant "AV_CODEC_ID_LAGARITH" int64_t
    and prores = constant "AV_CODEC_ID_PRORES" int64_t
    and jv = constant "AV_CODEC_ID_JV" int64_t
    and dfa = constant "AV_CODEC_ID_DFA" int64_t
    and wmv3image = constant "AV_CODEC_ID_WMV3IMAGE" int64_t
    and vc1image = constant "AV_CODEC_ID_VC1IMAGE" int64_t
    and utvideo = constant "AV_CODEC_ID_UTVIDEO" int64_t
    and bmv_video = constant "AV_CODEC_ID_BMV_VIDEO" int64_t
    and vble = constant "AV_CODEC_ID_VBLE" int64_t
    and dxtory = constant "AV_CODEC_ID_DXTORY" int64_t
    and v410 = constant "AV_CODEC_ID_V410" int64_t
    and xwd = constant "AV_CODEC_ID_XWD" int64_t
    and cdxl = constant "AV_CODEC_ID_CDXL" int64_t
    and xbm = constant "AV_CODEC_ID_XBM" int64_t
    and zerocodec = constant "AV_CODEC_ID_ZEROCODEC" int64_t
    and mss1 = constant "AV_CODEC_ID_MSS1" int64_t
    and msa1 = constant "AV_CODEC_ID_MSA1" int64_t
    and tscc2 = constant "AV_CODEC_ID_TSCC2" int64_t
    and mts2 = constant "AV_CODEC_ID_MTS2" int64_t
    and cllc = constant "AV_CODEC_ID_CLLC" int64_t
    and mss2 = constant "AV_CODEC_ID_MSS2" int64_t
    and vp9 = constant "AV_CODEC_ID_VP9" int64_t
    and aic = constant "AV_CODEC_ID_AIC" int64_t
    and escape130 = constant "AV_CODEC_ID_ESCAPE130" int64_t
    and g2m = constant "AV_CODEC_ID_G2M" int64_t
    and webp = constant "AV_CODEC_ID_WEBP" int64_t
    and hnm4_video = constant "AV_CODEC_ID_HNM4_VIDEO" int64_t
    and hevc = constant "AV_CODEC_ID_HEVC" int64_t
    and fic = constant "AV_CODEC_ID_FIC" int64_t
    and alias_pix = constant "AV_CODEC_ID_ALIAS_PIX" int64_t
    and brender_pix = constant "AV_CODEC_ID_BRENDER_PIX" int64_t
    and paf_video = constant "AV_CODEC_ID_PAF_VIDEO" int64_t
    and exr = constant "AV_CODEC_ID_EXR" int64_t
    and vp7 = constant "AV_CODEC_ID_VP7" int64_t
    and sanm = constant "AV_CODEC_ID_SANM" int64_t
    and sgirle = constant "AV_CODEC_ID_SGIRLE" int64_t
    and mvc1 = constant "AV_CODEC_ID_MVC1" int64_t
    and mvc2 = constant "AV_CODEC_ID_MVC2" int64_t
    and hqx = constant "AV_CODEC_ID_HQX" int64_t
    and tdsc = constant "AV_CODEC_ID_TDSC" int64_t
    and hq_hqa = constant "AV_CODEC_ID_HQ_HQA" int64_t
    and hap = constant "AV_CODEC_ID_HAP" int64_t
    and dds = constant "AV_CODEC_ID_DDS" int64_t
    and dxv = constant "AV_CODEC_ID_DXV" int64_t
    and screenpresso = constant "AV_CODEC_ID_SCREENPRESSO" int64_t
    and rscc = constant "AV_CODEC_ID_RSCC" int64_t
    and avs2 = constant "AV_CODEC_ID_AVS2" int64_t
    and y41p = constant "AV_CODEC_ID_Y41P" int64_t
    and avrp = constant "AV_CODEC_ID_AVRP" int64_t
    and _012v = constant "AV_CODEC_ID_012V" int64_t
    and avui = constant "AV_CODEC_ID_AVUI" int64_t
    and ayuv = constant "AV_CODEC_ID_AYUV" int64_t
    and targa_y216 = constant "AV_CODEC_ID_TARGA_Y216" int64_t
    and v308 = constant "AV_CODEC_ID_V308" int64_t
    and v408 = constant "AV_CODEC_ID_V408" int64_t
    and yuv4 = constant "AV_CODEC_ID_YUV4" int64_t
    and avrn = constant "AV_CODEC_ID_AVRN" int64_t
    and cpia = constant "AV_CODEC_ID_CPIA" int64_t
    and xface = constant "AV_CODEC_ID_XFACE" int64_t
    and snow = constant "AV_CODEC_ID_SNOW" int64_t
    and smvjpeg = constant "AV_CODEC_ID_SMVJPEG" int64_t
    and apng = constant "AV_CODEC_ID_APNG" int64_t
    and daala = constant "AV_CODEC_ID_DAALA" int64_t
    and cfhd = constant "AV_CODEC_ID_CFHD" int64_t
    and truemotion2rt = constant "AV_CODEC_ID_TRUEMOTION2RT" int64_t
    and m101 = constant "AV_CODEC_ID_M101" int64_t
    and magicyuv = constant "AV_CODEC_ID_MAGICYUV" int64_t
    and sheervideo = constant "AV_CODEC_ID_SHEERVIDEO" int64_t
    and ylc = constant "AV_CODEC_ID_YLC" int64_t
    and psd = constant "AV_CODEC_ID_PSD" int64_t
    and pixlet = constant "AV_CODEC_ID_PIXLET" int64_t
    and speedhq = constant "AV_CODEC_ID_SPEEDHQ" int64_t
    and fmvc = constant "AV_CODEC_ID_FMVC" int64_t
    and scpr = constant "AV_CODEC_ID_SCPR" int64_t
    and clearvideo = constant "AV_CODEC_ID_CLEARVIDEO" int64_t
    and xpm = constant "AV_CODEC_ID_XPM" int64_t
    and av1 = constant "AV_CODEC_ID_AV1" int64_t
    and bitpacked = constant "AV_CODEC_ID_BITPACKED" int64_t
    and mscc = constant "AV_CODEC_ID_MSCC" int64_t
    and srgc = constant "AV_CODEC_ID_SRGC" int64_t
    and svg = constant "AV_CODEC_ID_SVG" int64_t
    and gdv = constant "AV_CODEC_ID_GDV" int64_t
    and fits = constant "AV_CODEC_ID_FITS" int64_t
    and imm4 = constant "AV_CODEC_ID_IMM4" int64_t
    and prosumer = constant "AV_CODEC_ID_PROSUMER" int64_t
    and mwsc = constant "AV_CODEC_ID_MWSC" int64_t
    and wcmv = constant "AV_CODEC_ID_WCMV" int64_t
    and rasc = constant "AV_CODEC_ID_RASC" int64_t
    and first_audio = constant "AV_CODEC_ID_FIRST_AUDIO" int64_t
    and pcm_s16le = constant "AV_CODEC_ID_PCM_S16LE" int64_t
    and pcm_s16be = constant "AV_CODEC_ID_PCM_S16BE" int64_t
    and pcm_u16le = constant "AV_CODEC_ID_PCM_U16LE" int64_t
    and pcm_u16be = constant "AV_CODEC_ID_PCM_U16BE" int64_t
    and pcm_s8 = constant "AV_CODEC_ID_PCM_S8" int64_t
    and pcm_u8 = constant "AV_CODEC_ID_PCM_U8" int64_t
    and pcm_mulaw = constant "AV_CODEC_ID_PCM_MULAW" int64_t
    and pcm_alaw = constant "AV_CODEC_ID_PCM_ALAW" int64_t
    and pcm_s32le = constant "AV_CODEC_ID_PCM_S32LE" int64_t
    and pcm_s32be = constant "AV_CODEC_ID_PCM_S32BE" int64_t
    and pcm_u32le = constant "AV_CODEC_ID_PCM_U32LE" int64_t
    and pcm_u32be = constant "AV_CODEC_ID_PCM_U32BE" int64_t
    and pcm_s24le = constant "AV_CODEC_ID_PCM_S24LE" int64_t
    and pcm_s24be = constant "AV_CODEC_ID_PCM_S24BE" int64_t
    and pcm_u24le = constant "AV_CODEC_ID_PCM_U24LE" int64_t
    and pcm_u24be = constant "AV_CODEC_ID_PCM_U24BE" int64_t
    and pcm_s24daud = constant "AV_CODEC_ID_PCM_S24DAUD" int64_t
    and pcm_zork = constant "AV_CODEC_ID_PCM_ZORK" int64_t
    and pcm_s16le_planar = constant "AV_CODEC_ID_PCM_S16LE_PLANAR" int64_t
    and pcm_dvd = constant "AV_CODEC_ID_PCM_DVD" int64_t
    and pcm_f32be = constant "AV_CODEC_ID_PCM_F32BE" int64_t
    and pcm_f32le = constant "AV_CODEC_ID_PCM_F32LE" int64_t
    and pcm_f64be = constant "AV_CODEC_ID_PCM_F64BE" int64_t
    and pcm_f64le = constant "AV_CODEC_ID_PCM_F64LE" int64_t
    and pcm_bluray = constant "AV_CODEC_ID_PCM_BLURAY" int64_t
    and pcm_lxf = constant "AV_CODEC_ID_PCM_LXF" int64_t
    and s302m = constant "AV_CODEC_ID_S302M" int64_t
    and pcm_s8_planar = constant "AV_CODEC_ID_PCM_S8_PLANAR" int64_t
    and pcm_s24le_planar = constant "AV_CODEC_ID_PCM_S24LE_PLANAR" int64_t
    and pcm_s32le_planar = constant "AV_CODEC_ID_PCM_S32LE_PLANAR" int64_t
    and pcm_s16be_planar = constant "AV_CODEC_ID_PCM_S16BE_PLANAR" int64_t
    and pcm_s64le = constant "AV_CODEC_ID_PCM_S64LE" int64_t
    and pcm_s64be = constant "AV_CODEC_ID_PCM_S64BE" int64_t
    and pcm_f16le = constant "AV_CODEC_ID_PCM_F16LE" int64_t
    and pcm_f24le = constant "AV_CODEC_ID_PCM_F24LE" int64_t
    and pcm_vidc = constant "AV_CODEC_ID_PCM_VIDC" int64_t
    and adpcm_ima_qt = constant "AV_CODEC_ID_ADPCM_IMA_QT" int64_t
    and adpcm_ima_wav = constant "AV_CODEC_ID_ADPCM_IMA_WAV" int64_t
    and adpcm_ima_dk3 = constant "AV_CODEC_ID_ADPCM_IMA_DK3" int64_t
    and adpcm_ima_dk4 = constant "AV_CODEC_ID_ADPCM_IMA_DK4" int64_t
    and adpcm_ima_ws = constant "AV_CODEC_ID_ADPCM_IMA_WS" int64_t
    and adpcm_ima_smjpeg = constant "AV_CODEC_ID_ADPCM_IMA_SMJPEG" int64_t
    and adpcm_ms = constant "AV_CODEC_ID_ADPCM_MS" int64_t
    and adpcm_4xm = constant "AV_CODEC_ID_ADPCM_4XM" int64_t
    and adpcm_xa = constant "AV_CODEC_ID_ADPCM_XA" int64_t
    and adpcm_adx = constant "AV_CODEC_ID_ADPCM_ADX" int64_t
    and adpcm_ea = constant "AV_CODEC_ID_ADPCM_EA" int64_t
    and adpcm_g726 = constant "AV_CODEC_ID_ADPCM_G726" int64_t
    and adpcm_ct = constant "AV_CODEC_ID_ADPCM_CT" int64_t
    and adpcm_swf = constant "AV_CODEC_ID_ADPCM_SWF" int64_t
    and adpcm_yamaha = constant "AV_CODEC_ID_ADPCM_YAMAHA" int64_t
    and adpcm_sbpro_4 = constant "AV_CODEC_ID_ADPCM_SBPRO_4" int64_t
    and adpcm_sbpro_3 = constant "AV_CODEC_ID_ADPCM_SBPRO_3" int64_t
    and adpcm_sbpro_2 = constant "AV_CODEC_ID_ADPCM_SBPRO_2" int64_t
    and adpcm_thp = constant "AV_CODEC_ID_ADPCM_THP" int64_t
    and adpcm_ima_amv = constant "AV_CODEC_ID_ADPCM_IMA_AMV" int64_t
    and adpcm_ea_r1 = constant "AV_CODEC_ID_ADPCM_EA_R1" int64_t
    and adpcm_ea_r3 = constant "AV_CODEC_ID_ADPCM_EA_R3" int64_t
    and adpcm_ea_r2 = constant "AV_CODEC_ID_ADPCM_EA_R2" int64_t
    and adpcm_ima_ea_sead = constant "AV_CODEC_ID_ADPCM_IMA_EA_SEAD" int64_t
    and adpcm_ima_ea_eacs = constant "AV_CODEC_ID_ADPCM_IMA_EA_EACS" int64_t
    and adpcm_ea_xas = constant "AV_CODEC_ID_ADPCM_EA_XAS" int64_t
    and adpcm_ea_maxis_xa = constant "AV_CODEC_ID_ADPCM_EA_MAXIS_XA" int64_t
    and adpcm_ima_iss = constant "AV_CODEC_ID_ADPCM_IMA_ISS" int64_t
    and adpcm_g722 = constant "AV_CODEC_ID_ADPCM_G722" int64_t
    and adpcm_ima_apc = constant "AV_CODEC_ID_ADPCM_IMA_APC" int64_t
    and adpcm_vima = constant "AV_CODEC_ID_ADPCM_VIMA" int64_t
    and adpcm_afc = constant "AV_CODEC_ID_ADPCM_AFC" int64_t
    and adpcm_ima_oki = constant "AV_CODEC_ID_ADPCM_IMA_OKI" int64_t
    and adpcm_dtk = constant "AV_CODEC_ID_ADPCM_DTK" int64_t
    and adpcm_ima_rad = constant "AV_CODEC_ID_ADPCM_IMA_RAD" int64_t
    and adpcm_g726le = constant "AV_CODEC_ID_ADPCM_G726LE" int64_t
    and adpcm_thp_le = constant "AV_CODEC_ID_ADPCM_THP_LE" int64_t
    and adpcm_psx = constant "AV_CODEC_ID_ADPCM_PSX" int64_t
    and adpcm_aica = constant "AV_CODEC_ID_ADPCM_AICA" int64_t
    and adpcm_ima_dat4 = constant "AV_CODEC_ID_ADPCM_IMA_DAT4" int64_t
    and adpcm_mtaf = constant "AV_CODEC_ID_ADPCM_MTAF" int64_t
    and amr_nb = constant "AV_CODEC_ID_AMR_NB" int64_t
    and amr_wb = constant "AV_CODEC_ID_AMR_WB" int64_t
    and ra_144 = constant "AV_CODEC_ID_RA_144" int64_t
    and ra_288 = constant "AV_CODEC_ID_RA_288" int64_t
    and roq_dpcm = constant "AV_CODEC_ID_ROQ_DPCM" int64_t
    and interplay_dpcm = constant "AV_CODEC_ID_INTERPLAY_DPCM" int64_t
    and xan_dpcm = constant "AV_CODEC_ID_XAN_DPCM" int64_t
    and sol_dpcm = constant "AV_CODEC_ID_SOL_DPCM" int64_t
    and sdx2_dpcm = constant "AV_CODEC_ID_SDX2_DPCM" int64_t
    and gremlin_dpcm = constant "AV_CODEC_ID_GREMLIN_DPCM" int64_t
    and mp2 = constant "AV_CODEC_ID_MP2" int64_t
    and mp3 = constant "AV_CODEC_ID_MP3" int64_t
    and aac = constant "AV_CODEC_ID_AAC" int64_t
    and ac3 = constant "AV_CODEC_ID_AC3" int64_t
    and dts = constant "AV_CODEC_ID_DTS" int64_t
    and vorbis = constant "AV_CODEC_ID_VORBIS" int64_t
    and dvaudio = constant "AV_CODEC_ID_DVAUDIO" int64_t
    and wmav1 = constant "AV_CODEC_ID_WMAV1" int64_t
    and wmav2 = constant "AV_CODEC_ID_WMAV2" int64_t
    and mace3 = constant "AV_CODEC_ID_MACE3" int64_t
    and mace6 = constant "AV_CODEC_ID_MACE6" int64_t
    and vmdaudio = constant "AV_CODEC_ID_VMDAUDIO" int64_t
    and flac = constant "AV_CODEC_ID_FLAC" int64_t
    and mp3adu = constant "AV_CODEC_ID_MP3ADU" int64_t
    and mp3on4 = constant "AV_CODEC_ID_MP3ON4" int64_t
    and shorten = constant "AV_CODEC_ID_SHORTEN" int64_t
    and alac = constant "AV_CODEC_ID_ALAC" int64_t
    and westwood_snd1 = constant "AV_CODEC_ID_WESTWOOD_SND1" int64_t
    and gsm = constant "AV_CODEC_ID_GSM" int64_t
    and qdm2 = constant "AV_CODEC_ID_QDM2" int64_t
    and cook = constant "AV_CODEC_ID_COOK" int64_t
    and truespeech = constant "AV_CODEC_ID_TRUESPEECH" int64_t
    and tta = constant "AV_CODEC_ID_TTA" int64_t
    and smackaudio = constant "AV_CODEC_ID_SMACKAUDIO" int64_t
    and qcelp = constant "AV_CODEC_ID_QCELP" int64_t
    and wavpack = constant "AV_CODEC_ID_WAVPACK" int64_t
    and dsicinaudio = constant "AV_CODEC_ID_DSICINAUDIO" int64_t
    and imc = constant "AV_CODEC_ID_IMC" int64_t
    and musepack7 = constant "AV_CODEC_ID_MUSEPACK7" int64_t
    and mlp = constant "AV_CODEC_ID_MLP" int64_t
    and gsm_ms = constant "AV_CODEC_ID_GSM_MS" int64_t
    and atrac3 = constant "AV_CODEC_ID_ATRAC3" int64_t
    and ape = constant "AV_CODEC_ID_APE" int64_t
    and nellymoser = constant "AV_CODEC_ID_NELLYMOSER" int64_t
    and musepack8 = constant "AV_CODEC_ID_MUSEPACK8" int64_t
    and speex = constant "AV_CODEC_ID_SPEEX" int64_t
    and wmavoice = constant "AV_CODEC_ID_WMAVOICE" int64_t
    and wmapro = constant "AV_CODEC_ID_WMAPRO" int64_t
    and wmalossless = constant "AV_CODEC_ID_WMALOSSLESS" int64_t
    and atrac3p = constant "AV_CODEC_ID_ATRAC3P" int64_t
    and eac3 = constant "AV_CODEC_ID_EAC3" int64_t
    and sipr = constant "AV_CODEC_ID_SIPR" int64_t
    and mp1 = constant "AV_CODEC_ID_MP1" int64_t
    and twinvq = constant "AV_CODEC_ID_TWINVQ" int64_t
    and truehd = constant "AV_CODEC_ID_TRUEHD" int64_t
    and mp4als = constant "AV_CODEC_ID_MP4ALS" int64_t
    and atrac1 = constant "AV_CODEC_ID_ATRAC1" int64_t
    and binkaudio_rdft = constant "AV_CODEC_ID_BINKAUDIO_RDFT" int64_t
    and binkaudio_dct = constant "AV_CODEC_ID_BINKAUDIO_DCT" int64_t
    and aac_latm = constant "AV_CODEC_ID_AAC_LATM" int64_t
    and qdmc = constant "AV_CODEC_ID_QDMC" int64_t
    and celt = constant "AV_CODEC_ID_CELT" int64_t
    and g723_1 = constant "AV_CODEC_ID_G723_1" int64_t
    and g729 = constant "AV_CODEC_ID_G729" int64_t
    and _8svx_exp = constant "AV_CODEC_ID_8SVX_EXP" int64_t
    and _8svx_fib = constant "AV_CODEC_ID_8SVX_FIB" int64_t
    and bmv_audio = constant "AV_CODEC_ID_BMV_AUDIO" int64_t
    and ralf = constant "AV_CODEC_ID_RALF" int64_t
    and iac = constant "AV_CODEC_ID_IAC" int64_t
    and ilbc = constant "AV_CODEC_ID_ILBC" int64_t
    and opus = constant "AV_CODEC_ID_OPUS" int64_t
    and comfort_noise = constant "AV_CODEC_ID_COMFORT_NOISE" int64_t
    and tak = constant "AV_CODEC_ID_TAK" int64_t
    and metasound = constant "AV_CODEC_ID_METASOUND" int64_t
    and paf_audio = constant "AV_CODEC_ID_PAF_AUDIO" int64_t
    and on2avc = constant "AV_CODEC_ID_ON2AVC" int64_t
    and dss_sp = constant "AV_CODEC_ID_DSS_SP" int64_t
    and codec2 = constant "AV_CODEC_ID_CODEC2" int64_t
    and ffwavesynth = constant "AV_CODEC_ID_FFWAVESYNTH" int64_t
    and sonic = constant "AV_CODEC_ID_SONIC" int64_t
    and sonic_ls = constant "AV_CODEC_ID_SONIC_LS" int64_t
    and evrc = constant "AV_CODEC_ID_EVRC" int64_t
    and smv = constant "AV_CODEC_ID_SMV" int64_t
    and dsd_lsbf = constant "AV_CODEC_ID_DSD_LSBF" int64_t
    and dsd_msbf = constant "AV_CODEC_ID_DSD_MSBF" int64_t
    and dsd_lsbf_planar = constant "AV_CODEC_ID_DSD_LSBF_PLANAR" int64_t
    and dsd_msbf_planar = constant "AV_CODEC_ID_DSD_MSBF_PLANAR" int64_t
    and _4gv = constant "AV_CODEC_ID_4GV" int64_t
    and interplay_acm = constant "AV_CODEC_ID_INTERPLAY_ACM" int64_t
    and xma1 = constant "AV_CODEC_ID_XMA1" int64_t
    and xma2 = constant "AV_CODEC_ID_XMA2" int64_t
    and dst = constant "AV_CODEC_ID_DST" int64_t
    and atrac3al = constant "AV_CODEC_ID_ATRAC3AL" int64_t
    and atrac3pal = constant "AV_CODEC_ID_ATRAC3PAL" int64_t
    and dolby_e = constant "AV_CODEC_ID_DOLBY_E" int64_t
    and aptx = constant "AV_CODEC_ID_APTX" int64_t
    and aptx_hd = constant "AV_CODEC_ID_APTX_HD" int64_t
    and sbc = constant "AV_CODEC_ID_SBC" int64_t
    and atrac9 = constant "AV_CODEC_ID_ATRAC9" int64_t
    and first_subtitle = constant "AV_CODEC_ID_FIRST_SUBTITLE" int64_t
    and dvd_subtitle = constant "AV_CODEC_ID_DVD_SUBTITLE" int64_t
    and dvb_subtitle = constant "AV_CODEC_ID_DVB_SUBTITLE" int64_t
    and text = constant "AV_CODEC_ID_TEXT" int64_t
    and xsub = constant "AV_CODEC_ID_XSUB" int64_t
    and ssa = constant "AV_CODEC_ID_SSA" int64_t
    and mov_text = constant "AV_CODEC_ID_MOV_TEXT" int64_t
    and hdmv_pgs_subtitle = constant "AV_CODEC_ID_HDMV_PGS_SUBTITLE" int64_t
    and dvb_teletext = constant "AV_CODEC_ID_DVB_TELETEXT" int64_t
    and srt = constant "AV_CODEC_ID_SRT" int64_t
    and microdvd = constant "AV_CODEC_ID_MICRODVD" int64_t
    and eia_608 = constant "AV_CODEC_ID_EIA_608" int64_t
    and jacosub = constant "AV_CODEC_ID_JACOSUB" int64_t
    and sami = constant "AV_CODEC_ID_SAMI" int64_t
    and realtext = constant "AV_CODEC_ID_REALTEXT" int64_t
    and stl = constant "AV_CODEC_ID_STL" int64_t
    and subviewer1 = constant "AV_CODEC_ID_SUBVIEWER1" int64_t
    and subviewer = constant "AV_CODEC_ID_SUBVIEWER" int64_t
    and subrip = constant "AV_CODEC_ID_SUBRIP" int64_t
    and webvtt = constant "AV_CODEC_ID_WEBVTT" int64_t
    and mpl2 = constant "AV_CODEC_ID_MPL2" int64_t
    and vplayer = constant "AV_CODEC_ID_VPLAYER" int64_t
    and pjs = constant "AV_CODEC_ID_PJS" int64_t
    and ass = constant "AV_CODEC_ID_ASS" int64_t
    and hdmv_text_subtitle = constant "AV_CODEC_ID_HDMV_TEXT_SUBTITLE" int64_t
    and ttml = constant "AV_CODEC_ID_TTML" int64_t
    and first_unknown = constant "AV_CODEC_ID_FIRST_UNKNOWN" int64_t
    and ttf = constant "AV_CODEC_ID_TTF" int64_t
    and scte_35 = constant "AV_CODEC_ID_SCTE_35" int64_t
    and bintext = constant "AV_CODEC_ID_BINTEXT" int64_t
    and xbin = constant "AV_CODEC_ID_XBIN" int64_t
    and idf = constant "AV_CODEC_ID_IDF" int64_t
    and otf = constant "AV_CODEC_ID_OTF" int64_t
    and smpte_klv = constant "AV_CODEC_ID_SMPTE_KLV" int64_t
    and dvd_nav = constant "AV_CODEC_ID_DVD_NAV" int64_t
    and timed_id3 = constant "AV_CODEC_ID_TIMED_ID3" int64_t
    and bin_data = constant "AV_CODEC_ID_BIN_DATA" int64_t
    and probe = constant "AV_CODEC_ID_PROBE" int64_t
    and mpeg2ts = constant "AV_CODEC_ID_MPEG2TS" int64_t
    and mpeg4systems = constant "AV_CODEC_ID_MPEG4SYSTEMS" int64_t
    and ffmetadata = constant "AV_CODEC_ID_FFMETADATA" int64_t
    and wrapped_avframe = constant "AV_CODEC_ID_WRAPPED_AVFRAME" int64_t

    type t =
      | NONE
      | MPEG1VIDEO
      | MPEG2VIDEO
      | H261
      | H263
      | Rv10
      | Rv20
      | Mjpeg
      | Mjpegb
      | Ljpeg
      | Sp5x
      | Jpegls
      | Mpeg4
      | Rawvideo
      | Msmpeg4v1
      | Msmpeg4v2
      | Msmpeg4v3
      | Wmv1
      | Wmv2
      | H263p
      | H263i
      | Flv1
      | Svq1
      | Svq3
      | Dvvideo
      | Huffyuv
      | Cyuv
      | H264
      | Indeo3
      | Vp3
      | Theora
      | Asv1
      | Asv2
      | Ffv1
      | A_4xm
      | Vcr1
      | Cljr
      | Mdec
      | Roq
      | Interplay_video
      | Xan_wc3
      | Xan_wc4
      | Rpza
      | Cinepak
      | Ws_vqa
      | Msrle
      | Msvideo1
      | Idcin
      | A_8bps
      | Smc
      | Flic
      | Truemotion1
      | Vmdvideo
      | Mszh
      | Zlib
      | Qtrle
      | Tscc
      | Ulti
      | Qdraw
      | Vixl
      | Qpeg
      | Png
      | Ppm
      | Pbm
      | Pgm
      | Pgmyuv
      | Pam
      | Ffvhuff
      | Rv30
      | Rv40
      | Vc1
      | Wmv3
      | Loco
      | Wnv1
      | Aasc
      | Indeo2
      | Fraps
      | Truemotion2
      | Bmp
      | Cscd
      | Mmvideo
      | Zmbv
      | Avs
      | Smackvideo
      | Nuv
      | Kmvc
      | Flashsv
      | Cavs
      | Jpeg2000
      | Vmnc
      | Vp5
      | Vp6
      | Vp6f
      | Targa
      | Dsicinvideo
      | Tiertexseqvideo
      | Tiff
      | Gif
      | Dxa
      | Dnxhd
      | Thp
      | Sgi
      | C93
      | Bethsoftvid
      | Ptx
      | Txd
      | Vp6a
      | Amv
      | Vb
      | Pcx
      | Sunrast
      | Indeo4
      | Indeo5
      | Mimic
      | Rl2
      | Escape124
      | Dirac
      | Bfi
      | Cmv
      | Motionpixels
      | Tgv
      | Tgq
      | Tqi
      | Aura
      | Aura2
      | V210x
      | Tmv
      | V210
      | Dpx
      | Mad
      | Frwu
      | Flashsv2
      | Cdgraphics
      | R210
      | Anm
      | Binkvideo
      | Iff_ilbm
      | Kgv1
      | Yop
      | Vp8
      | Pictor
      | Ansi
      | A64_multi
      | A64_multi5
      | R10k
      | Mxpeg
      | Lagarith
      | Prores
      | Jv
      | Dfa
      | Wmv3image
      | Vc1image
      | Utvideo
      | Bmv_video
      | Vble
      | Dxtory
      | V410
      | Xwd
      | Cdxl
      | Xbm
      | Zerocodec
      | Mss1
      | Msa1
      | Tscc2
      | Mts2
      | Cllc
      | Mss2
      | Vp9
      | Aic
      | Escape130
      | G2m
      | Webp
      | Hnm4_video
      | Hevc
      | Fic
      | Alias_pix
      | Brender_pix
      | Paf_video
      | Exr
      | Vp7
      | Sanm
      | Sgirle
      | Mvc1
      | Mvc2
      | Hqx
      | Tdsc
      | Hq_hqa
      | Hap
      | Dds
      | Dxv
      | Screenpresso
      | Rscc
      | Avs2
      | Y41p
      | Avrp
      | A_012v
      | Avui
      | Ayuv
      | Targa_y216
      | V308
      | V408
      | Yuv4
      | Avrn
      | Cpia
      | Xface
      | Snow
      | Smvjpeg
      | Apng
      | Daala
      | Cfhd
      | Truemotion2rt
      | M101
      | Magicyuv
      | Sheervideo
      | Ylc
      | Psd
      | Pixlet
      | Speedhq
      | Fmvc
      | Scpr
      | Clearvideo
      | Xpm
      | Av1
      | Bitpacked
      | Mscc
      | Srgc
      | Svg
      | Gdv
      | Fits
      | Imm4
      | Prosumer
      | Mwsc
      | Wcmv
      | Rasc
      | First_audio
      | Pcm_s16le
      | Pcm_s16be
      | Pcm_u16le
      | Pcm_u16be
      | Pcm_s8
      | Pcm_u8
      | Pcm_mulaw
      | Pcm_alaw
      | Pcm_s32le
      | Pcm_s32be
      | Pcm_u32le
      | Pcm_u32be
      | Pcm_s24le
      | Pcm_s24be
      | Pcm_u24le
      | Pcm_u24be
      | Pcm_s24daud
      | Pcm_zork
      | Pcm_s16le_planar
      | Pcm_dvd
      | Pcm_f32be
      | Pcm_f32le
      | Pcm_f64be
      | Pcm_f64le
      | Pcm_bluray
      | Pcm_lxf
      | S302m
      | Pcm_s8_planar
      | Pcm_s24le_planar
      | Pcm_s32le_planar
      | Pcm_s16be_planar
      | Pcm_s64le
      | Pcm_s64be
      | Pcm_f16le
      | Pcm_f24le
      | Pcm_vidc
      | Adpcm_ima_qt
      | Adpcm_ima_wav
      | Adpcm_ima_dk3
      | Adpcm_ima_dk4
      | Adpcm_ima_ws
      | Adpcm_ima_smjpeg
      | Adpcm_ms
      | Adpcm_4xm
      | Adpcm_xa
      | Adpcm_adx
      | Adpcm_ea
      | Adpcm_g726
      | Adpcm_ct
      | Adpcm_swf
      | Adpcm_yamaha
      | Adpcm_sbpro_4
      | Adpcm_sbpro_3
      | Adpcm_sbpro_2
      | Adpcm_thp
      | Adpcm_ima_amv
      | Adpcm_ea_r1
      | Adpcm_ea_r3
      | Adpcm_ea_r2
      | Adpcm_ima_ea_sead
      | Adpcm_ima_ea_eacs
      | Adpcm_ea_xas
      | Adpcm_ea_maxis_xa
      | Adpcm_ima_iss
      | Adpcm_g722
      | Adpcm_ima_apc
      | Adpcm_vima
      | Adpcm_afc
      | Adpcm_ima_oki
      | Adpcm_dtk
      | Adpcm_ima_rad
      | Adpcm_g726le
      | Adpcm_thp_le
      | Adpcm_psx
      | Adpcm_aica
      | Adpcm_ima_dat4
      | Adpcm_mtaf
      | Amr_nb
      | Amr_wb
      | Ra_144
      | Ra_288
      | Roq_dpcm
      | Interplay_dpcm
      | Xan_dpcm
      | Sol_dpcm
      | Sdx2_dpcm
      | Gremlin_dpcm
      | Mp2
      | Mp3
      | Aac
      | Ac3
      | Dts
      | Vorbis
      | Dvaudio
      | Wmav1
      | Wmav2
      | Mace3
      | Mace6
      | Vmdaudio
      | Flac
      | Mp3adu
      | Mp3on4
      | Shorten
      | Alac
      | Westwood_snd1
      | Gsm
      | Qdm2
      | Cook
      | Truespeech
      | Tta
      | Smackaudio
      | Qcelp
      | Wavpack
      | Dsicinaudio
      | Imc
      | Musepack7
      | Mlp
      | Gsm_ms
      | Atrac3
      | Ape
      | Nellymoser
      | Musepack8
      | Speex
      | Wmavoice
      | Wmapro
      | Wmalossless
      | Atrac3p
      | Eac3
      | Sipr
      | Mp1
      | Twinvq
      | Truehd
      | Mp4als
      | Atrac1
      | Binkaudio_rdft
      | Binkaudio_dct
      | Aac_latm
      | Qdmc
      | Celt
      | G723_1
      | G729
      | A_8svx_exp
      | A_8svx_fib
      | Bmv_audio
      | Ralf
      | Iac
      | Ilbc
      | Opus
      | Comfort_noise
      | Tak
      | Metasound
      | Paf_audio
      | On2avc
      | Dss_sp
      | Codec2
      | Ffwavesynth
      | Sonic
      | Sonic_ls
      | Evrc
      | Smv
      | Dsd_lsbf
      | Dsd_msbf
      | Dsd_lsbf_planar
      | Dsd_msbf_planar
      | A_4gv
      | Interplay_acm
      | Xma1
      | Xma2
      | Dst
      | Atrac3al
      | Atrac3pal
      | Dolby_e
      | Aptx
      | Aptx_hd
      | Sbc
      | Atrac9
      | First_subtitle
      | Dvd_subtitle
      | Dvb_subtitle
      | Text
      | Xsub
      | Ssa
      | Mov_text
      | Hdmv_pgs_subtitle
      | Dvb_teletext
      | Srt
      | Microdvd
      | Eia_608
      | Jacosub
      | Sami
      | Realtext
      | Stl
      | Subviewer1
      | Subviewer
      | Subrip
      | Webvtt
      | Mpl2
      | Vplayer
      | Pjs
      | Ass
      | Hdmv_text_subtitle
      | Ttml
      | First_unknown
      | Ttf
      | Scte_35
      | Bintext
      | Xbin
      | Idf
      | Otf
      | Smpte_klv
      | Dvd_nav
      | Timed_id3
      | Bin_data
      | Probe
      | Mpeg2ts
      | Mpeg4systems
      | Ffmetadata
      | Wrapped_avframe
    [@@deriving show { with_path = false }]

    let t : t typ =
      enum "AVCodecID"
        [
          (NONE, none);
          (MPEG1VIDEO, mpeg1video);
          (MPEG2VIDEO, mpeg2video);
          (H261, h261);
          (H263, h263);
          (Rv10, rv10);
          (Rv20, rv20);
          (Mjpeg, mjpeg);
          (Mjpegb, mjpegb);
          (Ljpeg, ljpeg);
          (Sp5x, sp5x);
          (Jpegls, jpegls);
          (Mpeg4, mpeg4);
          (Rawvideo, rawvideo);
          (Msmpeg4v1, msmpeg4v1);
          (Msmpeg4v2, msmpeg4v2);
          (Msmpeg4v3, msmpeg4v3);
          (Wmv1, wmv1);
          (Wmv2, wmv2);
          (H263p, h263p);
          (H263i, h263i);
          (Flv1, flv1);
          (Svq1, svq1);
          (Svq3, svq3);
          (Dvvideo, dvvideo);
          (Huffyuv, huffyuv);
          (Cyuv, cyuv);
          (H264, h264);
          (Indeo3, indeo3);
          (Vp3, vp3);
          (Theora, theora);
          (Asv1, asv1);
          (Asv2, asv2);
          (Ffv1, ffv1);
          (A_4xm, _4xm);
          (Vcr1, vcr1);
          (Cljr, cljr);
          (Mdec, mdec);
          (Roq, roq);
          (Interplay_video, interplay_video);
          (Xan_wc3, xan_wc3);
          (Xan_wc4, xan_wc4);
          (Rpza, rpza);
          (Cinepak, cinepak);
          (Ws_vqa, ws_vqa);
          (Msrle, msrle);
          (Msvideo1, msvideo1);
          (Idcin, idcin);
          (A_8bps, _8bps);
          (Smc, smc);
          (Flic, flic);
          (Truemotion1, truemotion1);
          (Vmdvideo, vmdvideo);
          (Mszh, mszh);
          (Zlib, zlib);
          (Qtrle, qtrle);
          (Tscc, tscc);
          (Ulti, ulti);
          (Qdraw, qdraw);
          (Vixl, vixl);
          (Qpeg, qpeg);
          (Png, png);
          (Ppm, ppm);
          (Pbm, pbm);
          (Pgm, pgm);
          (Pgmyuv, pgmyuv);
          (Pam, pam);
          (Ffvhuff, ffvhuff);
          (Rv30, rv30);
          (Rv40, rv40);
          (Vc1, vc1);
          (Wmv3, wmv3);
          (Loco, loco);
          (Wnv1, wnv1);
          (Aasc, aasc);
          (Indeo2, indeo2);
          (Fraps, fraps);
          (Truemotion2, truemotion2);
          (Bmp, bmp);
          (Cscd, cscd);
          (Mmvideo, mmvideo);
          (Zmbv, zmbv);
          (Avs, avs);
          (Smackvideo, smackvideo);
          (Nuv, nuv);
          (Kmvc, kmvc);
          (Flashsv, flashsv);
          (Cavs, cavs);
          (Jpeg2000, jpeg2000);
          (Vmnc, vmnc);
          (Vp5, vp5);
          (Vp6, vp6);
          (Vp6f, vp6f);
          (Targa, targa);
          (Dsicinvideo, dsicinvideo);
          (Tiertexseqvideo, tiertexseqvideo);
          (Tiff, tiff);
          (Gif, gif);
          (Dxa, dxa);
          (Dnxhd, dnxhd);
          (Thp, thp);
          (Sgi, sgi);
          (C93, c93);
          (Bethsoftvid, bethsoftvid);
          (Ptx, ptx);
          (Txd, txd);
          (Vp6a, vp6a);
          (Amv, amv);
          (Vb, vb);
          (Pcx, pcx);
          (Sunrast, sunrast);
          (Indeo4, indeo4);
          (Indeo5, indeo5);
          (Mimic, mimic);
          (Rl2, rl2);
          (Escape124, escape124);
          (Dirac, dirac);
          (Bfi, bfi);
          (Cmv, cmv);
          (Motionpixels, motionpixels);
          (Tgv, tgv);
          (Tgq, tgq);
          (Tqi, tqi);
          (Aura, aura);
          (Aura2, aura2);
          (V210x, v210x);
          (Tmv, tmv);
          (V210, v210);
          (Dpx, dpx);
          (Mad, mad);
          (Frwu, frwu);
          (Flashsv2, flashsv2);
          (Cdgraphics, cdgraphics);
          (R210, r210);
          (Anm, anm);
          (Binkvideo, binkvideo);
          (Iff_ilbm, iff_ilbm);
          (Kgv1, kgv1);
          (Yop, yop);
          (Vp8, vp8);
          (Pictor, pictor);
          (Ansi, ansi);
          (A64_multi, a64_multi);
          (A64_multi5, a64_multi5);
          (R10k, r10k);
          (Mxpeg, mxpeg);
          (Lagarith, lagarith);
          (Prores, prores);
          (Jv, jv);
          (Dfa, dfa);
          (Wmv3image, wmv3image);
          (Vc1image, vc1image);
          (Utvideo, utvideo);
          (Bmv_video, bmv_video);
          (Vble, vble);
          (Dxtory, dxtory);
          (V410, v410);
          (Xwd, xwd);
          (Cdxl, cdxl);
          (Xbm, xbm);
          (Zerocodec, zerocodec);
          (Mss1, mss1);
          (Msa1, msa1);
          (Tscc2, tscc2);
          (Mts2, mts2);
          (Cllc, cllc);
          (Mss2, mss2);
          (Vp9, vp9);
          (Aic, aic);
          (Escape130, escape130);
          (G2m, g2m);
          (Webp, webp);
          (Hnm4_video, hnm4_video);
          (Hevc, hevc);
          (Fic, fic);
          (Alias_pix, alias_pix);
          (Brender_pix, brender_pix);
          (Paf_video, paf_video);
          (Exr, exr);
          (Vp7, vp7);
          (Sanm, sanm);
          (Sgirle, sgirle);
          (Mvc1, mvc1);
          (Mvc2, mvc2);
          (Hqx, hqx);
          (Tdsc, tdsc);
          (Hq_hqa, hq_hqa);
          (Hap, hap);
          (Dds, dds);
          (Dxv, dxv);
          (Screenpresso, screenpresso);
          (Rscc, rscc);
          (Avs2, avs2);
          (Y41p, y41p);
          (Avrp, avrp);
          (A_012v, _012v);
          (Avui, avui);
          (Ayuv, ayuv);
          (Targa_y216, targa_y216);
          (V308, v308);
          (V408, v408);
          (Yuv4, yuv4);
          (Avrn, avrn);
          (Cpia, cpia);
          (Xface, xface);
          (Snow, snow);
          (Smvjpeg, smvjpeg);
          (Apng, apng);
          (Daala, daala);
          (Cfhd, cfhd);
          (Truemotion2rt, truemotion2rt);
          (M101, m101);
          (Magicyuv, magicyuv);
          (Sheervideo, sheervideo);
          (Ylc, ylc);
          (Psd, psd);
          (Pixlet, pixlet);
          (Speedhq, speedhq);
          (Fmvc, fmvc);
          (Scpr, scpr);
          (Clearvideo, clearvideo);
          (Xpm, xpm);
          (Av1, av1);
          (Bitpacked, bitpacked);
          (Mscc, mscc);
          (Srgc, srgc);
          (Svg, svg);
          (Gdv, gdv);
          (Fits, fits);
          (Imm4, imm4);
          (Prosumer, prosumer);
          (Mwsc, mwsc);
          (Wcmv, wcmv);
          (Rasc, rasc);
          (First_audio, first_audio);
          (Pcm_s16le, pcm_s16le);
          (Pcm_s16be, pcm_s16be);
          (Pcm_u16le, pcm_u16le);
          (Pcm_u16be, pcm_u16be);
          (Pcm_s8, pcm_s8);
          (Pcm_u8, pcm_u8);
          (Pcm_mulaw, pcm_mulaw);
          (Pcm_alaw, pcm_alaw);
          (Pcm_s32le, pcm_s32le);
          (Pcm_s32be, pcm_s32be);
          (Pcm_u32le, pcm_u32le);
          (Pcm_u32be, pcm_u32be);
          (Pcm_s24le, pcm_s24le);
          (Pcm_s24be, pcm_s24be);
          (Pcm_u24le, pcm_u24le);
          (Pcm_u24be, pcm_u24be);
          (Pcm_s24daud, pcm_s24daud);
          (Pcm_zork, pcm_zork);
          (Pcm_s16le_planar, pcm_s16le_planar);
          (Pcm_dvd, pcm_dvd);
          (Pcm_f32be, pcm_f32be);
          (Pcm_f32le, pcm_f32le);
          (Pcm_f64be, pcm_f64be);
          (Pcm_f64le, pcm_f64le);
          (Pcm_bluray, pcm_bluray);
          (Pcm_lxf, pcm_lxf);
          (S302m, s302m);
          (Pcm_s8_planar, pcm_s8_planar);
          (Pcm_s24le_planar, pcm_s24le_planar);
          (Pcm_s32le_planar, pcm_s32le_planar);
          (Pcm_s16be_planar, pcm_s16be_planar);
          (Pcm_s64le, pcm_s64le);
          (Pcm_s64be, pcm_s64be);
          (Pcm_f16le, pcm_f16le);
          (Pcm_f24le, pcm_f24le);
          (Pcm_vidc, pcm_vidc);
          (Adpcm_ima_qt, adpcm_ima_qt);
          (Adpcm_ima_wav, adpcm_ima_wav);
          (Adpcm_ima_dk3, adpcm_ima_dk3);
          (Adpcm_ima_dk4, adpcm_ima_dk4);
          (Adpcm_ima_ws, adpcm_ima_ws);
          (Adpcm_ima_smjpeg, adpcm_ima_smjpeg);
          (Adpcm_ms, adpcm_ms);
          (Adpcm_4xm, adpcm_4xm);
          (Adpcm_xa, adpcm_xa);
          (Adpcm_adx, adpcm_adx);
          (Adpcm_ea, adpcm_ea);
          (Adpcm_g726, adpcm_g726);
          (Adpcm_ct, adpcm_ct);
          (Adpcm_swf, adpcm_swf);
          (Adpcm_yamaha, adpcm_yamaha);
          (Adpcm_sbpro_4, adpcm_sbpro_4);
          (Adpcm_sbpro_3, adpcm_sbpro_3);
          (Adpcm_sbpro_2, adpcm_sbpro_2);
          (Adpcm_thp, adpcm_thp);
          (Adpcm_ima_amv, adpcm_ima_amv);
          (Adpcm_ea_r1, adpcm_ea_r1);
          (Adpcm_ea_r3, adpcm_ea_r3);
          (Adpcm_ea_r2, adpcm_ea_r2);
          (Adpcm_ima_ea_sead, adpcm_ima_ea_sead);
          (Adpcm_ima_ea_eacs, adpcm_ima_ea_eacs);
          (Adpcm_ea_xas, adpcm_ea_xas);
          (Adpcm_ea_maxis_xa, adpcm_ea_maxis_xa);
          (Adpcm_ima_iss, adpcm_ima_iss);
          (Adpcm_g722, adpcm_g722);
          (Adpcm_ima_apc, adpcm_ima_apc);
          (Adpcm_vima, adpcm_vima);
          (Adpcm_afc, adpcm_afc);
          (Adpcm_ima_oki, adpcm_ima_oki);
          (Adpcm_dtk, adpcm_dtk);
          (Adpcm_ima_rad, adpcm_ima_rad);
          (Adpcm_g726le, adpcm_g726le);
          (Adpcm_thp_le, adpcm_thp_le);
          (Adpcm_psx, adpcm_psx);
          (Adpcm_aica, adpcm_aica);
          (Adpcm_ima_dat4, adpcm_ima_dat4);
          (Adpcm_mtaf, adpcm_mtaf);
          (Amr_nb, amr_nb);
          (Amr_wb, amr_wb);
          (Ra_144, ra_144);
          (Ra_288, ra_288);
          (Roq_dpcm, roq_dpcm);
          (Interplay_dpcm, interplay_dpcm);
          (Xan_dpcm, xan_dpcm);
          (Sol_dpcm, sol_dpcm);
          (Sdx2_dpcm, sdx2_dpcm);
          (Gremlin_dpcm, gremlin_dpcm);
          (Mp2, mp2);
          (Mp3, mp3);
          (Aac, aac);
          (Ac3, ac3);
          (Dts, dts);
          (Vorbis, vorbis);
          (Dvaudio, dvaudio);
          (Wmav1, wmav1);
          (Wmav2, wmav2);
          (Mace3, mace3);
          (Mace6, mace6);
          (Vmdaudio, vmdaudio);
          (Flac, flac);
          (Mp3adu, mp3adu);
          (Mp3on4, mp3on4);
          (Shorten, shorten);
          (Alac, alac);
          (Westwood_snd1, westwood_snd1);
          (Gsm, gsm);
          (Qdm2, qdm2);
          (Cook, cook);
          (Truespeech, truespeech);
          (Tta, tta);
          (Smackaudio, smackaudio);
          (Qcelp, qcelp);
          (Wavpack, wavpack);
          (Dsicinaudio, dsicinaudio);
          (Imc, imc);
          (Musepack7, musepack7);
          (Mlp, mlp);
          (Gsm_ms, gsm_ms);
          (Atrac3, atrac3);
          (Ape, ape);
          (Nellymoser, nellymoser);
          (Musepack8, musepack8);
          (Speex, speex);
          (Wmavoice, wmavoice);
          (Wmapro, wmapro);
          (Wmalossless, wmalossless);
          (Atrac3p, atrac3p);
          (Eac3, eac3);
          (Sipr, sipr);
          (Mp1, mp1);
          (Twinvq, twinvq);
          (Truehd, truehd);
          (Mp4als, mp4als);
          (Atrac1, atrac1);
          (Binkaudio_rdft, binkaudio_rdft);
          (Binkaudio_dct, binkaudio_dct);
          (Aac_latm, aac_latm);
          (Qdmc, qdmc);
          (Celt, celt);
          (G723_1, g723_1);
          (G729, g729);
          (A_8svx_exp, _8svx_exp);
          (A_8svx_fib, _8svx_fib);
          (Bmv_audio, bmv_audio);
          (Ralf, ralf);
          (Iac, iac);
          (Ilbc, ilbc);
          (Opus, opus);
          (Comfort_noise, comfort_noise);
          (Tak, tak);
          (Metasound, metasound);
          (Paf_audio, paf_audio);
          (On2avc, on2avc);
          (Dss_sp, dss_sp);
          (Codec2, codec2);
          (Ffwavesynth, ffwavesynth);
          (Sonic, sonic);
          (Sonic_ls, sonic_ls);
          (Evrc, evrc);
          (Smv, smv);
          (Dsd_lsbf, dsd_lsbf);
          (Dsd_msbf, dsd_msbf);
          (Dsd_lsbf_planar, dsd_lsbf_planar);
          (Dsd_msbf_planar, dsd_msbf_planar);
          (A_4gv, _4gv);
          (Interplay_acm, interplay_acm);
          (Xma1, xma1);
          (Xma2, xma2);
          (Dst, dst);
          (Atrac3al, atrac3al);
          (Atrac3pal, atrac3pal);
          (Dolby_e, dolby_e);
          (Aptx, aptx);
          (Aptx_hd, aptx_hd);
          (Sbc, sbc);
          (Atrac9, atrac9);
          (First_subtitle, first_subtitle);
          (Dvd_subtitle, dvd_subtitle);
          (Dvb_subtitle, dvb_subtitle);
          (Text, text);
          (Xsub, xsub);
          (Ssa, ssa);
          (Mov_text, mov_text);
          (Hdmv_pgs_subtitle, hdmv_pgs_subtitle);
          (Dvb_teletext, dvb_teletext);
          (Srt, srt);
          (Microdvd, microdvd);
          (Eia_608, eia_608);
          (Jacosub, jacosub);
          (Sami, sami);
          (Realtext, realtext);
          (Stl, stl);
          (Subviewer1, subviewer1);
          (Subviewer, subviewer);
          (Subrip, subrip);
          (Webvtt, webvtt);
          (Mpl2, mpl2);
          (Vplayer, vplayer);
          (Pjs, pjs);
          (Ass, ass);
          (Hdmv_text_subtitle, hdmv_text_subtitle);
          (Ttml, ttml);
          (First_unknown, first_unknown);
          (Ttf, ttf);
          (Scte_35, scte_35);
          (Bintext, bintext);
          (Xbin, xbin);
          (Idf, idf);
          (Otf, otf);
          (Smpte_klv, smpte_klv);
          (Dvd_nav, dvd_nav);
          (Timed_id3, timed_id3);
          (Bin_data, bin_data);
          (Probe, probe);
          (Mpeg2ts, mpeg2ts);
          (Mpeg4systems, mpeg4systems);
          (Ffmetadata, ffmetadata);
          (Wrapped_avframe, wrapped_avframe);
        ]
  end

  module PktFlag = struct
    let key = constant "AV_PKT_FLAG_KEY" int
    let corrupt = constant "AV_PKT_FLAG_CORRUPT" int
    let discard = constant "AV_PKT_FLAG_DISCARD" int
    let trusted = constant "AV_PKT_FLAG_TRUSTED" int
    let disposable = constant "AV_PKT_FLAG_DISPOSABLE" int

    type t = Key | Corrupt | Discard | Trusted | Disposable
    [@@deriving show { with_path = false }]
  end

  module CodecFlag = struct
    let qscale = constant "AV_CODEC_FLAG_QSCALE" int
    let _4mv = constant "AV_CODEC_FLAG_4MV" int
    let output_corrupt = constant "AV_CODEC_FLAG_OUTPUT_CORRUPT" int
    let qpel = constant "AV_CODEC_FLAG_QPEL" int
    let pass1 = constant "AV_CODEC_FLAG_PASS1" int
    let pass2 = constant "AV_CODEC_FLAG_PASS2" int
    let loop_filter = constant "AV_CODEC_FLAG_LOOP_FILTER" int
    let gray = constant "AV_CODEC_FLAG_GRAY" int
    let psnr = constant "AV_CODEC_FLAG_PSNR" int
    let truncated = constant "AV_CODEC_FLAG_TRUNCATED" int
    let interlaced_dct = constant "AV_CODEC_FLAG_INTERLACED_DCT" int
    let low_delay = constant "AV_CODEC_FLAG_LOW_DELAY" int
    let global_header = constant "AV_CODEC_FLAG_GLOBAL_HEADER" int
    let bitexact = constant "AV_CODEC_FLAG_BITEXACT" int
    let ac_pred = constant "AV_CODEC_FLAG_AC_PRED" int
    let interlaced_me = constant "AV_CODEC_FLAG_INTERLACED_ME" int
    let closed_gop = constant "AV_CODEC_FLAG_CLOSED_GOP" int
  end

  module CodecFlag2 = struct
    let fast = constant "AV_CODEC_FLAG2_FAST" int
    let no_output = constant "AV_CODEC_FLAG2_NO_OUTPUT" int
    let local_header = constant "AV_CODEC_FLAG2_LOCAL_HEADER" int
  end

  module Codec = struct
    type codec
    type t = codec structure

    let t : t typ = structure "AVCodec"
    let f x y = field t x y
    let name = f "name" string
    let long_name = f "long_name" string
    let type_ = f "type" U.MediaType.t
    let id = f "id" CODEC_ID.t
    let supported_framerates = f "supported_framerates" @@ ptr_opt U.Rational.t
    let pix_fmts = f "pix_fmts" @@ ptr_opt U.PixFmt.t
    let supported_samplerates = f "supported_samplerates" @@ ptr_opt int
    let sample_fmts = f "sample_fmts" @@ ptr_opt U.SampleFmt.t
    let channel_layouts = f "channel_layouts" @@ ptr_opt int64_t
  end

  module CodecParameters = struct
    type codec_parameters
    type t = codec_parameters structure

    let t : t typ = structure "AVCodecParameters"
    let f x y = field t x y
    let codec_type = f "codec_type" U.MediaType.t
    let codec_id = f "codec_id" CODEC_ID.t
    let codec_tag = f "codec_tag" uint32_t
    let format = f "format" int
    let width = f "width" int
    let height = f "height" int
    let channels = f "channels" int
    let sample_rate = f "sample_rate" int
    let frame_size = f "frame_size" int
    let () = seal t
  end

  module CodecContext = struct
    type codec_context
    type t = codec_context structure

    let t : t typ = structure "AVCodecContext"
    let f x y = field t x y
    let priv_data = f "priv_data" @@ ptr void
    let bitrate = f "bit_rate" int64_t
    let bit_rate_tolerance = f "bit_rate_tolerance" int
    let global_quality = f "global_quality" int
    let compression_level = f "compression_level" int
    let framerate = f "framerate" U.Rational.t
    let flags = f "flags" int
    let flags2 = f "flags2" int
    let extradata = f "extradata" @@ ptr uint8_t
    let extradata_size = f "extradata_size" int
    let time_base = f "time_base" U.Rational.t
    let width = f "width" int
    let height = f "height" int
    let gop_size = f "gop_size" int
    let pix_fmt = f "pix_fmt" U.PixFmt.t
    let max_b_frames = f "max_b_frames" int
    let b_quant_factor = f "b_quant_factor" float
    let b_quant_offset = f "b_quant_offset" int
    let i_quant_factor = f "i_quant_factor" float
    let i_quant_offset = f "i_quant_offset" float
    let sample_aspect_ratio = f "sample_aspect_ratio" U.Rational.t
    let me_cmp = f "me_cmp" int
    let me_sub_cmp = f "me_sub_cmp" int
    let mb_cmp = f "mb_cmp" int
    let ildct_cmp = f "ildct_cmp" int
    let dia_size = f "dia_size" int
    let sample_rate = f "sample_rate" int
    let channels = f "channels" int
    let sample_fmt = f "sample_fmt" U.SampleFmt.t
    let channel_layout = f "channel_layout" int64_t
    let rc_buffer_size = f "rc_buffer_size" int64_t
    let rc_max_rate = f "rc_max_rate" int64_t
    let rc_min_rate = f "rc_min_rate" int64_t
    let thread_count = f "thread_count" int
  end

  (* module PacketSideData = struct
       type packet_side_data
       type t = packet_side_data structure

       let t : t typ = structure "AVPacketSideData"
     end *)

  module Packet = struct
    type packet
    type t = packet structure

    let t : t typ = structure "AVPacket"
    let f x y = field t x y
    let buf = f "buf" U.Buffer.t
    let pts = f "pts" int64_t
    let dts = f "dts" int64_t
    let data = f "data" @@ ptr uint8_t
    let size = f "size" int
    let stream_index = f "stream_index" int
    let flags = f "flags" int

    (* let side_data = f "side_data" @@ ptr PacketSideData.t *)
    let side_data_elems = f "side_data_elems" int
    let duration = f "duration" int64_t
    let pos = f "pos" int64_t
    let () = seal t
  end
end
