(* module type S = sig
  type 'a typ

  module Buffer : sig
    type t

    val t : t typ
  end
end *)

module Types (T : Cstubs.Types.TYPE) = struct
  type 'a structure = 'a Ctypes.structure
  type 'a union = 'a Ctypes.union
  type 'a typ = 'a T.typ

  open T

  module type Decl = sig
    type t

    val t : t typ
  end
  (* module Util = Avutils_types.Types (T) *)

  module DeviceInfoList = struct
    type device_info_list
    type t = device_info_list structure

    let t : t typ = structure "AVDeviceInfoList"
  end

  module AVDeviceCapabilitiesQuery = struct
    type device_cap_query
    type t = device_cap_query structure

    let t : t typ = structure "AVDeviceCapabilitiesQuery"
  end
end
