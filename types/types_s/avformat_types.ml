module type Util = sig
  type 'a typ

  module type Decl = sig
    type t

    val t : t typ
  end

  module Dictionary : Decl
  module Class : Decl
  module Rational : Decl
end

module type Codec = sig
  type 'a typ

  module type Decl = sig
    type t

    val t : t typ
  end

  module Packet : Decl
  module Codec : Decl
  module CODEC_ID : Decl
  module CodecParameters : Decl
end

module type Device = sig
  type 'a typ

  module type Decl = sig
    type t

    val t : t typ
  end

  module DeviceInfoList : Decl
  module AVDeviceCapabilitiesQuery : Decl
end

module Types
    (T : Cstubs.Types.TYPE)
    (U : Util with type 'a typ = 'a T.typ)
    (C : Codec with type 'a typ = 'a T.typ)
    (D : Device with type 'a typ = 'a T.typ) =
struct
  type 'a structure = 'a Ctypes.structure
  type 'a union = 'a Ctypes.union
  type 'a typ = 'a T.typ

  open T

  module type Decl = sig
    type t

    val t : t typ
  end
  (* module AVCT = Avcodec_types.Types (T) *)
  (* module AVUT = Avutils_types.Types (T) *)
  (* module AVDT = Avdevice_types.Types (T) *)

  module AVFMT_DURATION = struct
    let from_pts = constant "AVFMT_DURATION_FROM_PTS" int64_t
    and from_stream = constant "AVFMT_DURATION_FROM_STREAM" int64_t

    type avfmt_duration = FROM_PTS | FROM_STREAM

    let avfmt_duration : avfmt_duration typ =
      enum "AVDurationEstimationMethod"
        [ (FROM_PTS, from_pts); (FROM_STREAM, from_stream) ]
  end

  module StreamInitInResult = struct
    let write_header = constant "AVSTREAM_INIT_IN_WRITE_HEADER" int
    let init_output = constant "AVSTREAM_INIT_IN_INIT_OUTPUT" int
  end

  module FmtFlag = struct
    let no_file = constant "AVFMT_NOFILE" int
    let neednumber = constant "AVFMT_NEEDNUMBER" int
    let show_ids = constant "AVFMT_SHOW_IDS" int
    let globalheader = constant "AVFMT_GLOBALHEADER" int
    let notimestamps = constant "AVFMT_NOTIMESTAMPS" int
    let generic_index = constant "AVFMT_GENERIC_INDEX" int
    let ts_discont = constant "AVFMT_TS_DISCONT" int
    let variable_fps = constant "AVFMT_VARIABLE_FPS" int
    let nodimensions = constant "AVFMT_NODIMENSIONS" int
    let nostreams = constant "AVFMT_NOSTREAMS" int
    let nobinsearch = constant "AVFMT_NOBINSEARCH" int
    let nogensearch = constant "AVFMT_NOGENSEARCH" int
    let no_byte_seek = constant "AVFMT_NO_BYTE_SEEK" int
    let allow_flush = constant "AVFMT_ALLOW_FLUSH" int
    let ts_nonstrict = constant "AVFMT_TS_NONSTRICT" int
    let ts_negative = constant "AVFMT_TS_NEGATIVE" int
    let seek_to_pts = constant "AVFMT_SEEK_TO_PTS" int
  end

  module SeekFlag = struct
    let backward = constant "AVSEEK_FLAG_BACKWARD" int
    let byte = constant "AVSEEK_FLAG_BYTE" int
    let any = constant "AVSEEK_FLAG_ANY" int
    let frame = constant "AVSEEK_FLAG_FRAME" int
  end

  module IOConst = struct
    let flag_write = constant "AVIO_FLAG_WRITE" int
  end

  module IOContext = struct
    type io_context
    type t = io_context structure

    let t : t typ = structure "AVIOContext"
  end

  module IOInterruptCB = struct
    type io_interrupt_cb
    type t = io_interrupt_cb structure

    let t : t typ = structure "AVIOInterruptCB"
  end

  type codec_tag

  let codec_tag : codec_tag structure typ = structure "AVCodecTag"

  module ProbeData = struct
    type probe_data
    type t = probe_data structure

    let t : t typ = structure "AVProbeData"
  end

  module FormatContextDecl = struct
    type format_context
    type t = format_context structure

    let t : t typ = structure "AVFormatContext"
  end

  module InputFormatDecl = struct
    type input_format
    type t = input_format structure

    let t : t typ = structure "AVInputFormat"
  end

  module InputFormat = struct
    include InputFormatDecl

    let f x y = field t x y
    let read_probe_t = ptr ProbeData.t @-> returning int
    let read_header_t = ptr FormatContextDecl.t @-> returning int

    let read_packet_t =
      ptr FormatContextDecl.t @-> ptr C.Packet.t @-> returning int

    let read_close_t = ptr FormatContextDecl.t @-> returning int

    let read_seek_t =
      ptr FormatContextDecl.t @-> int @-> int64_t @-> int @-> returning int

    let read_timestamp_t =
      ptr FormatContextDecl.t @-> int @-> ptr int64_t @-> int64_t
      @-> returning int64_t

    let read_play_t = ptr FormatContextDecl.t @-> returning int
    let read_pause_t = ptr FormatContextDecl.t @-> returning int

    let read_seek2_t =
      ptr FormatContextDecl.t @-> int @-> int64_t @-> int64_t @-> int64_t
      @-> int @-> returning int

    let get_device_list_t =
      ptr FormatContextDecl.t @-> ptr D.DeviceInfoList.t @-> returning int

    let create_device_capabilities_t =
      ptr FormatContextDecl.t
      @-> ptr D.AVDeviceCapabilitiesQuery.t
      @-> returning int

    let free_device_capabilities_t =
      ptr FormatContextDecl.t
      @-> ptr D.AVDeviceCapabilitiesQuery.t
      @-> returning int

    let name = f "name" @@ ptr char
    let long_name = f "long_name" @@ ptr char
    let flags = f "flags" int
    let extensions = f "extensions" @@ ptr char
    let codec_tag = f "codec_tag" @@ ptr @@ ptr codec_tag
    let priv_class = f "priv_class" @@ ptr U.Class.t
    let mime_type = f "mime_type" @@ ptr char
    let next = f "next" @@ ptr t
    let raw_codec_id = f "raw_codec_id" int
    let priv_data_size = f "priv_data_size" int
    let read_probe = f "read_probe" @@ static_funptr read_probe_t
    let read_header = f "read_header" @@ static_funptr read_header_t
    let read_packet = f "read_packet" @@ static_funptr read_packet_t
    let read_close = f "read_close" @@ static_funptr read_close_t
    let read_seek = f "read_seek" @@ static_funptr read_seek_t
    let read_timestamp = f "read_timestamp" @@ static_funptr read_timestamp_t
    let read_play = f "read_play" @@ static_funptr read_play_t
    let read_pause = f "read_pause" @@ static_funptr read_pause_t
    let read_seek2 = f "read_seek2" @@ static_funptr read_seek2_t
    let get_device_list = f "get_device_list" @@ static_funptr get_device_list_t

    let create_device_capabilities =
      f "create_device_capabilities"
      @@ static_funptr create_device_capabilities_t

    let free_device_capabilities =
      f "free_device_capabilities" @@ static_funptr free_device_capabilities_t

    let () = seal t
  end

  module CodecTag = struct
    type codec_tag
    type t = codec_tag structure

    let t : t typ = structure "AVCodecTag"
    let id = field t "id" C.CODEC_ID.t
    let tag = field t "tag" uint
    let () = seal t
  end

  module OutputFormat = struct
    type output_format
    type t = output_format structure

    let t : t typ = structure "AVOutputFormat"
    let name = field t "name" string
    let flags = field t "flags" int
    let codec_tag = field t "codec_tag" @@ ptr @@ ptr_opt CodecTag.t
    let () = seal t
  end

  module Stream = struct
    type stream
    type t = stream structure

    let t : t typ = structure "AVStream"
    let f x y = field t x y
    let index = f "index" int
    let id = f "id" int
    let time_base = f "time_base" U.Rational.t
    let duration = f "duration" int64_t
    let nb_frames = f "nb_frames" int64_t
    let metadata = f "metadata" U.Dictionary.t
    let avg_frame_rate = f "avg_frame_rate" U.Rational.t
    let r_frame_rate = f "r_frame_rate" U.Rational.t
    let codecpar = f "codecpar" @@ ptr_opt C.CodecParameters.t
    let () = seal t
  end

  type program

  let program : program structure typ = structure "AVProgram"

  type chapter

  let chapter : chapter structure typ = structure "AVChapter"

  type io_interrupt_cb

  let io_interrupt_cb : io_interrupt_cb structure typ =
    structure "AVIOInterruptCB"

  type format_internal

  let format_internal : format_internal structure typ =
    structure "AVFormatInternal"

  module FormatContext = struct
    include FormatContextDecl

    let av_format_control_message_t =
      ptr t @-> int @-> ptr void @-> size_t @-> returning int

    let io_open_t =
      ptr t @-> ptr IOContext.t @-> ptr char @-> int
      @-> ptr (ptr U.Dictionary.t)
      @-> returning int

    let io_close_t = ptr t @-> ptr IOContext.t @-> returning void
    let fc_field x y = field t x y
    let av_class = fc_field "av_class" @@ ptr U.Class.t
    let iformat = fc_field "iformat" @@ ptr InputFormatDecl.t
    let oformat = fc_field "oformat" @@ ptr OutputFormat.t
    let priv_data = fc_field "priv_data" @@ ptr void
    let pb = fc_field "pb" @@ ptr_opt IOContext.t
    let ctx_flags = fc_field "ctx_flags" int
    let nb_streams = fc_field "nb_streams" uint
    let streams = fc_field "streams" @@ ptr @@ ptr Stream.t
    let url = fc_field "url" @@ ptr char
    let start_time = fc_field "start_time" int64_t
    let duration = fc_field "duration" int64_t
    let bit_rate = fc_field "bit_rate" int64_t
    let packet_size = fc_field "packet_size" uint
    let max_delay = fc_field "max_delay" uint
    let flags = fc_field "flags" int
    let probesize = fc_field "probesize" int64_t
    let max_analyze_duration = fc_field "max_analyze_duration" int64_t
    let key = fc_field "key" @@ ptr uint8_t
    let keylen = fc_field "keylen" int
    let nb_programs = fc_field "nb_programs" uint
    let programs = fc_field "programs" @@ ptr @@ ptr program
    let video_codec_id = fc_field "video_codec_id" C.CODEC_ID.t
    let audio_codec_id = fc_field "audio_codec_id" C.CODEC_ID.t
    let subtitle_codec_id = fc_field "subtitle_codec_id" C.CODEC_ID.t
    let max_index_size = fc_field "max_index_size" uint
    let max_picture_buffer = fc_field "max_picture_buffer" uint
    let nb_chapters = fc_field "nb_chapters" uint
    let chapters = fc_field "chapters" @@ ptr @@ ptr chapter
    let metadata = fc_field "metadata" @@ ptr U.Dictionary.t
    let start_time_realtime = fc_field "start_time_realtime" @@ int64_t
    let fps_probe_size = fc_field "fps_probe_size" int
    let error_recognition = fc_field "error_recognition" int
    let interrupt_callback = fc_field "interrupt_callback" io_interrupt_cb
    let debug = fc_field "debug" int
    let max_interleave_delta = fc_field "max_interleave_delta" int64_t
    let strict_std_compliance = fc_field "strict_std_compliance" int
    let event_flags = fc_field "event_flags" int
    let max_ts_probe = fc_field "max_ts_probe" int
    let avoid_negative_ts = fc_field "avoid_negative_ts" int
    let ts_id = fc_field "ts_id" int
    let audio_preload = fc_field "audio_preload" int
    let max_chunk_duration = fc_field "max_chunk_duration" int
    let max_chunk_size = fc_field "max_chunk_size" int
    let use_wallclock_as_timestamps = fc_field "use_wallclock_as_timestamps" int
    let avio_flags = fc_field "avio_flags" int

    let duration_estimation_method =
      fc_field "duration_estimation_method" AVFMT_DURATION.avfmt_duration

    let skip_initial_bytes = fc_field "skip_initial_bytes" int64_t
    let correct_ts_overflow = fc_field "correct_ts_overflow" uint
    let seek2any = fc_field "seek2any" int
    let flush_packets = fc_field "flush_packets" int
    let probe_score = fc_field "probe_score" int
    let format_probesize = fc_field "format_probesize" int
    let codec_whitelist = fc_field "codec_whitelist" @@ ptr char
    let format_whitelist = fc_field "format_whitelist" @@ ptr char
    let internal = fc_field "internal" format_internal
    let io_repositioned = fc_field "io_repositioned" int
    let video_codec = fc_field "video_codec" C.Codec.t
    let audio_codec = fc_field "audio_codec" C.Codec.t
    let subtitle_codec = fc_field "subtitle_codec" C.Codec.t
    let data_codec = fc_field "data_codec" C.Codec.t
    let metadata_header_padding = fc_field "metadata_header_padding" int
    let opaque = fc_field "opaque" @@ ptr void

    let control_message_cb =
      fc_field "control_message_cb" @@ static_funptr av_format_control_message_t

    let output_ts_offset = fc_field "output_ts_offset" int64_t
    let dump_separator = fc_field "dump_separator" @@ ptr uint8_t
    let data_codec_id = fc_field "data_codec_id" C.CODEC_ID.t
    let protocol_whitelist = fc_field "protocol_whitelist" @@ ptr char
    let io_open = fc_field "io_open" @@ static_funptr io_open_t
    let io_close = fc_field "io_close" @@ static_funptr io_close_t
    let protocol_blacklist = fc_field "protocol_blacklist" @@ ptr char
    let max_streams = fc_field "max_streams" int

    let skip_estimate_duration_from_pts =
      fc_field "skip_estimate_duration_from_pts" int

    let () = seal t
  end
end
