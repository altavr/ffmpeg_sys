module Types (T : Ctypes.TYPE) = struct
  module AVUtil = Avutils_types.Types (T)
  module AVDevice = Avdevice_types.Types (T)
  module AVCodec = Avcodec_types.Types (T) (AVUtil)
  module AVFormat = Avformat_types.Types (T) (AVUtil) (AVCodec) (AVDevice)
  module Swscale = Swscale_types.Types (T) (AVUtil)
end
  