let header =
  {|
  #include <errno.h>
  #include <stdio.h>
  #include <libavutil/log.h>
  #include <libavutil/opt.h>
  #include <libavutil/frame.h>
  #include <libavutil/dict.h>
  #include <libavformat/avformat.h>
  #include <libavformat/avio.h>
  #include <libavdevice/avdevice.h>
  #include <libavcodec/avcodec.h>
  #include <libswscale/swscale.h>

 typedef struct AVCodecTag {
     enum AVCodecID id;
        unsigned int tag;
   } AVCodecTag;
|}

let () =
  let file =
    match Sys.argv with
    | [| _; file |] -> file
    | _ -> failwith "Arguments error"
  in

  let oc = open_out file in
  let fmt = Format.formatter_of_out_channel oc in
  output_string oc header;
  Cstubs.Types.write_c fmt (module Types_s.Types);

  Format.pp_print_flush fmt ();
  close_out oc
